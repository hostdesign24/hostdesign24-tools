package com.aem.hostdesign24tools.core.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.HostDesign24ScrappingToolForMenService;
import com.aem.hostdesign24tools.core.valueobjects.CsvProductImportVO;
import com.aem.hostdesign24tools.core.valueobjects.styleformen.Link;
import com.aem.hostdesign24tools.core.valueobjects.styleformen.SearchResults;
import com.aem.hostdesign24tools.core.valueobjects.styleformen.ServiceProduct;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(HostDesign24ScrappingToolForMenService.class)
@Component(metatype = true, immediate = true, label = "Choicify Scrapping Tool For Men Service")
public class HostDesign24ScrappingToolForMenServiceImpl
		implements HostDesign24ScrappingToolForMenService {

	@Property(unbounded = PropertyUnbounded.ARRAY, label = "Category URL Mapping", cardinality = 50,
			description = "Please enter the category url mapping")
	private static final String URL_MAPPER = "scsrappingtool.formen.urlmapper";

	@Property(unbounded = PropertyUnbounded.ARRAY, label = "Illegal Products", cardinality = 50,
			description = "Please enter the code of Illegal Products")
	private static final String ILLEGAL_PRODUCTS = "scsrappingtool.formen.illegalproducts";

	private String[] multiUrlMapper;
	private String[] multiIllegalProducts;

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		log.info("[*** HostDesign24ScrappingToolForMenServiceImpl: activating configuration service");
		readProperties(properties);
	}

	protected void readProperties(Map<String, Object> properties) {
		this.multiUrlMapper = PropertiesUtil.toStringArray(properties.get(URL_MAPPER));
		this.multiIllegalProducts = PropertiesUtil.toStringArray(properties.get(ILLEGAL_PRODUCTS));
	}

	/**
	 * Execute thread importer Service.
	 */
	@Override
	public void process(ResourceResolver resourceResolver, SlingHttpServletRequest request) {
		Session session = null;

		session = resourceResolver.adaptTo(Session.class);
		processSchedularInputs(this.multiUrlMapper, session);
	}

	public void processSchedularInputs(String[] categoryList, Session session) {
		log.info("ScrappingToolForMenService :: START :: processing products gtins from Excel file");
		try {
			List<CsvProductImportVO> results = new ArrayList<>();
			for (int i = 0; i < categoryList.length; i++) {
				String[] urlcateogory = categoryList[i].split("#");
				processResults(urlcateogory[0], urlcateogory[1], results);
			}

			saveScrappingToolResults(session, results);
		} catch (Exception ex) {
			log.error("ScrappingToolForMenService :: processing products gtins from Excel file {}",
					ex);
		}
		log.info("ScrappingToolForMenService :: END :: processing products gtins from Excel file");
	}

	private void saveScrappingToolResults(Session session, List<CsvProductImportVO> results)
			throws RepositoryException {

		String jsonResults = new Gson().toJson(results);

		Node resultNode =
				JcrUtils.getOrCreateByPath(HostDesign24AppConstants.SCRAPPING_PRODUCTS_PATH_FOR_MEN, false,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED, session, true);

		if (resultNode.hasProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS)) {
			resultNode.getProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS).remove();
			resultNode.getSession().save();
		}
		resultNode.setProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, jsonResults);
		JcrUtils.setLastModified(resultNode, Calendar.getInstance());
		session.save();
	}

	private void processResults(String url, String category, List<CsvProductImportVO> results)
			throws IOException {
		URL obj = new URL(url.toString());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuilder response = new StringBuilder();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		try {
			Gson gson = new Gson();
			SearchResults searchResults = gson.fromJson(response.toString(), SearchResults.class);
			if (searchResults != null) {
				List<ServiceProduct> serviceProducts = searchResults.getServiceProducts();
				CsvProductImportVO csvProductImportVO = null;
				System.setProperty("file.encoding", "UTF-8");
				for (ServiceProduct serviceProduct : serviceProducts) {
					if (!Arrays.asList(this.multiIllegalProducts).contains(serviceProduct.getGtin())) {
						csvProductImportVO = new CsvProductImportVO();

						csvProductImportVO.setGtin(serviceProduct.getGtin());
						csvProductImportVO.setCountry(serviceProduct.getIsoCountry());
						csvProductImportVO.setLanguage(serviceProduct.getIsoLanguage());
						csvProductImportVO.setBrandTitle(serviceProduct.getBrand());
						csvProductImportVO.setCategory(category);
						csvProductImportVO.setProductLineTitle(category);

						if (StringUtils.isNotBlank(serviceProduct.getNetQuantityContent())) {
							csvProductImportVO.setProductTitle(serviceProduct.getName() + ","
									+ Math.round(Float.valueOf(serviceProduct.getNetQuantityContent())) + ""
									+ serviceProduct.getContentUnit());
						} else {
							csvProductImportVO.setProductTitle(serviceProduct.getName());
						}

						List<Link> links = serviceProduct.getLinks();
						if (links != null && !links.isEmpty()) {
							csvProductImportVO.setProductUrl("https://www.dm.de" + links.get(2).getHref());
							csvProductImportVO.setImagePath(links.get(0).getHref());
						}
					}
					results.add(csvProductImportVO);
				}
			}

		} catch (Exception ex) {
			log.error("Exception {}", ex);
		}
	}

}
