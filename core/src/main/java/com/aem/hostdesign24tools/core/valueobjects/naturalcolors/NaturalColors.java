package com.aem.hostdesign24tools.core.valueobjects.naturalcolors;

import java.util.ArrayList;
import java.util.List;
import com.aem.hostdesign24tools.core.valueobjects.ColorVO;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class NaturalColors {
	@Expose
	private List<ColorVO> nhcWithNhs = new ArrayList<>();
}
