package com.aem.hostdesign24tools.core.excel;

/**
 * @author HostDesign24.com
 *
 */
public class ExcelReaderException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExcelReaderException(Throwable cause) {
		super(cause);
	}

	public ExcelReaderException(String string) {
		super(string);
	}
}
