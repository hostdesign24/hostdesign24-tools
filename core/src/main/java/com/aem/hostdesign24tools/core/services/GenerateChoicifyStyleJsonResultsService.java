package com.aem.hostdesign24tools.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

/**
 * @author HostDesign24.com
 *
 */
public interface GenerateChoicifyStyleJsonResultsService {

	public void processJsonResult(ResourceResolver resourceResolver, SlingHttpServletRequest request);
}
