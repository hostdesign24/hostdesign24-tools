<%@include file="/apps/hostDesign24/global.jsp"%>
<cq:includeClientLib categories="cq.jquery" />
<cq:includeClientLib categories="choicify.component.tools.colors" />
<choicifysling:adaptTo adaptable="${resource}" adaptTo="com.aem.hostdesign24tools.core.models.ChoicifyResultsModel" var="choicify" />
<div class="coral--light choicify-tools-page">
	<div id="choicify-tools-app">
		<div class="page" role="main">
			<div class="content">
				<div class="content-container">
					<div class="content-container-inner">
						<div>
							<h1>Upload Metadata CSV File</h1>
							<!-- The form starts -->
							<form action="/" method="post" enctype="multipart/form-data" id="importMetaData">
								<!-- The file to upload -->
								<input name="_charset_" type="hidden" value="utf-8">
								<p>
									<input id="file-id" type="file" name="productsFile" />
								</p>
								<p>
									<input type="checkbox" id="deleteImportedContent"
										name="cleannContent" value="coding"> <label
										for="coding">Delete Imported Colors</label>
								</p>
								
								<h2>Select the brand you want to upload products</h2>
								<coral-select id="importbrand" name="importbrand" placeholder="Choose brand" required>
                                	<c:forEach var="type" items="${choicify.brands}" varStatus="status">
								  		<coral-select-item value="${type}">${type}</coral-select-item>
								  	</c:forEach>
								</coral-select>
								
								<coral-select id="importcountry" name="importcountry" placeholder="Choose country" required>
                                	<c:forEach var="type" items="${choicify.countries}" varStatus="status">
								  		<coral-select-item value="${type}">${type}</coral-select-item>
								  	</c:forEach>
								</coral-select>
								<br/><br/>
								
								<button is="coral-button" id="upload-button-id" variant="primary" iconsize="M" size="M">Upload</button>
								
							</form>

						</div>
						<div class="content-hana-results ">
							<h4>Last import: ${choicify.modificationTime}</h4>
							<h3>Successfull Products</h3>
							<table>
								<tr>
									<th>Counter</th>
									<th>Gtin</th>
									<th>Product Title</th>
									<th>Page Path</th>
									<th>isPageActivated</th>
								</tr>
								<c:forEach var="child" items="${choicify.successResults}" varStatus="status">
								     <tr>
	                                    <td>${status.index}</td>
										<td>${child.gtin}</td>
										<td>${child.productTitle}</td>
										<td>${child.pagePath}</td>
										<td>${child.pageActivated}</td>
									 </tr>
								</c:forEach>
							</table>
						</div>
						<div class="content-aem-results ">
							<h3>No Products</h3>
							<table>
								<tr>
									<th>Counter</th>
									<th>Gtin</th>
									<th>Product Title</th>
									<th>Page Path</th>
									<th>isPageActivated</th>
								</tr>
								<c:forEach var="child" items="${choicify.noProductResults}" varStatus="status">
								     <tr>
	                                    <td>${status.index}</td>
										<td>${child.gtin}</td>
										<td>${child.productTitle}</td>
										<td>${child.pagePath}</td>
										<td>${child.pageActivated}</td>
									 </tr>
								</c:forEach>
							</table>
						</div>
						<div class="content-aem-results ">
							<h3>Failed Products</h3>
							<table>
								<tr>
									<th>Counter</th>
									<th>Gtin</th>
									<th>Product Title</th>
									<th>Page Path</th>
									<th>isPageActivated</th>
								</tr>
								<c:forEach var="child" items="${choicify.failureResults}" varStatus="status">
								     <tr>
	                                    <td>${status.index}</td>
										<td>${child.gtin}</td>
										<td>${child.productTitle}</td>
										<td>${child.pagePath}</td>
										<td>${child.pageActivated}</td>
									 </tr>
								</c:forEach>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
tbody, tr, td, th {border:1px solid;}
</style>