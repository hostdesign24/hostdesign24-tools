package com.aem.hostdesign24tools.core.valueobjects;

import java.io.InputStream;
import javax.jcr.Session;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.ChoicifyReplicationService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author HostDesign24.com
 *
 */
public class ThreadVO {

	private InputStream stream;
	private ResourceResolver resourceResolver;
	private Session session;
	private Boolean deleteAction;
	private ChoicifyAppService choicifyAppService;
	private ChoicifyReplicationService choicifyReplicationService;
	private boolean naturalColors;
	private boolean targetColors;
	private boolean products;
	private boolean selectorLevelOne;
	private boolean selectorLevelTwo;
	private boolean setSelectorLevelThree;
	private boolean selectorLevelFour;
	private boolean allSearch;
	@Getter
	@Setter
	private String country;
	@Getter
	@Setter
	private String brand;

	public InputStream getStream() {
		return stream;
	}

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Boolean getDeleteAction() {
		return deleteAction;
	}

	public void setDeleteAction(Boolean deleteAction) {
		this.deleteAction = deleteAction;
	}

	public ChoicifyAppService getChoicifyAppService() {
		return choicifyAppService;
	}

	public void setChoicifyAppService(ChoicifyAppService choicifyAppService) {
		this.choicifyAppService = choicifyAppService;
	}

	public ChoicifyReplicationService getChoicifyReplicationService() {
		return choicifyReplicationService;
	}

	public void setChoicifyReplicationService(ChoicifyReplicationService choicifyReplicationService) {
		this.choicifyReplicationService = choicifyReplicationService;
	}

	public boolean isTargetColors() {
		return targetColors;
	}

	public boolean isProductSearch() {
		return products;
	}

	public boolean isNaturalColors() {
		return naturalColors;
	}

	public void setNaturalColors(boolean naturalColors) {
		this.naturalColors = naturalColors;
	}

	public void setTargetcolors(boolean targetColors) {
		this.targetColors = targetColors;
	}

	public void setProducts(boolean products) {
		this.products = products;
	}

	public void setSelectorLevelOne(boolean selectorLevelOne) {
		this.selectorLevelOne = selectorLevelOne;
	}

	public void setSelectorLevelTwo(boolean selectorLevelTwo) {
		this.selectorLevelTwo = selectorLevelTwo;
	}

	public void setSelectorLevelThree(boolean selectorLevelThree) {
		this.setSelectorLevelThree = selectorLevelThree;
	}

	public void setSelectorLevelFour(boolean selectorLevelFour) {
		this.selectorLevelFour = selectorLevelFour;
	}

	public boolean isSelectorLevelTwo() {
		return this.selectorLevelTwo;
	}

	public boolean isSelectorLevelFourth() {
		return this.selectorLevelFour;
	}

	public boolean isSelectorLevelOne() {
		return selectorLevelOne;
	}

	public boolean isSelectorLevelThree() {
		return setSelectorLevelThree;
	}

	public boolean isAllSearch() {
		return this.allSearch;
	}

	public void setAllSearch(boolean allSearch) {
		this.allSearch = allSearch;
	}

}
