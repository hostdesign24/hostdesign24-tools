package com.aem.hostdesign24tools.core.valueobjects.styling;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.StringUtils;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class GroupItem {

	@Expose
	private String key;

	@Expose
	private String name;

	@Expose
	private List<Item> items = new ArrayList<>();

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GroupItem) {
			return StringUtils.equals(((GroupItem) obj).getKey(), this.key);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.key.hashCode();
	}

}
