package com.aem.hostdesign24tools.core.services.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.nodetype.NodeType;
import javax.jcr.query.Query;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.enums.ChoicifyBrandType;
import com.aem.hostdesign24tools.core.excel.ExcelReaderException;
import com.aem.hostdesign24tools.core.excel.PoiExcelReader;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.ImportStyleProductsService;
import com.aem.hostdesign24tools.core.utils.NodeUtils;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.valueobjects.CsvProductImportVO;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.tagging.Tag;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(ImportStyleProductsService.class)
@Component(metatype = false, immediate = true, label = "Choicify Style Products Import Service")
public class ImportStyleProductsServiceImpl implements ImportStyleProductsService {

	private static final String QUERY_STRING =
			"SELECT * FROM [nt:unstructured] as ref WHERE ISDESCENDANTNODE(ref,[%s]) AND ref.[gtin] IS NOT NULL";

	@Reference
	private ChoicifyAppService choicifyAppService;

	/**
	 * Execute thread importer Service.
	 */
	@Override
	public void process(InputStream inputStream, ResourceResolver resourceResolver,
			SlingHttpServletRequest request) {
		Session session = null;
		try {
			log.debug("ImportProductsService :: START: processing products from Excel file");
			session = resourceResolver.adaptTo(Session.class);

			createProducts(inputStream, session, resourceResolver, request);

			log.debug("ImportProductsService :: END: processing products from Excel file is finished");
		} catch (ExcelReaderException e) {
			log.error("ExcelReaderException", e);
		} finally {
			if (session != null && session.isLive()) {
				session.logout();
			}
			ResourceResolverUtils.close(resourceResolver);
			IOUtils.closeQuietly(inputStream);
		}
	}


	private void createProducts(InputStream inputStream, Session session,
			ResourceResolver resourceResolver, SlingHttpServletRequest request)
			throws ExcelReaderException {
		String cleannContent = request.getParameter("cleannContent");
		String brand = request.getParameter("importcountry");
		if (StringUtils.isBlank(brand)) {
			brand = ChoicifyBrandType.DM.getCountryType();
		}
		boolean deleteAction =
				StringUtils.isNotBlank(cleannContent) && StringUtils.equals("true", cleannContent);
		List<CsvProductImportVO> successProducts = new ArrayList<>();
		List<CsvProductImportVO> rejectedProducts = new ArrayList<>();

		Map<String, Resource> products = new HashMap<>();

		// set all existing products for a division
		setProducts(products, resourceResolver, brand);

		// read workbook
		Workbook workbook = PoiExcelReader.read(inputStream);
		Sheet sheet = workbook.getSheetAt(0);
		CsvProductImportVO productImportVO;

		int rowcount = 0;

		String categoryLocalLanguage = StringUtils.EMPTY;
		String categoryLanguage = StringUtils.EMPTY;

		for (Row row : sheet) {
			productImportVO = new CsvProductImportVO();
			// skip for header row
			if (rowcount == 0) {

				categoryLocalLanguage = StringUtils.substringBetween(row.getCell(4).toString(), "$$", "$$");
				categoryLanguage = StringUtils.substringBetween(row.getCell(5).toString(), "$$", "$$");

				rowcount++;
				continue;
			}
			log.info("processing row {}", rowcount);
			// read all columns from row
			Cell gtin = row.getCell(0);
			Cell brandtype = row.getCell(1);
			Cell country = row.getCell(2);
			Cell language = row.getCell(3);
			Cell categoryLocal = row.getCell(4);
			Cell category = row.getCell(5);
			Cell brandTitle = row.getCell(6);
			Cell productLineTitle = categoryLocal;
			Cell productTitleLocal = row.getCell(7);
			Cell productTitle = row.getCell(8);
			Cell imagePath = row.getCell(9);
			Cell productUrl = row.getCell(10);

			// fallback case
			if (productLineTitle == null) {
				productLineTitle = brandTitle;
			}

			gtin.setCellType(Cell.CELL_TYPE_STRING);

			if (StringUtils.isBlank(gtin.toString())) {
				log.debug("CSV file content has empty line.");
				break;
			}

			// setting the values
			productImportVO.setGtin(gtin.toString());
			productImportVO.setBrand(brand);
			productImportVO.setBrandType(brandtype.toString());
			productImportVO.setCountry(country.toString());
			productImportVO.setLanguage(language.toString());
			productImportVO.setCategoryLocal(categoryLocal != null ? categoryLocal.toString() : "");

			productImportVO.setCategory(category.toString());
			productImportVO.setBrandTitle(brandTitle.toString());
			productImportVO.setProductLineTitle(productLineTitle.toString());
			productImportVO.setProductTitle(productTitle.toString());
			productImportVO.setProductTitleLocal(productTitleLocal.toString());

			productImportVO.setImagePath(imagePath != null ? imagePath.toString() : "");
			productImportVO.setProductUrl(productUrl != null ? productUrl.toString() : "");

			rowcount++;
			if (!isCsvFileContentValid(productImportVO)) {
				log.debug("CSV file content has empty line.");
				break;
			}
			if (StringUtils.isEmpty(productImportVO.getGtin())) {
				log.debug("GTin is not avaliable");
				productImportVO.setStatus("Error: GTIN is not avaliable");
				rejectedProducts.add(productImportVO);
				continue;
			}

			if (products.containsKey(productImportVO.getGtin())) {
				if (deleteAction) {
					deleteProductPage(products.get(productImportVO.getGtin()), productImportVO,
							rejectedProducts, successProducts);
				} else {
					productImportVO
							.setStatus("Error: Product already exists for the Gtin:" + productImportVO.getGtin());
					rejectedProducts.add(productImportVO);
				}
			} else if (!deleteAction) {
				log.debug("Product page is creating for {}", productImportVO.getGtin());
				createProductPage(session, resourceResolver, productImportVO, rejectedProducts,
						successProducts);

				// create i18n texts
				createI18nTranslations(session, productImportVO, categoryLocalLanguage, categoryLanguage);
			}
		}
	}


	private void createI18nTranslations(Session session, CsvProductImportVO productImportVO,
			String categoryLocalLanguage, String categoryLanguage) {
		try {
			String baseI18nPath = new StringBuilder("/apps/hostDesign24/styletranslations").toString();
			JcrUtils.getOrCreateByPath(baseI18nPath, false, JcrConstants.NT_FOLDER,
					JcrConstants.NT_FOLDER, session, true);
			String brandI18nPath =
					new StringBuilder(baseI18nPath).append("/").append(productImportVO.getBrand()).toString();
			Node brandNode = JcrUtils.getOrCreateByPath(brandI18nPath, false, JcrConstants.NT_FOLDER,
					JcrConstants.NT_FOLDER, session, true);
			if (brandNode != null) {
				String i18nPath = new StringBuilder(brandI18nPath).append("/i18n").toString();
				// check if path exist or not.
				JcrUtils.getOrCreateByPath(i18nPath, false, "sling:Folder", "sling:Folder", session, true);

				// for local language
				String i18nLanguagePath = new StringBuilder(i18nPath).append("/")
						.append(StringUtils.lowerCase(categoryLocalLanguage)).toString();

				Node i18nLanguageNode = JcrUtils.getOrCreateByPath(i18nLanguagePath, false,
						JcrConstants.NT_UNSTRUCTURED, "sling:Folder", session, true);
				if (!i18nLanguageNode.hasProperty(JcrConstants.JCR_LANGUAGE)) {
					i18nLanguageNode.setProperty(JcrConstants.JCR_LANGUAGE,
							StringUtils.lowerCase(categoryLocalLanguage));
					i18nLanguageNode.addMixin(NodeType.MIX_LANGUAGE);
					i18nLanguageNode.save();
				}
				// category title
				String categorymessageKey = JcrUtil.createValidName(productImportVO.getCategory());
				String i18nmessage =
						new StringBuilder(i18nLanguagePath).append("/").append(categorymessageKey).toString();
				Node i18nmessageNode = JcrUtils.getOrCreateByPath(i18nmessage, false,
						JcrConstants.NT_UNSTRUCTURED, "sling:MessageEntry", session, true);
				i18nmessageNode.setProperty("sling:message", productImportVO.getCategoryLocal());
				i18nmessageNode.setProperty("sling:key", categorymessageKey);

				// product title
				String productMessageKey = JcrUtil.createValidName(productImportVO.getProductTitle());
				i18nmessage =
						new StringBuilder(i18nLanguagePath).append("/").append(productMessageKey).toString();
				i18nmessageNode = JcrUtils.getOrCreateByPath(i18nmessage, false,
						JcrConstants.NT_UNSTRUCTURED, "sling:MessageEntry", session, true);
				i18nmessageNode.setProperty("sling:message", productImportVO.getProductTitleLocal());
				i18nmessageNode.setProperty("sling:key", productMessageKey);

				// for other language
				i18nLanguagePath = new StringBuilder(i18nPath).append("/")
						.append(StringUtils.lowerCase(categoryLanguage)).toString();
				i18nLanguageNode = JcrUtils.getOrCreateByPath(i18nLanguagePath, false,
						JcrConstants.NT_UNSTRUCTURED, "sling:Folder", session, true);
				if (!i18nLanguageNode.hasProperty(JcrConstants.JCR_LANGUAGE)) {
					i18nLanguageNode.setProperty(JcrConstants.JCR_LANGUAGE,
							StringUtils.lowerCase(categoryLanguage));
					i18nLanguageNode.addMixin(NodeType.MIX_LANGUAGE);
					i18nLanguageNode.save();
				}
				// category title
				i18nmessage =
						new StringBuilder(i18nLanguagePath).append("/").append(categorymessageKey).toString();
				i18nmessageNode = JcrUtils.getOrCreateByPath(i18nmessage, false,
						JcrConstants.NT_UNSTRUCTURED, "sling:MessageEntry", session, true);
				i18nmessageNode.setProperty("sling:message", productImportVO.getCategory());
				i18nmessageNode.setProperty("sling:key", categorymessageKey);

				// product title
				i18nmessage =
						new StringBuilder(i18nLanguagePath).append("/").append(productMessageKey).toString();
				i18nmessageNode = JcrUtils.getOrCreateByPath(i18nmessage, false,
						JcrConstants.NT_UNSTRUCTURED, "sling:MessageEntry", session, true);
				i18nmessageNode.setProperty("sling:message", productImportVO.getProductTitle());
				i18nmessageNode.setProperty("sling:key", productMessageKey);

				brandNode.save();

			}
		} catch (RepositoryException ex) {
			log.error("RepositoryException {}", ex);
		}
	}

	private void deleteProductPage(final Resource resource, final CsvProductImportVO productImportVO,
			final List<CsvProductImportVO> rejectedProducts,
			final List<CsvProductImportVO> successProducts) {

		try {
			if (null != resource) {
				Node node = NodeUtils.getNode(resource.getParent().getParent());
				NodeUtils.deleteNode(node);
				productImportVO.setStatus("Deleted");
				productImportVO
						.setProductPagePath(node.getPath() + HostDesign24AppConstants.HTML_EXTENSION);
				successProducts.add(productImportVO);
			}
		} catch (RepositoryException e) {
			log.error("RepositoryException in deleteProductPage", e);
			productImportVO.setStatus(
					"Error: In Deleting the existing product with the Gtin:" + productImportVO.getGtin());
			rejectedProducts.add(productImportVO);
		}
	}

	private boolean hasValidContent(final CsvProductImportVO productImportVO) {
		if (StringUtils.isEmpty(productImportVO.getGtin())
				|| StringUtils.isEmpty(productImportVO.getBrand())
				|| StringUtils.isEmpty(productImportVO.getBrandType())
				|| StringUtils.isEmpty(productImportVO.getCountry())
				|| StringUtils.isEmpty(productImportVO.getLanguage())
				|| StringUtils.isEmpty(productImportVO.getCategory())
				|| StringUtils.isEmpty(productImportVO.getBrandTitle())
				|| StringUtils.isEmpty(productImportVO.getProductLineTitle())
				|| StringUtils.isEmpty(productImportVO.getProductTitle())) {
			return false;
		}
		return true;
	}

	private void createProductPage(Session session, ResourceResolver resourceResolver,
			final CsvProductImportVO productImportVO, final List<CsvProductImportVO> rejectedProducts,
			final List<CsvProductImportVO> successProducts) {

		boolean error = false;
		if (!hasValidContent(productImportVO)) {
			log.debug("Error: Product with Gtin has empty values {}", productImportVO.getGtin());
			productImportVO
					.setStatus("Error: Product with Gtin has empty values:" + productImportVO.getGtin());
			rejectedProducts.add(productImportVO);
			return;
		}

		String productPath = new StringBuilder(HostDesign24AppConstants.CHOICIFY_PRODUCT_ROOT_PATH)
				.append(productImportVO.getBrand()).append("/").append(productImportVO.getBrandType())
				.append("/").append(productImportVO.getCountry()).append("/")
				.append(productImportVO.getLanguage()).append("/products/")
				.append(productImportVO.getCategoryLocal()).append("/")
				.append(productImportVO.getBrandTitle()).append("/")
				.append(productImportVO.getProductLineTitle()).toString();

		Map<String, String> folderProperties = new HashMap<>();
		folderProperties.put(HostDesign24AppConstants.CQ_TEMPLATE_PROPERTY,
				HostDesign24AppConstants.TEMPLATE_FOLDER);
		folderProperties.put(HostDesign24AppConstants.SLING_RESOURCETYPE,
				HostDesign24AppConstants.RESOURCETYPE_FOLDER);
		folderProperties.put(HostDesign24AppConstants.HIDE_IN_NAVIGATION, "true");

		try {
			Node productLine = NodeUtils.createNodeStructureFromPath(productPath, session.getRootNode(),
					HostDesign24AppConstants.CQ_PAGE_NODE, folderProperties);

			// create product page
			if (null != productLine) {
				Map<String, String> productProperties = new HashMap<>();
				productProperties.put(HostDesign24AppConstants.CQ_TEMPLATE_PROPERTY,
						HostDesign24AppConstants.TEMPLATE_PRODUCT);
				productProperties.put(HostDesign24AppConstants.SLING_RESOURCETYPE,
						HostDesign24AppConstants.RESOURCETYPE_PRODUCT);
				productProperties.put(HostDesign24AppConstants.HIDE_IN_NAVIGATION, "true");

				String productPageName =
						productImportVO.getProductTitle() + "_" + productImportVO.getGtin();
				Node productPage = NodeUtils.createNode(productLine, productPageName,
						HostDesign24AppConstants.CQ_PAGE_NODE, productProperties);
				Node jcrNode = NodeUtils.getJcrNode(productPage);

				if (null != jcrNode) {
					Node product = jcrNode.addNode(HostDesign24AppConstants.JCR_NODE_NAME_PRODUCT,
							HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED);
					product.setProperty(HostDesign24AppConstants.CHOICIFY_GTIN_NAME,
							productImportVO.getGtin());
					product.setProperty(HostDesign24AppConstants.CHOICIFY_PRODUCT_URL,
							productImportVO.getProductUrl());
					product.setProperty(HostDesign24AppConstants.CHOICIFY_PRODUCT_PAGE_PATH,
							productPage.getPath() + HostDesign24AppConstants.HTML_EXTENSION);
					product.setProperty(HostDesign24AppConstants.BRAND_TITLE,
							productImportVO.getBrandTitle());

					product.setProperty("categoryTitle",
							JcrUtil.createValidName(productImportVO.getCategory()));
					product.setProperty("productTitle",
							JcrUtil.createValidName(productImportVO.getProductTitle()));

					// store image path
					Node packshot = product.addNode(HostDesign24AppConstants.JCR_NODE_NAME_PACKSHOT,
							HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED);
					packshot.setProperty(HostDesign24AppConstants.CHOICIFY_FILE_REFERENCE,
							productImportVO.getImagePath());
					productImportVO
							.setProductPagePath(productPage.getPath() + HostDesign24AppConstants.HTML_EXTENSION);

					session.save();
					// create filters
					storeFilters(productImportVO, product.getPath(), session, resourceResolver);
					session.save();
					// set status
					productImportVO.setStatus("Created");

					successProducts.add(productImportVO);
				} else {
					error = true;
				}

			} else {
				error = true;
			}

		} catch (RepositoryException e) {
			log.error("RepositoryException in creatingProductPage", e);
			error = true;
		}

		if (error) {
			log.debug("Error: In creating the product with the Gtin {}", productImportVO.getGtin());
			productImportVO.setStatus("Gtin already exist");
			rejectedProducts.add(productImportVO);
		}
	}

	private void storeFilters(final CsvProductImportVO productImportVO, String productPath,
			Session session, ResourceResolver resourceResolver) throws RepositoryException {

		String filterPath = productPath + HostDesign24AppConstants.CHOICIFY_ROOT_PATH;
		// fall back: in case filters doesn't exist
		JcrUtils.getOrCreateByPath(filterPath, false, JcrConstants.NT_UNSTRUCTURED,
				JcrConstants.NT_UNSTRUCTURED, session, true);
		filterPath = filterPath + HostDesign24AppConstants.STRING_SEPERATOR_SLASH
				+ HostDesign24AppConstants.JSON_NAME_PRODUCT_FILTER;
		JcrUtils.getOrCreateByPath(filterPath, false, JcrConstants.NT_UNSTRUCTURED,
				JcrConstants.NT_UNSTRUCTURED, session, true);
		String brandsTagPath = filterPath + HostDesign24AppConstants.STRING_SEPERATOR_SLASH
				+ HostDesign24AppConstants.BRANDS;
		Node brandTagNode = JcrUtils.getOrCreateByPath(brandsTagPath, false,
				JcrConstants.NT_UNSTRUCTURED, JcrConstants.NT_UNSTRUCTURED, session, true);

		String brandTag = NodeUtils.createValidTagName(productImportVO.getBrandTitle());
		String tagPath = HostDesign24AppConstants.TAG_BRANDS_ROOT_PATH + brandTag;
		Resource resource = resourceResolver.getResource(tagPath);
		if (resource != null) {
			Tag tag = resource.adaptTo(Tag.class);
			String tagValue = choicifyAppService.createTagValueStoredInJcrContent(tag);
			brandTagNode.setProperty(HostDesign24AppConstants.BRANDS, tagValue);
		}
	}

	private boolean isCsvFileContentValid(CsvProductImportVO productImportVO) {
		if (StringUtils.isEmpty(productImportVO.getGtin())
				&& StringUtils.isEmpty(productImportVO.getCountry())
				&& StringUtils.isEmpty(productImportVO.getLanguage())
				&& StringUtils.isEmpty(productImportVO.getCategory())
				&& StringUtils.isEmpty(productImportVO.getBrandTitle())
				&& StringUtils.isEmpty(productImportVO.getProductLineTitle())
				&& StringUtils.isEmpty(productImportVO.getProductTitle())) {
			return false;
		}
		return true;
	}

	/**
	 * Method sets the product resource
	 *
	 * return Map<String, String>
	 */
	private void setProducts(final Map<String, Resource> products,
			final ResourceResolver resourceResolver, final String brand) {

		String queryString =
				String.format(QUERY_STRING, HostDesign24AppConstants.CHOICIFY_PRODUCT_ROOT_PATH + brand
						+ "/" + HostDesign24AppConstants.CHOICIFY_HAIRSTYLE_BRAND);
		Iterator<Resource> iterator = resourceResolver.findResources(queryString, Query.JCR_SQL2);
		while (iterator.hasNext()) {
			final Resource resource = iterator.next();
			final Node childNode = resource.adaptTo(Node.class);

			if (null == childNode) {
				continue;
			}

			String gtin = NodeUtils.getStringPropertyFromNode(childNode,
					HostDesign24AppConstants.CHOICIFY_GTIN_NAME);

			if (StringUtils.isNotBlank(gtin)) {
				products.put(gtin, resource);
			}

		}

	}

}
