package com.aem.hostdesign24tools.core.valueobjects;

import org.apache.commons.lang3.StringUtils;
import com.aem.hostdesign24tools.core.enums.ChoicifyBrandType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author HostDesign24.com
 *
 */
public class CsvImportVO {

	@Getter
	@Setter
	private String nhc;
	@Getter
	@Setter
	private String nhs;
	@Getter
	@Setter
	private String thc;
	@Getter
	@Setter
	private String ths;
	@Getter
	@Setter
	private int productPriority;
	@Getter
	@Setter
	private String naturalHairColor;
	@Getter
	@Setter
	private String naturalHairShade;
	@Getter
	@Setter
	private String targetHairColor;
	@Getter
	@Setter
	private String targetHairShade;
	@Getter
	@Setter
	private String gtin;
	@Getter
	@Setter
	private String productTitle;
	@Getter
	@Setter
	private String brandCompany;
	@Getter
	@Setter
	private String durability;
	@Getter
	@Setter
	private String pagePath;
	private boolean isPageActivated;
	private String country;
	@Getter
	@Setter
	private String brand;
	@Getter
	@Setter
	private String colorTile;

	public String getCountry() {
		if (StringUtils.isBlank(country)) {
			country = ChoicifyBrandType.DM.getCountryType();
		}
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isPageActivated() {
		return isPageActivated;
	}

	public void setPageActivated(boolean isPageActivated) {
		this.isPageActivated = isPageActivated;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CsvImportVO) {
			return StringUtils.equals(((CsvImportVO) obj).getGtin(), gtin);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getGtin().hashCode();
	}

}
