package com.aem.hostdesign24tools.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.map.HashedMap;

/**
 * @author HostDesign24.com
 *
 */
public class ColorUtils {

	public Map<Integer, List<String>> iterateColorCombinations(String[] elems) {
		Map<Integer, List<String>> allCombinations = new HashedMap();
		int n = elems.length;
		for (int num = 0; num < (1 << n); num++) {
			List<String> combination = new ArrayList<>();
			for (int ndx = 0; ndx < n; ndx++) {
				// (is the bit "on" in this number?)
				if ((num & (1 << ndx)) != 0) {
					// then it's included in the list
					combination.add(elems[ndx]);
				}
			}
			allCombinations.put(num, combination);
		}
		return allCombinations;
	}
}
