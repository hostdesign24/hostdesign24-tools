package com.aem.hostdesign24tools.core.services.impl;

import javax.jcr.Session;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.hostdesign24tools.core.services.ChoicifyReplicationService;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;

/**
 * @author HostDesign24.com
 *
 */
@Service(ChoicifyReplicationService.class)
@Component(metatype = false, immediate = true, label = "Choicify Replication Service")
public class ChoicifyReplicationServiceImpl implements ChoicifyReplicationService {
  private static final Logger LOGGER =
      LoggerFactory.getLogger(ChoicifyReplicationServiceImpl.class);

  @Reference
  protected Replicator replicator;

  public void replicate(Session session, String path, ReplicationActionType actiontype)
      throws ReplicationException {
    if (StringUtils.isNotEmpty(path)) {
      LOGGER.debug("Replication Path {}", path);
      replicator.replicate(session, actiontype, path);
    }
  }
}
