package com.aem.hostdesign24tools.core.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.osgi.PropertiesUtil;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.HostDesign24ScrappingToolService;
import com.aem.hostdesign24tools.core.valueobjects.CsvProductImportVO;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(HostDesign24ScrappingToolService.class)
@Component(metatype = true, immediate = true, label = "Choicify Scrapping Tool Service")
public class HostDesign24ScrappingToolServiceImpl implements HostDesign24ScrappingToolService {

  @Property(unbounded = PropertyUnbounded.ARRAY, label = "Category URL Mapping", cardinality = 50,
      description = "Please enter the category url mapping")
  private static final String URL_MAPPER = "urlmapper";

  @Property(unbounded = PropertyUnbounded.ARRAY, label = "Illegal Products", cardinality = 50,
      description = "Please enter the code of Illegal Products")
  private static final String ILLEGAL_PRODUCTS = "illegalproducts";

  private String[] multiUrlMapper;
  private String[] multiIllegalProducts;
  private List<CsvProductImportVO> results;

  @Activate
  @Modified
  protected void activate(Map<String, Object> properties) {
    log.info("[*** ChoicifyScrappingToolServiceImpl: activating configuration service");
    readProperties(properties);
  }

  protected void readProperties(Map<String, Object> properties) {
    this.multiUrlMapper = PropertiesUtil.toStringArray(properties.get(URL_MAPPER));
    this.multiIllegalProducts = PropertiesUtil.toStringArray(properties.get(ILLEGAL_PRODUCTS));
  }

  /**
   * Execute thread importer Service.
   */
  @Override
  public void process(ResourceResolver resourceResolver, SlingHttpServletRequest request) {
    Session session = null;

    session = resourceResolver.adaptTo(Session.class);
    processSchedularInputs(this.multiUrlMapper, session);
  }

  public void processSchedularInputs(String[] categoryList, Session session) {
    log.info("START: processing products gtins from Excel file");
    try {
      results = new ArrayList<>();
      for (int i = 0; i < categoryList.length; i++) {
        String[] urlcateogory = categoryList[i].split("#");
        processResults(urlcateogory[0], urlcateogory[1]);
      }

      saveScrappingToolResults(session, results);
    } catch (Exception e) {
      log.error("Error: processing products gtins from Excel file" + e.getMessage());
    }
    log.info("END: processing products gtins from Excel file");
  }

  private void saveScrappingToolResults(Session session, List<CsvProductImportVO> results2)
      throws RepositoryException {

    String results = new Gson().toJson(results2);

    Node resultNode = JcrUtils.getOrCreateByPath("/var/hostDesign24/scrappingPath", false,
        HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED,
        HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED, session, true);

    if (resultNode.hasProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS)) {
      resultNode.getProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS).remove();
      resultNode.getSession().save();
    }
    resultNode.setProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, results);

    JcrUtils.setLastModified(resultNode, Calendar.getInstance());
    session.save();
  }

  private void processResults(String url, String category) throws IOException {
    URL obj = new URL(url.toString());
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    con.setRequestMethod("GET");
    con.setRequestProperty("User-Agent", "Mozilla/5.0");
    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();
    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    in.close();

    JSONObject output;
    try {
      output = new JSONObject(response.toString());
      JSONArray jsonArray = output.getJSONArray("serviceProducts");
      log.error("Json array for category" + category.toString() + "is" + jsonArray);
      System.setProperty("file.encoding", "UTF-8");
      Field charset = Charset.class.getDeclaredField("defaultCharset");
      charset.setAccessible(true);
      charset.set(null, null);
      for (int i = 0; i < jsonArray.length(); i++) {
        CsvProductImportVO eachRow = new CsvProductImportVO();

        JSONObject explrObject = jsonArray.getJSONObject(i);
        eachRow.setGtin(explrObject.getString("gtin"));
        eachRow.setCountry(explrObject.getString("isoCountry"));
        eachRow.setLanguage(explrObject.getString("isoLanguage"));
        eachRow.setCategory(category);
        byte[] bytearray = explrObject.getString("brand").getBytes("UTF-8");
        String value = new String(bytearray, "UTF-8");
        eachRow.setBrandTitle(explrObject.getString("brand"));
        eachRow.setProductLineTitle(category.toString());
        if (explrObject.has("netQuantityContent")) {
          eachRow.setProductTitle(explrObject.getString("name") + ","
              + Math.round(Float.valueOf(explrObject.getString("netQuantityContent"))) + ""
              + explrObject.getString("contentUnit"));
        } else {
          eachRow.setProductTitle(explrObject.getString("name"));
        }
        eachRow.setProductUrl("https://www.dm.de"
            + explrObject.getJSONArray("links").getJSONObject(2).getString("href"));
        eachRow.setImagePath(explrObject.getJSONArray("links").getJSONObject(0).getString("href"));
        if (!Arrays.asList(this.multiIllegalProducts).contains(explrObject.getString("gtin"))) {
          results.add(eachRow);
        }
      }

    } catch (Exception ex) {
      log.error("Exception {}", ex);
    }
  }

}
