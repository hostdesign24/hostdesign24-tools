package com.aem.hostdesign24tools.core.services.runnable;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.excel.ExcelReaderException;
import com.aem.hostdesign24tools.core.excel.PoiExcelReader;
import com.aem.hostdesign24tools.core.utils.NodeUtils;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.valueobjects.CsvImportVO;
import com.aem.hostdesign24tools.core.valueobjects.ThreadVO;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.search.result.Hit;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

/**
 * Service for processing CSV file containing Chades and Colors.
 *
 * @author HostDesign24.com
 *
 */
public class ChoicifyMetadataImporterService implements Runnable {

	private static final Logger LOGGER =
			LoggerFactory.getLogger(ChoicifyMetadataImporterService.class);

	@Getter
	@Setter
	private ThreadVO threadVO;

	public Map<String, List<CsvImportVO>> processSpreadSheet(InputStream inputStream,
			ResourceResolver resourceResolver, Session session, boolean deleteAction, final String brand,
			final String country) throws RepositoryException, ReplicationException, ExcelReaderException {
		Map<String, List<CsvImportVO>> mapProducts = new HashMap<>();
		Map<String, List<CsvImportVO>> mapResults = new HashMap<>();
		List<CsvImportVO> publishedPages = new ArrayList<>();
		List<CsvImportVO> rejectedPages = new ArrayList<>();
		List<CsvImportVO> noProductFound = new ArrayList<>();

		Workbook workbook = PoiExcelReader.read(inputStream);
		Sheet sheet = workbook.getSheetAt(0);

		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<String> replicationPagePaths = new ArrayList<>();

		int rowcount = 0;
		for (Row row : sheet) {
			// skip for header row
			if (rowcount == 0) {
				rowcount++;
				continue;
			}
			rowcount++;
			LOGGER.info("Processing line {}", rowcount);
			CsvImportVO csvImportVO = readCsvFileContent(row, brand, country);
			if (!isCsvFileContentValid(csvImportVO)) {
				LOGGER.debug("CSV file content has empta line.");
				break;
			}
			if (StringUtils.isEmpty(csvImportVO.getGtin())) {
				LOGGER.debug("GTin is not avaliable");
				continue;
			}
			createJcrTagPropertyValue(csvImportVO, resourceResolver);
			List<Hit> searchResult = getThreadVO().getChoicifyAppService().searchProductPageByGtins(
					resourceResolver, HostDesign24AppConstants.CHOICIFY_GTIN_NAME, csvImportVO);

			List<Resource> resources =
					getThreadVO().getChoicifyAppService().getResourcesFromSearchResult(searchResult);
			if (resources != null && resources.isEmpty()) {
				LOGGER.debug("Product not available for given Gtin {}", csvImportVO.getGtin());
				noProductFound.add(csvImportVO);
				continue;
			}
			for (Resource resource : resources) {
				Node productPage = resource.adaptTo(Node.class);

				csvImportVO.setPagePath(pageManager.getContainingPage(resource).getPath());
				if (!publishPage(pageManager, resource) && !StringUtils.contains(resource.getPath(),
						HostDesign24AppConstants.CHOICIFY_PRODUCT_ROOT_PATH)) {
					LOGGER.debug("Create Shade and colors only for activated pages {} ", resource.getPath());
					csvImportVO.setPageActivated(false);
					rejectedPages.add(csvImportVO);
					continue;
				}
				if (productPage != null) {
					String productPath = productPage.getPath() + "/product";
					String colorationPaths = productPath + HostDesign24AppConstants.CHOICIFY_ROOT_PATH
							+ HostDesign24AppConstants.COLORATIONPATHS;
					if (deleteAction) {
						getThreadVO().getChoicifyReplicationService().replicate(session, colorationPaths,
								ReplicationActionType.DEACTIVATE);
						deleteNode(colorationPaths, resourceResolver, session);
					} else {
						Resource productResource = resourceResolver.getResource(productPath);
						if (productResource != null) {
							Node productNode = productResource.adaptTo(Node.class);
							if (productNode != null) {
								if (productNode.hasProperty("productTitle")) {
									if (StringUtils.isBlank(productNode.getProperty("productTitle").getString()))
										productNode.setProperty("productTitle", csvImportVO.getProductTitle());
								} else {
									productNode.setProperty("productTitle", csvImportVO.getProductTitle());
								}
							}
						}
						storeFilters(csvImportVO, productPath, session, resourceResolver);
						Node tagNode = createNodeForShadeAndColorTags(csvImportVO, mapProducts, productPath,
								resourceResolver, session, deleteAction);
						storeShadesAndColors(csvImportVO, tagNode);
					}
				}
				String replicationPath = resource.getPath() + HostDesign24AppConstants.CHOICIFY_ROOT_PATH;
				replicationPagePaths.add(replicationPath);
				csvImportVO.setPageActivated(true);
				publishedPages.add(csvImportVO);
			}

		}
		session.save();
		LOGGER.info("Metadata import has been finished");
		// remove duplicates
		if (publishedPages != null && !publishedPages.isEmpty()) {
			publishedPages = publishedPages.stream().distinct().collect(Collectors.toList());
		}
		if (noProductFound != null && !noProductFound.isEmpty()) {
			noProductFound = noProductFound.stream().distinct().collect(Collectors.toList());
		}
		if (rejectedPages != null && !rejectedPages.isEmpty()) {
			rejectedPages = rejectedPages.stream().distinct().collect(Collectors.toList());
		}

		mapResults.put("failure", rejectedPages);
		mapResults.put("success", publishedPages);
		mapResults.put("noProduct", noProductFound);
		saveChoicifyResults(session, mapResults);
		LOGGER.info("Metadata import has been finished :: saving the results");

		if (deleteAction) {
			deactivateCreatedColors(session, replicationPagePaths);
		} else {
			replicateCreatedColors(session, replicationPagePaths);
		}

		return mapResults;
	}

	private void saveChoicifyResults(Session session, Map<String, List<CsvImportVO>> mapResults)
			throws RepositoryException {
		String results = new Gson().toJson(mapResults);
		Node resultNode =
				JcrUtils.getOrCreateByPath(HostDesign24AppConstants.CHOICIFY_METADATA_RESULT_PATH, false,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED, session, true);

		if (resultNode.hasProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS)) {
			resultNode.getProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS).remove();
			resultNode.getSession().save();
		}
		resultNode.setProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, results);
		JcrUtils.setLastModified(resultNode, Calendar.getInstance());
		session.save();
	}

	private void replicateCreatedColors(Session session, List<String> replicationPagePaths)
			throws ReplicationException {
		Set<String> pagePathsWithoutDuplicates = new LinkedHashSet<>(replicationPagePaths);
		LOGGER.debug("Start Replication.");
		for (String pagePath : pagePathsWithoutDuplicates) {
			getThreadVO().getChoicifyReplicationService().replicate(session, pagePath,
					ReplicationActionType.ACTIVATE);
		}
		LOGGER.debug("Finish replication.");
	}

	private void deactivateCreatedColors(Session session, List<String> replicationPagePaths)
			throws ReplicationException {
		Set<String> pagePathsWithoutDuplicates = new LinkedHashSet<>(replicationPagePaths);
		LOGGER.debug("Start Replication.");
		for (String pagePath : pagePathsWithoutDuplicates) {
			getThreadVO().getChoicifyReplicationService().replicate(session, pagePath,
					ReplicationActionType.DEACTIVATE);
		}
		LOGGER.debug("Finish replication.");
	}

	/**
	 * Verify iif page should be activated and colors and shade should also be created.
	 *
	 * @param pageManager {@link PageManager}
	 * @param resource {@link Resource}
	 * @return {@link Boolean}
	 */
	private boolean publishPage(PageManager pageManager, Resource resource) {
		Page page = pageManager.getContainingPage(resource);
		if (page == null) {
			return false;
		}
		ReplicationStatus replicationStatus = page.adaptTo(ReplicationStatus.class);
		if (replicationStatus == null) {
			return false;
		}
		return replicationStatus.isActivated();
	}

	/**
	 * Store Shades And Colors under product node.
	 *
	 * @param csvImportVO {@link CsvImportVO}
	 * @param tagNode {@link Node}
	 * @throws RepositoryException {@link RepositoryException}
	 */
	private void storeShadesAndColors(CsvImportVO csvImportVO, Node tagNode)
			throws RepositoryException {
		if (tagNode == null) {
			return;
		}
		if (StringUtils.isNotEmpty(csvImportVO.getNaturalHairColor())) {
			tagNode.setProperty(HostDesign24AppConstants.NATURAL_HAIR_COLORS_PROP_NAME,
					csvImportVO.getNaturalHairColor());
		}
		if (StringUtils.isNotEmpty(csvImportVO.getNaturalHairShade())) {
			tagNode.setProperty(HostDesign24AppConstants.NATURAL_HAIR_SHADES_PROP_NAME,
					csvImportVO.getNaturalHairShade());
		}
		if (StringUtils.isNotEmpty(csvImportVO.getTargetHairColor())) {
			tagNode.setProperty(HostDesign24AppConstants.TARGET_HAIR_COLORS_PROP_NAME,
					csvImportVO.getTargetHairColor());
		}
		if (StringUtils.isNotEmpty(csvImportVO.getTargetHairShade())) {
			tagNode.setProperty(HostDesign24AppConstants.TARGET_HAIR_SHADES_PROP_NAME,
					csvImportVO.getTargetHairShade());
		}
		tagNode.setProperty(HostDesign24AppConstants.PRODUCT_PRIORITY,
				csvImportVO.getProductPriority());
		tagNode.setProperty(HostDesign24AppConstants.COLOR_TILE, csvImportVO.getColorTile());
	}

	private void storeFilters(final CsvImportVO csvImportVO, String productPath, Session session,
			ResourceResolver resourceResolver) throws RepositoryException {

		String filterPath = productPath + HostDesign24AppConstants.CHOICIFY_ROOT_PATH;

		// fall back: in case filters doesn't exist
		JcrUtils.getOrCreateByPath(filterPath, false, "nt:unstructured", "nt:unstructured", session,
				true);
		filterPath = filterPath + HostDesign24AppConstants.STRING_SEPERATOR_SLASH
				+ HostDesign24AppConstants.JSON_NAME_PRODUCT_FILTER;
		JcrUtils.getOrCreateByPath(filterPath, false, "nt:unstructured", "nt:unstructured", session,
				true);

		String durabilityTagPath = filterPath + HostDesign24AppConstants.STRING_SEPERATOR_SLASH
				+ HostDesign24AppConstants.CHOICIFY_TAG_DURABILITY_NAME;
		Node durabilityTagNode = JcrUtils.getOrCreateByPath(durabilityTagPath, false, "nt:unstructured",
				"nt:unstructured", session, true);

		String durabilityTag = NodeUtils.createValidTagName(csvImportVO.getDurability());
		String tagPath = HostDesign24AppConstants.TAG_DURABILITY_ROOT_PATH + durabilityTag;
		Resource resource = resourceResolver.getResource(tagPath);
		if (resource != null) {
			Tag tag = resource.adaptTo(Tag.class);
			String tagValue = getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(tag);
			durabilityTagNode.setProperty(HostDesign24AppConstants.CHOICIFY_TAG_DURABILITY_NAME,
					tagValue);
		}
		session.save();
	}

	/**
	 * Create jcr node for Shades And Colors under product node.
	 *
	 * @param csvImportVO {@link CsvImportVO}
	 * @param mapProducts {@link List}t
	 * @param productPath {@link String}
	 * @param resourceResolver {@link ResourceResolver}
	 * @param session {@link Session}
	 * @param deleteAction {@link Boolean}
	 * @throws AccessDeniedException {@link AccessDeniedException}
	 * @throws VersionException {@link VersionException}
	 * @throws LockException {@link LockException}
	 * @throws ConstraintViolationException {@link ConstraintViolationException}
	 * @throws RepositoryException {@link RepositoryException}
	 */
	private Node createNodeForShadeAndColorTags(CsvImportVO csvImportVO,
			Map<String, List<CsvImportVO>> mapProducts, String productPath,
			ResourceResolver resourceResolver, Session session, boolean deleteAction)
			throws RepositoryException {
		if (StringUtils.isEmpty(csvImportVO.getNaturalHairColor())
				&& StringUtils.isEmpty(csvImportVO.getNaturalHairShade())
				&& StringUtils.isEmpty(csvImportVO.getTargetHairColor())
				&& StringUtils.isEmpty(csvImportVO.getTargetHairShade())) {
			return null;
		}

		String tagNodeName = csvImportVO.getNhc() + "_" + csvImportVO.getNhs() + "_"
				+ csvImportVO.getThc() + "_" + csvImportVO.getThs();

		String tagRootPath = HostDesign24AppConstants.PRODUCT_COLORATIONS_PATH;
		String tagPath = productPath + tagRootPath + tagNodeName;
		if (deleteAction) {
			deleteNode((productPath + HostDesign24AppConstants.CHOICIFY_ROOT_PATH), resourceResolver,
					session);
			return null;
		}

		deleteExistingNode((productPath + tagRootPath), csvImportVO, mapProducts, resourceResolver,
				session);
		createCsvImporterObject(csvImportVO, mapProducts);
		return JcrUtils.getOrCreateByPath(tagPath, false, "nt:unstructured", "nt:unstructured", session,
				true);
	}

	/**
	 * @param tagPath {@link String}
	 * @param csvImportVO {@link CsvImportVO}
	 * @param mapProducts {@link List}t
	 * @param resourceResolver {@link ResourceResolver}
	 * @param deleteAction {@link Boolean}
	 * @throws AccessDeniedException {@link AccessDeniedException}
	 * @throws VersionException {@link VersionException}
	 * @throws LockException {@link LockException}
	 * @throws ConstraintViolationException {@link ConstraintViolationException}
	 * @throws RepositoryException {@link RepositoryException}
	 */
	private void deleteExistingNode(String tagPath, CsvImportVO csvImportVO,
			Map<String, List<CsvImportVO>> mapProducts, ResourceResolver resourceResolver,
			Session session) throws RepositoryException {

		if (mapProducts.containsKey(csvImportVO.getGtin())) {
			return;
		}
		deleteNode(tagPath, resourceResolver, session);
	}

	/**
	 * @param tagPath {@link String}
	 * @param session {@link Session}
	 * @param resourceResolver {@link ResourceResolver}
	 * @throws AccessDeniedException {@link AccessDeniedException}
	 * @throws VersionException {@link VersionException}
	 * @throws LockException {@link LockException}
	 * @throws ConstraintViolationException {@link ConstraintViolationException}
	 * @throws RepositoryException {@link RepositoryException}
	 */
	private void deleteNode(String tagPath, ResourceResolver resourceResolver, Session session)
			throws RepositoryException {
		if (resourceResolver.getResource(tagPath) != null) {
			session.removeItem(tagPath);
			session.save();
		}
	}

	/**
	 * Create tag property values for jcr node.
	 *
	 * @param csvImportVO {@link CsvImportVO}
	 * @param resourceResolver {@link ResourceResolver}
	 */
	private void createJcrTagPropertyValue(CsvImportVO csvImportVO,
			ResourceResolver resourceResolver) {
		String nhcPath = HostDesign24AppConstants.ROOT_PATH_NATURAL_HAIR_COLOR
				+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getNhc();
		Resource naturalHaircolorResource = resourceResolver.getResource(nhcPath);
		Resource naturalHairShadeResource = null;
		if (naturalHaircolorResource != null) {
			Tag nhcTag = naturalHaircolorResource.adaptTo(Tag.class);
			csvImportVO.setNaturalHairColor(
					getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(nhcTag));
			String nhsPath = naturalHaircolorResource.getPath()
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getNhs();
			naturalHairShadeResource = resourceResolver.getResource(nhsPath);
		} else {
			csvImportVO.setNaturalHairColor(null);
		}

		if (naturalHairShadeResource != null) {
			Tag nhsTag = naturalHairShadeResource.adaptTo(Tag.class);
			csvImportVO.setNaturalHairShade(
					getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(nhsTag));
		} else {
			csvImportVO.setNaturalHairShade(null);
		}

		String thcPath = HostDesign24AppConstants.ROOT_PATH_TARGET_HAIR_COLOR
				+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getThc();
		Resource targetHairColorResource = resourceResolver.getResource(thcPath);
		Resource targetHairShadeResource = null;
		if (targetHairColorResource != null) {
			Tag thcTag = targetHairColorResource.adaptTo(Tag.class);
			csvImportVO.setTargetHairColor(
					getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(thcTag));
			String thsPath = targetHairColorResource.getPath()
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getThs();
			targetHairShadeResource = resourceResolver.getResource(thsPath);
		} else {
			csvImportVO.setTargetHairColor(null);
		}

		if (targetHairShadeResource != null) {
			Tag thsTag = targetHairShadeResource.adaptTo(Tag.class);
			csvImportVO.setTargetHairShade(
					getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(thsTag));
		} else {
			csvImportVO.setTargetHairShade(null);
		}
	}

	/**
	 * Read the content of csv file and store in an onject.
	 *
	 * @param sheet {@link Sheet}
	 * @param index {@link Integer}
	 * @return {@link CsvImportVO}
	 */
	private CsvImportVO readCsvFileContent(Row row, final String brandShop, String country) {
		CsvImportVO csvImportVO = new CsvImportVO();
		Cell nhc = row.getCell(0);
		Cell nhs = row.getCell(1);
		Cell thc = row.getCell(2);
		Cell ths = row.getCell(3);
		Cell priority = row.getCell(4);
		Cell brand = row.getCell(5);
		Cell productTitle = row.getCell(6);
		Cell gtin = row.getCell(7);
		Cell durability = row.getCell(8);
		Cell colorTile = row.getCell(9);

		csvImportVO.setCountry(country);
		csvImportVO.setNhc(nhc.toString());
		csvImportVO.setNhs(nhs.toString());
		csvImportVO.setThc(thc.toString());
		csvImportVO.setThs(ths.toString());
		csvImportVO.setBrand(brand.toString());
		csvImportVO.setProductTitle(productTitle.toString());
		csvImportVO.setDurability(durability.toString());
		if (colorTile != null) {
			csvImportVO.setColorTile(colorTile.toString());
		}

		if (priority != null && StringUtils.isNotBlank(priority.toString())) {
			Double doubleValue = Double.parseDouble(priority.toString());
			csvImportVO.setProductPriority(doubleValue.intValue());
		}
		gtin.setCellType(Cell.CELL_TYPE_STRING);

		csvImportVO.setGtin(gtin.toString());
		csvImportVO.setBrandCompany(brandShop);
		return csvImportVO;
	}

	/**
	 * Read CSV file content and if line is empty, return false.
	 *
	 * @param csvImportVO {@link CsvImportVO}
	 * @return {@link Boolean}
	 */
	private boolean isCsvFileContentValid(CsvImportVO csvImportVO) {
		if (StringUtils.isEmpty(csvImportVO.getNhc()) && StringUtils.isEmpty(csvImportVO.getNhs())
				&& StringUtils.isEmpty(csvImportVO.getThc()) && StringUtils.isEmpty(csvImportVO.getThs())) {
			return false;
		}
		return true;
	}

	/**
	 * Create CSV Importer Object for json out put.
	 *
	 * @param csvImportVO {@link CsvImportVO}
	 * @param mapProducts {@link List}
	 */
	private void createCsvImporterObject(CsvImportVO csvImportVO,
			Map<String, List<CsvImportVO>> mapProducts) {
		List<CsvImportVO> csvImportVOs = new ArrayList<>();
		if (mapProducts.containsKey(csvImportVO.getGtin())) {
			csvImportVOs = mapProducts.get(csvImportVO.getGtin());
		} else {
			mapProducts.put(csvImportVO.getGtin(), csvImportVOs);
		}
		if (!csvImportVOs.contains(csvImportVO)) {
			csvImportVOs.add(csvImportVO);
		}
	}

	/**
	 * Exercute thread importer Service.
	 */
	private void exercute() {
		try {
			processSpreadSheet(getThreadVO().getStream(), getThreadVO().getResourceResolver(),
					getThreadVO().getSession(), getThreadVO().getDeleteAction(), getThreadVO().getBrand(),
					getThreadVO().getCountry());
		} catch (Exception ex) {
			LOGGER.error("Exception: ", ex);
		} finally {
			if (getThreadVO().getSession() != null && getThreadVO().getSession().isLive()) {
				getThreadVO().getSession().logout();
			}
			ResourceResolverUtils.close(getThreadVO().getResourceResolver());
			IOUtils.closeQuietly(getThreadVO().getStream());
		}
	}

	public void run() {
		exercute();
	}

}
