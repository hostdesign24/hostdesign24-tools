package com.aem.hostdesign24tools.core.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.rmi.ServerException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.Session;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpStatus;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.HostDesign24ScrappingToolForMenService;
import com.aem.hostdesign24tools.core.services.HostDesign24ScrappingToolService;
import com.aem.hostdesign24tools.core.services.ScrappingToolForICAService;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.valueobjects.CsvProductImportVO;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@SlingServlet(resourceTypes = "sling/servlet/default",
		selectors = {"scrappingtool", "scrappingtoolformen", "scrappingtoolforica"},
		extensions = "json", methods = {"POST", "GET"})
public class HostDesign24ProductsScrappingTool extends AbstractAllMethodsServlet {

	private static final long serialVersionUID = -6978585149802445252L;

	@Reference
	private HostDesign24ScrappingToolService choicifyScrappingToolService;

	@Reference
	private HostDesign24ScrappingToolForMenService scrappingToolForMenService;

	@Reference
	private ScrappingToolForICAService scrappingToolForICAService;

	private ResourceResolver resourceResolver;


	private static String[] headerColumns = {"Gtin", "Country", "Language", "Category", "Brand",
			"ProductLine", "Product Title	", "Packshot URL", "Product URL"};

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		response.setContentType("application/vnd.ms-excel");
		response.setCharacterEncoding("UTF-8");
		String fileName = null;
		String resultsPath = HostDesign24AppConstants.SCRAPPING_PRODUCTS_PATH_FOR_WOMEN;
		final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
		if (selectors.contains("scrappingtoolformen")) {
			fileName = "Products_ScrappingTool_ForMen_Report.xlsx";
			resultsPath = HostDesign24AppConstants.SCRAPPING_PRODUCTS_PATH_FOR_MEN;
		} else if (selectors.contains("scrappingtool")) {
			fileName = "Products_ScrappingTool_Report.xlsx";

		} else if (selectors.contains("scrappingtoolforica")) {
			resultsPath = HostDesign24AppConstants.SCRAPPING_PRODUCTS_PATH_FOR_ICA;
			fileName = "Products_ScrappingTool_ICA_Report.xlsx";

		} else {
			response.setStatus(HttpStatus.SC_METHOD_NOT_ALLOWED);
			return;
		}
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		OutputStream out = response.getOutputStream();

		try {
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet healthCheckStatus = workbook.createSheet("Product List");
			// Create header row
			Row headerRow = healthCheckStatus.createRow(0);
			// Creating Header cells
			for (int i = 0; i < headerColumns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(headerColumns[i]);
			}

			// write here
			resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
			Resource varResource = resourceResolver.getResource(resultsPath);
			if (varResource != null) {
				ValueMap valueMap = varResource.adaptTo(ValueMap.class);
				if (valueMap != null) {
					String results =
							valueMap.get(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, String.class);
					List<CsvProductImportVO> scrappingToolList = new Gson().fromJson(results,
							new TypeToken<ArrayList<CsvProductImportVO>>() {}.getType());
					if (scrappingToolList != null && !scrappingToolList.isEmpty()) {
						int rowNum = 1;
						String defaultCharacterEncoding = System.getProperty("file.encoding");
						log.debug("defaultCharacterEncoding by property: " + defaultCharacterEncoding);
						System.setProperty("file.encoding", "UTF-8");
						for (CsvProductImportVO productstatus : scrappingToolList) {
							Row row = healthCheckStatus.createRow(rowNum++);
							row.createCell(0).setCellValue(productstatus.getGtin());
							row.createCell(1).setCellValue(productstatus.getCountry());
							row.createCell(2).setCellValue(productstatus.getLanguage());
							row.createCell(3).setCellValue(productstatus.getCategory());
							row.createCell(4).setCellValue(productstatus.getBrandTitle());
							row.createCell(5).setCellValue(productstatus.getProductLineTitle());
							row.createCell(6).setCellValue(productstatus.getProductTitle());
							row.createCell(7).setCellValue(productstatus.getImagePath());
							row.createCell(8).setCellValue(productstatus.getProductUrl());
						}
					}
				}
			}
			workbook.write(out);
			out.flush();
		} catch (LoginException ex) {
			log.error("LoginException while reading ResourceResolver {}", ex);
		} finally {
			ResourceResolverUtils.close(resourceResolver);
			out.close();
		}
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		response.setContentType("text/plain");
		try {

			final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
			resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
			Session session = resourceResolver.adaptTo(Session.class);
			String nodepath = "/apps/hostDesign24/components/scrappingtool/timestamp";
			if (selectors.contains("scrappingtoolformen")) {
				scrappingToolForMenService.process(resourceResolver, request);
				nodepath = "/apps/hostDesign24/components/scrappingtoolformen/timestamp";

			} else if (selectors.contains("scrappingtool")) {
				choicifyScrappingToolService.process(resourceResolver, request);

			}
			if (selectors.contains("scrappingtoolforica")) {
				scrappingToolForICAService.process(resourceResolver);
				nodepath = "/apps/hostDesign24/components/scrappingtoolforica/timestamp";

			} else {
				response.setStatus(HttpStatus.SC_METHOD_NOT_ALLOWED);
				return;
			}

			Node node = JcrUtil.createPath(nodepath, JcrConstants.NT_UNSTRUCTURED, session);
			node.setProperty("time",
					"Created at "
							+ new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())
							+ "(yyyyMMdd_HHmmss)");
			if (session != null && session.isLive()) {
				session.save();
			}
			response.setStatus(200);
			response.getWriter().println("Creation of product list completed, please download report!");
			response.getWriter().close();
		} catch (Exception ex) {
			log.error("LoginException while reading ResourceResolver {}", ex);
		} finally {
			ResourceResolverUtils.close(resourceResolver);
		}
	}

}
