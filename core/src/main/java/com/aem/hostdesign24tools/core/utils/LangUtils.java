package com.aem.hostdesign24tools.core.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;

/**
 * @author HostDesign24.com
 *
 */
public class LangUtils {

	private static final String LANGUAGE_CODE_PATTERN = "(/[a-z]{2}/)";
	private static final String LANGUAGE_CODE_SELECTOR_PATTERN = "(.(de.json|en.json))";

	public static String getLanguageCodeFromSelector(SlingHttpServletRequest request) {
		return LangUtils.getLanguageCodeFromSelector(request.getRequestURI());
	}

	public static String getCountryFromSelector(SlingHttpServletRequest request,
			final List<String> availablesites) {
		return LangUtils.getCountryFromSelector(request.getRequestURI(), availablesites);
	}

	public static String getLanguageCode(String path) {

		Pattern pattern = Pattern.compile(LANGUAGE_CODE_PATTERN);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String langCode = matcher.group(1);
			if (StringUtils.isNotBlank(langCode)) {
				langCode = StringUtils.substringBetween(langCode, "/");
			}
			return langCode;
		}
		return StringUtils.EMPTY;
	}

	public static String getLanguageCodeFromSelector(String path) {

		Pattern pattern = Pattern.compile(LANGUAGE_CODE_SELECTOR_PATTERN);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String langCode = matcher.group(1);
			if (StringUtils.isNotBlank(langCode)) {
				langCode = StringUtils.substringBetween(langCode, ".");
			}
			return langCode;
		} else {
			return getLanguageCode(path);
		}
	}

	public static String getCountryFromSelector(String path, final List<String> availablesites) {
		String pattenString = "(.(" + StringUtils.join(availablesites, "|") + ").)";
		Pattern pattern = Pattern.compile(pattenString);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String countryCode = matcher.group(1);
			if (StringUtils.isNotBlank(countryCode)) {
				countryCode = StringUtils.substringBetween(countryCode, ".");
			}
			return countryCode;
		} else {
			return "dm";
		}
	}
}
