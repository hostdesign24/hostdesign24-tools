package com.aem.hostdesign24tools.core.valueobjects.styling;

import org.apache.commons.codec.binary.StringUtils;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class Item {

	@Expose
	private String key;

	@Expose
	private String name;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Item) {
			return StringUtils.equals(((Item) obj).getKey(), this.key);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.key.hashCode();
	}

}
