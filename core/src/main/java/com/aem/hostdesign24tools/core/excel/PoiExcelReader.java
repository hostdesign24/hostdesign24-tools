package com.aem.hostdesign24tools.core.excel;

/**
 * @author HostDesign24.com
 *
 */
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class PoiExcelReader {
	public static Workbook read(InputStream input) throws ExcelReaderException {
		try {
			return WorkbookFactory.create(input);
		} catch (Exception e) {
			throw new ExcelReaderException(e);
		}
	}
}
