package com.aem.hostdesign24tools.core.valueobjects.styling;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class Hold {

	@Expose
	private List<Item> hold = new ArrayList<>();
}
