package com.aem.hostdesign24tools.core.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.ChoicifyHealthCheckService;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.valueobjects.ProductHealthCheckVO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author HostDesign24.com
 *
 */
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = {"healthcheck"},
    extensions = "json", methods = {"POST", "GET"})
public class ChoicifyProductsHealthCheck extends AbstractAllMethodsServlet {

  protected final Logger LOGGER = LoggerFactory.getLogger(ChoicifyProductsHealthCheck.class);

  private static final long serialVersionUID = -6978585149802445252L;

  @Reference
  private ChoicifyAppService choicifyAppService;

  @Reference
  private ChoicifyHealthCheckService choicifyHealthCheckService;

  private ResourceResolver resourceResolver;

  private static String[] headerColumns = {"Initial Color Family", "Initial Shade Name",
      "Desired Color Family", "Desired Shade Name", "GTIN Henkel", "isProductPageExist",
      "isPublished", "hasChoicify", "isColorCombinationExist", "Product Path"};

  @Override
  protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
      throws ServerException, IOException {
    response.setContentType("application/vnd.ms-excel");
    String fileName = "Products_HealthCheck_Report.xlsx";
    response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
    OutputStream out = response.getOutputStream();
    try {
      // download as workbook or excel sheet
      @SuppressWarnings("resource")
      XSSFWorkbook workbook = new XSSFWorkbook();
      XSSFSheet healthCheckStatus = workbook.createSheet("Health Check Status");
      // Create header row
      Row headerRow = healthCheckStatus.createRow(0);
      // Creating Header cells
      for (int i = 0; i < headerColumns.length; i++) {
        Cell cell = headerRow.createCell(i);
        cell.setCellValue(headerColumns[i]);
      }

      // write here
      resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
      String path = HostDesign24AppConstants.CHOICIFY_HEALTHCHECK_RESULT_PATH;
      Resource varResource = resourceResolver.getResource(path);
      ValueMap valueMap = varResource.adaptTo(ValueMap.class);
      String results = valueMap.get(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, String.class);

      List<ProductHealthCheckVO> productHealthCheckVOList = new Gson().fromJson(results,
          new TypeToken<ArrayList<ProductHealthCheckVO>>() {}.getType());
      if (productHealthCheckVOList != null && !productHealthCheckVOList.isEmpty()) {
        int rowNum = 1;

        for (ProductHealthCheckVO productstatus : productHealthCheckVOList) {
          Row row = healthCheckStatus.createRow(rowNum++);
          row.createCell(0).setCellValue(productstatus.getInitialColorFamily());
          row.createCell(1).setCellValue(productstatus.getInitialShadeName());
          row.createCell(2).setCellValue(productstatus.getDesiredColorFamily());
          row.createCell(3).setCellValue(productstatus.getDesiredShadeName());
          row.createCell(4, CellType.NUMERIC).setCellValue(productstatus.getGtin());
          row.createCell(5).setCellValue(productstatus.isProductPageExist());
          row.createCell(6).setCellValue(productstatus.isPublished());
          row.createCell(7).setCellValue(productstatus.isHasChoicify());
          row.createCell(8).setCellValue(productstatus.isColorCombinationExist());
          row.createCell(9).setCellValue(productstatus.getProductPath());
        }
      }

      workbook.write(out);
      out.flush();
    } catch (LoginException ex) {
      LOGGER.error("LoginException while reading ResourceResolver {}", ex);
    } finally {
      ResourceResolverUtils.close(resourceResolver);
      out.close();
    }
  }

  @Override
  protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
      throws ServerException, IOException {
    response.setContentType("text/plain");
    try {

      final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
      if (!ServletFileUpload.isMultipartContent(request)) {
        response.setStatus(500);
        LOGGER.debug("Internal server error: is not MultipartContent");
        response.getWriter()
            .println(String.format("Internal server error: is not MultipartContent"));
        response.getWriter().close();
        return;
      }
      resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
      final Map<String, RequestParameter[]> params = request.getRequestParameterMap();
      for (final Map.Entry<String, RequestParameter[]> pairs : params.entrySet()) {
        RequestParameter[] pArr = pairs.getValue();
        RequestParameter param = pArr[0];
        InputStream inputStream = param.getInputStream();
        if (inputStream == null) {
          LOGGER.error("File not properly uploaded :: Selectors are {}" + selectors);
          response.setStatus(405);
          response.getWriter().println(String.format("File not properly uploaded"));
          response.getWriter().close();
          return;
        }
        choicifyHealthCheckService.process(inputStream, resourceResolver, request);
      }
      response.setStatus(200);
      response.getWriter().println(
          String.format("Products Health Check has been finished, please download report"));
      response.getWriter().close();
    } catch (LoginException ex) {
      LOGGER.error("LoginException while reading ResourceResolver {}", ex);
    }
  }

}
