package com.aem.hostdesign24tools.core.valueobjects.styleformen;

import java.util.List;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class ServiceProduct {

	private String gtin;
	private String name;
	private String brandName;
	private String isoLanguage;
	private String isoCountry;
	private boolean isActive;
	private boolean visibleForShopSearch;
	private boolean notDeliverable;
	private boolean notAvailable;
	private boolean purchasable;
	private String productNameUrlSegment;
	private String price;
	private String priceLocalized;
	private String brand;
	private String netQuantityContent;
	private String contentUnit;
	private List<Link> links;
}
