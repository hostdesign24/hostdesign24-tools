package com.aem.hostdesign24tools.core.valueobjects.ica;

import java.util.List;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class Store {

	private String id;
	private String name;
	private List<String> deliveryMethods;
	private List<String> customerTypes;
	private String slug;
	private String storeFormat;
}
