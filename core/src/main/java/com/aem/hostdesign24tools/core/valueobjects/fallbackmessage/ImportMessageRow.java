package com.aem.hostdesign24tools.core.valueobjects.fallbackmessage;

import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class ImportMessageRow {
	@Expose
	private String initialnhs;
	@Expose
	private String initialths;

	@Expose
	private String recommendednhs;
	@Expose
	private String recommendedths;

	@Expose
	private String messageInLocalLanguage;
	@Expose
	private String messageInOtherLanguage;
}
