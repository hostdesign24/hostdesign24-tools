<%@include file="/apps/hostDesign24/global.jsp"%>
<cq:includeClientLib categories="cq.jquery" />
<cq:includeClientLib categories="choicify.component.tools.healthcheck" />
<!-- <choicifysling:adaptTo adaptable="${resource}" adaptTo="com.aem.hostdesign24tools.core.models.ProductsHealthCheckModel" var="choicify" /> -->
<div class="coral--light choicify-tools-page">
	<div id="choicify-tools-app">
		<div class="page" role="main">
			<div class="content">
				<div class="content-container">
					<div class="content-container-inner">
						<div>
							<h2>Health check</h2>
							<p id="support-notice">Your browser does not support Ajax
								uploads :-(The form will be submitted as normal.</p>
							<!-- The form starts -->
							<form action="/" method="POST" enctype="multipart/form-data"
								id="form-id">
								<!-- The file to upload -->
								<p>
									<input id="file-id" type="file" name="our-file" />
								</p>

								<!-- filters -->
								<p>
									<input type="checkbox" name="unpublishedProducts" value="true">
									Unpublished Products
								</p>
								<p>
									<input type="checkbox" name="notExistingProducts" value="true">
									Not Existing Products
								</p>
								<p>
									<input type="checkbox" name="noChoicifyColors" value="true">
									Not having Choicify Colors
								</p>

								<p>
									<input type="button" value="Upload" id="upload-button-id"
										disabled="disabled" />
								</p>
								<!-- Placeholders for messages set by event handlers -->
								<p id="upload-status"></p>
								<p id="csvImporterProgress"></p>
								<p id="csvImporterResultSuccess"></p>
								<p id="csvImporterResultFailue"></p>
							</form>
						</div>
						<a x-cq-linkchecker="skip"
							href="/etc/choicifytools/productimporter.healthcheck.json"
							class="healthCheckDownload">Download</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
tbody, tr, td, th {
	border: 1px solid;
}
</style>