(function(document, $) {
	"use strict";
	var URL_BASE =  "/etc/choicifytools/metadataimporter";
	var GET_COUNTRYS_PATH = URL_BASE + ".getcolorscountries.json";
	
	$(document).on("change", "#importbrand", function(e) {
		 var brandlist = document.querySelector('#importbrand');
		 var importcountry = document.querySelector('#importcountry');
		 importcountry.items.clear();
		 
		 $.ajax({
	            type: "GET",
	            url: Granite.HTTP.externalize(GET_COUNTRYS_PATH+"?brand="+brandlist.selectedItem.value),
	            processData : false,
	            contentType : false,
	            success: function (data) {
	            	
	            	data.forEach(function(item){
	            		importcountry.items.add({
	            	        value: item,
	            	        content: {
	            	          textContent: item
	            	        }
	            	      });
	            	});
	            }
	        });
	 });
	
	$(document).on("change", "#importcountry", function(e) {
		
	 });
	
	$(document).on("click", "button#upload-button-id", function(e) {
        var form = $("form#importMetaData");
        // stop the event propagation
        e.preventDefault();
        var formdata = new FormData(form[0]);
        
        var selector = "metadataImporter";
		if ($('#deleteImportedContent').is(':checked')) {
			selector = "deleteMetadata"
		}
		var url = '/etc/choicifytools/metadataimporter.'
				+ selector + '.json';
        
        $.ajax({
            type: "POST",
            url: Granite.HTTP.externalize(url),
            data: formdata,
            processData : false,
            contentType : false,
            success: function (data) {
            },
            error: function (xmlhttprequest, textStatus, message) {
            }
        });
    });
	
})(document, Granite.$);