package com.aem.hostdesign24tools.core.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.models.ColorationModel;
import com.aem.hostdesign24tools.core.models.ProductModel;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.utils.ColorUtils;
import com.aem.hostdesign24tools.core.utils.I18nUtils;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.utils.SearchUtils;
import com.aem.hostdesign24tools.core.valueobjects.ColorVO;
import com.aem.hostdesign24tools.core.valueobjects.CsvImportVO;
import com.aem.hostdesign24tools.core.valueobjects.Filter;
import com.aem.hostdesign24tools.core.valueobjects.FilterOption;
import com.aem.hostdesign24tools.core.valueobjects.ShadeVO;
import com.aem.hostdesign24tools.core.valueobjects.naturalcolors.NaturalColors;
import com.aem.hostdesign24tools.core.valueobjects.productsearch.ProductResult;
import com.aem.hostdesign24tools.core.valueobjects.productsearch.ProductSearchModel;
import com.aem.hostdesign24tools.core.valueobjects.productsearch.ProductTag;
import com.aem.hostdesign24tools.core.valueobjects.targetcolors.TargetColors;
import com.aem.hostdesign24tools.core.valueobjects.targetnaturalcolors.TargetNaturalColors;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(ChoicifyAppService.class)
@Component(metatype = true, immediate = true, label = "Choicify Product Search Service")
public class ChoicifyAppServiceImpl implements ChoicifyAppService {

	@Property(label = "Allowed brands", unbounded = PropertyUnbounded.ARRAY, cardinality = 50,
			description = "Please enter the brand name")
	private static final String CHOICIFY_DEFAULT_SITES = "choicify.domain.allowed.sites";

	@Property(label = "Default root path of Tags (naturalHairColor)", value = "")
	private static final String TAGS_NATURAL_COLORS_PATH = "tags.natural.colors.path";

	@Property(label = "Default root path of Tags (targetHairColor)", value = "")
	private static final String TAGS_TARGET_COLORS_PATH = "tags.target.colors.path";

	@Property(label = "Choicify root path of Site", value = "")
	private static final String CHOICIFY_SEARCH_PAGE_PATH = "choicify.search.page.path";

	@Property(label = "Default Domain name (https://schwarzkopf.de)", value = "")
	private static final String CHOICIFY_DOMAIN_NAME = "choicify.domain.name";

	@Property(label = "Target Hair Shade GUIDs", unbounded = PropertyUnbounded.ARRAY,
			cardinality = 50, description = "Please enter target hair shade GUIDs")
	private static final String CHOICIFY_TARGET_HAIR_SHADE_GUIDs = "choicify.target.hair.shade.guids";

	private String[] allowedsites;
	private String[] targetHairShadeGUIDs;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Getter
	private String naturalHairColorsPath;

	@Getter
	private String targetHairColorsPath;

	@Getter
	private String choicifySearchPath;

	private String choicifyDomainName;

	@Activate
	@Modified
	protected void activate(final ComponentContext componentContext) {
		Dictionary<?, ?> properties = componentContext.getProperties();
		try {
			this.naturalHairColorsPath =
					PropertiesUtil.toString(properties.get(TAGS_NATURAL_COLORS_PATH), "");
			this.targetHairColorsPath =
					PropertiesUtil.toString(properties.get(TAGS_TARGET_COLORS_PATH), "");
			this.choicifyDomainName = PropertiesUtil.toString(properties.get(CHOICIFY_DOMAIN_NAME), "");
			this.choicifySearchPath =
					PropertiesUtil.toString(properties.get(CHOICIFY_SEARCH_PAGE_PATH), "");
			this.allowedsites = PropertiesUtil.toStringArray(properties.get(CHOICIFY_DEFAULT_SITES));
			this.targetHairShadeGUIDs =
					PropertiesUtil.toStringArray(properties.get(CHOICIFY_TARGET_HAIR_SHADE_GUIDs));
		} catch (Exception ex) {
			log.error("Error while reading properties", ex);
		}
	}

	@Override
	public Map<String, String> getTargetHairShadeGUIDs() {
		return Arrays.asList(targetHairShadeGUIDs).stream().map(shadeGuid -> shadeGuid.split(":"))
				.filter(shadeGuid -> shadeGuid.length == 2).collect(Collectors.toMap(e -> e[0], e -> e[1]));
	}

	@Override
	public List<String> getAllowedsites() {
		return Arrays.asList(allowedsites);
	}

	@Override
	public List<String> getAvailableCountrys() {
		List<String> availableCountrys = new ArrayList<>();
		List<String> brands = getAllowedsites();
		for (String brand : brands) {
			String defaultPath =
					String.format(HostDesign24AppConstants.CHOICIFY_COLORS_DEFAULT_PATH, brand);
			List<String> countrys = getChildNodes(defaultPath);
			if (!countrys.isEmpty()) {
				availableCountrys.addAll(countrys);
			}
		}
		return availableCountrys;
	}

	@Override
	public List<String> getChildNodes(final String path) {
		ResourceResolver resourceResolver = null;
		List<String> availableCountrys = new ArrayList<>();
		try {
			resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
			Resource resource = resourceResolver.getResource(path);
			if (resource != null) {
				Iterator<Resource> childResources = resource.listChildren();
				while (childResources.hasNext()) {
					Resource childResource = childResources.next();
					if (childResource.getName().equals("jcr:content")) {
						continue;
					}
					availableCountrys.add(childResource.getName());
				}
			}
		} catch (LoginException ex) {
			log.error("LoginException {}", ex);
		} finally {
			ResourceResolverUtils.close(resourceResolver);
		}
		return availableCountrys;
	}

	@Override
	public String createTagValueStoredInJcrContent(Tag tag) {
		String namespacePath = tag.getNamespace().getPath();
		String tagPath = tag.getPath();
		String namespaceName = tag.getNamespace().getName();
		return StringUtils.replace(tagPath,
				(namespacePath + HostDesign24AppConstants.STRING_SEPERATOR_SLASH),
				(namespaceName + HostDesign24AppConstants.STRING_SEPERATOR_COLON));
	}

	@Override
	public Resource getValidTagResource(String tagName, ResourceResolver resourceResolver,
			String rootPath) {
		Resource resource = resourceResolver.getResource(rootPath);
		if (resource == null) {
			return null;
		}
		Iterator<Resource> tagColors = resource.listChildren();
		while (tagColors.hasNext()) {
			Resource tagColor = tagColors.next();
			if (StringUtils.equals(tagColor.getName(), tagName)) {
				return tagColor;
			}
			Iterator<Resource> tagShades = tagColor.listChildren();
			while (tagShades.hasNext()) {
				Resource tagShade = tagShades.next();
				if (StringUtils.equals(tagShade.getName(), tagName)) {
					return tagShade;
				}
			}

		}
		return null;
	}

	@Override
	public List<Hit> searchProductPageByGtins(final ResourceResolver resourceResolver,
			final String propertyName, final CsvImportVO csvImportVO) {
		Session session = resourceResolver.adaptTo(Session.class);
		QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
		List<Hit> hits = new ArrayList<>();

		String searchPath = String.format(this.choicifySearchPath, csvImportVO.getBrandCompany(),
				csvImportVO.getCountry());

		List<Hit> searchResults = searchProductNodesByGtins(propertyName, csvImportVO.getGtin(),
				session, queryBuilder, searchPath, "1").getHits();
		if (searchResults != null && !searchResults.isEmpty()) {
			hits.addAll(searchResults);
		}
		return hits;
	}

	private SearchResult searchProductNodesByGtins(String propertyName, final String tagValue,
			Session session, QueryBuilder queryBuilder, String searchPath, String searchDepth) {
		Map<String, String> predicateMap = new HashMap<>();
		predicateMap.put(HostDesign24AppConstants.QUERY_STRING_PATH_NAME, searchPath);
		predicateMap.put(HostDesign24AppConstants.QUERY_STRING_TYPE_NAME, "cq:PageContent");
		predicateMap.put(HostDesign24AppConstants.QUERY_STRING_LIMIT_NAME, "-1");
		predicateMap.put("group.1_property", propertyName);
		predicateMap.put("group.1_property.value", tagValue);
		predicateMap.put("group.1_property.depth", searchDepth);
		PredicateGroup predicateGroup = PredicateGroup.create(predicateMap);
		Query query = queryBuilder.createQuery(predicateGroup, session);
		SearchResult searchResult = query.getResult();
		return searchResult;
	}

	@Override
	public String getChoicifyDomainName() {
		return choicifyDomainName;
	}

	@Override
	public List<Resource> getResourcesFromSearchResult(List<Hit> searchResult) {
		List<Resource> result = new ArrayList<>();
		for (Hit hit : searchResult) {
			try {
				result.add(hit.getResource());
			} catch (RepositoryException e) {
				log.warn("while parsing results: {}", e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Create path to store results.
	 * 
	 * @param selectors
	 * @return {@link String}
	 */
	public String createPathFromSelector(List<String> selectors) {
		String path = StringUtils.EMPTY;
		for (String selector : selectors) {
			if (StringUtils.startsWith(selector, HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							HostDesign24AppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
				if (StringUtils.isBlank(path)) {
					path = selector;
				} else {
					path = path + "_" + selector;
				}
			}
		}
		return path;
	}

	@Override
	public List<ProductModel> findProductNodesByGtin(final String gtin,
			final ResourceResolver resourceResolver, final String brand, final String country) {
		List<ProductModel> pages = new ArrayList<>();
		try {
			String searchPath = String.format(this.choicifySearchPath, brand, country);
			NodeIterator sesults = SearchUtils.searchByProperty(searchPath,
					HostDesign24AppConstants.CHOICIFY_GTIN_NAME, gtin, resourceResolver);
			if (sesults != null && sesults.getSize() > 0) {
				addSearchedPagesToList(resourceResolver, pages, sesults);
			}

		} catch (RepositoryException e) {
			log.error("RepositoryException : {}", e.getMessage());
		}
		return pages;
	}

	private void addSearchedPagesToList(final ResourceResolver resourceResolver,
			List<ProductModel> pages, NodeIterator nodeIterator) throws RepositoryException {
		Node resultNode;
		while (nodeIterator.hasNext()) {
			resultNode = nodeIterator.nextNode();
			pages.add(resourceResolver.getResource(resultNode.getPath()).adaptTo(ProductModel.class));
		}
	}

	private void generateTargetResultsAndCache(final ResourceResolver resourceResolver,
			final List<String> selectors, final String locale, final String brand, final String country,
			final String jsonNodePath) {
		try {
			String nodeName = createPathFromSelector(selectors);
			TargetColors targetColors =
					searchTargetHairColorTagsOnPages(resourceResolver, selectors, locale, brand, country);

			if (targetColors != null) {
				String savePath = jsonNodePath + "/" + nodeName;
				saveJsonResults(resourceResolver.adaptTo(Session.class), targetColors, locale, savePath);
			}

		} catch (RepositoryException ex) {
			log.error("RepositoryException {}", ex);
		}
	}

	@Override
	public TargetColors searchTargetHairColorTagsOnPages(final ResourceResolver resourceResolver,
			final List<String> selectors, final String locale, final String brand, final String country)
			throws RepositoryException {

		TargetColors targetColors = new TargetColors();

		Session session = resourceResolver.adaptTo(Session.class);
		QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
		TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
		ColorVO naturalColorVO = new ColorVO();
		ShadeVO naturalHairShadeVO = new ShadeVO();
		List<ShadeVO> nhShades = new ArrayList<>();
		String naturalHairColor = null;
		String naturalHairShade = null;
		String tagPath = null;
		if (selectors != null) {
			for (String selector : selectors) {
				if (StringUtils.equals(HostDesign24AppConstants.CHOICIFY_SERVLET_SELECTOR, selector)
						|| StringUtils.equals(HostDesign24AppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR,
								selector)) {
					continue;
				}
				if (StringUtils.isEmpty(tagPath)) {
					tagPath = getValidTag(selector, resourceResolver, this.naturalHairColorsPath);
				} else {
					Resource resource = resourceResolver.getResource(tagPath).getChild(selector);
					if (resource != null) {
						tagPath = resource.getPath();
					}
				}
				if (selector.startsWith(HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
						&& StringUtils.isNotEmpty(tagPath)) {
					naturalColorVO.setNameColor(selector);
					Tag shadeTag = resourceResolver.getResource(tagPath).adaptTo(Tag.class);
					naturalColorVO.setUrlColor(createImageUrlForColorOrShade(shadeTag,
							HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
					naturalColorVO.setUrlColorHead(createImageUrlForWomanHead(shadeTag,
							HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
					naturalHairColor = tagPath;
					continue;
				}
				if (StringUtils.startsWith(selector,
						HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
						&& StringUtils.isNotEmpty(naturalColorVO.getNameColor())) {
					naturalHairShadeVO.setNameShade(selector);
					Tag shadeTag = resourceResolver.getResource(tagPath).adaptTo(Tag.class);
					naturalHairShadeVO.setUrlShade(createImageUrlForColorOrShade(shadeTag,
							HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
					naturalHairShadeVO.setUrlShadeHead(createImageUrlForWomanHead(shadeTag,
							HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
					nhShades.add(naturalHairShadeVO);
					naturalHairShade = tagPath;
				}
			}
		}

		List<ColorVO> colorVOsnhcWithNhs = new ArrayList<>();
		targetColors.setNhcWithNhs(colorVOsnhcWithNhs);
		naturalColorVO.setShades(nhShades);
		colorVOsnhcWithNhs.add(naturalColorVO);

		if (StringUtils.isEmpty(naturalHairColor)) {
			return targetColors;
		}
		List<ColorVO> colorVOsThcWithThs = new ArrayList<>();
		targetColors.setThcWithThs(colorVOsThcWithThs);
		List<Hit> searchResult = searchNodesWithNaturalHairColorOrShades(session, queryBuilder,
				tagManager, naturalHairColor, naturalHairShade, locale, brand, country);
		naturalColorVO.setProductsCount(searchResult.size());
		naturalHairShadeVO.setProductsCount(searchResult.size());

		Resource resource = resourceResolver.getResource(this.targetHairColorsPath);
		Tag baseTag = resource.adaptTo(Tag.class);
		Iterator<Tag> thcTags = baseTag.listChildren();
		while (thcTags.hasNext()) {
			Tag colorTag = thcTags.next();
			List<String> shadesColors = new ArrayList<>();
			shadesColors.add(naturalColorVO.getNameColor());
			shadesColors.add(naturalHairShadeVO.getNameShade());
			shadesColors.add(colorTag.getName());

			ColorVO colorVO = new ColorVO();
			List<ShadeVO> shadeVOs = new ArrayList<>();

			int productsCountSize = 0;
			Map<Integer, List<Hit>> result = searchResults(resourceResolver.adaptTo(Session.class),
					resourceResolver, selectors, brand, country);
			int size = 0;
			if (result != null && !result.isEmpty()) {
				for (Integer key : result.keySet()) {
					size = size + result.get(key).size();
				}
			}
			productsCountSize = size;

			if (productsCountSize > 0) {
				colorVO.setNameColor(colorTag.getName());
				colorVO.setLabelColor(colorTag.getTitle(new Locale(locale)));
				colorVO.setUrlColor(createImageUrlForColorOrShade(colorTag,
						HostDesign24AppConstants.TARGET_HAIR_COLOR_ABBREVIATION, resourceResolver));
				colorVO.setUrlColorHead(createImageUrlForWomanHead(colorTag,
						HostDesign24AppConstants.TARGET_HAIR_COLOR_ABBREVIATION, resourceResolver));
				colorVO.setProductsCount(productsCountSize);
				colorVO.setShades(shadeVOs);
				colorVOsThcWithThs.add(colorVO);

				Map<String, String> targetHairShadeGUIds = getTargetHairShadeGUIDs();
				Iterator<Tag> shadeTags = colorTag.listChildren();
				while (shadeTags.hasNext()) {
					ShadeVO shadeVO = new ShadeVO();
					Tag shadeTag = shadeTags.next();

					shadesColors = new ArrayList<>();
					shadesColors.add(naturalColorVO.getNameColor());
					shadesColors.add(naturalHairShadeVO.getNameShade());
					shadesColors.add(colorTag.getName());
					shadesColors.add(shadeTag.getName());

					productsCountSize = 0;
					Map<Integer, List<Hit>> shadesColorsResult = searchResults(
							resourceResolver.adaptTo(Session.class), resourceResolver, selectors, brand, country);
					int shadesColorsResultSize = 0;
					if (shadesColorsResult != null && !shadesColorsResult.isEmpty()) {
						for (Integer key : shadesColorsResult.keySet()) {
							shadesColorsResultSize = shadesColorsResultSize + shadesColorsResult.get(key).size();
						}
					}
					productsCountSize = shadesColorsResultSize;

					if (productsCountSize > 0) {
						shadeVO.setNameShade(shadeTag.getName());
						shadeVO.setLabelShade(shadeTag.getTitle(new Locale(locale)));
						shadeVO.setUrlShade(createImageUrlForColorOrShade(shadeTag,
								HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION, resourceResolver));
						shadeVO.setUrlShadeHead(createImageUrlForWomanHead(shadeTag,
								HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION, resourceResolver));
						shadeVO.setProductsCount(productsCountSize);

						if (targetHairShadeGUIds != null && !targetHairShadeGUIds.isEmpty()
								&& targetHairShadeGUIds.containsKey(shadeTag.getName())) {
							shadeVO.setPerfectCamColorCode(targetHairShadeGUIds.get(shadeTag.getName()));
							shadeVO.setPerfectCamColorTile(
									new StringBuilder(HostDesign24AppConstants.PERFECT_CAMCODE_TILE_PATH)
											.append(shadeTag.getName()).append(".jpg").toString());
						}

						shadeVOs.add(shadeVO);
					}
				}
			}
		}

		return targetColors;
	}

	private String getValidTag(String tagName, ResourceResolver resourceResolver, String rootPath) {
		Resource resource = getValidTagResource(tagName, resourceResolver, rootPath);
		if (resource == null) {
			return null;
		}
		return resource.getPath();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildTargetResultsJson(ResourceResolver resourceResolver, final String locale,
			final String brand, final String country) {
		log.debug("Start: TargetResults");
		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(HostDesign24AppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(HostDesign24AppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR).toString();

		Resource resourceRootNhc = resourceResolver
				.getResource(HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
		if (resourceRootNhc != null) {
			List<Resource> resourceNhcs = IteratorUtils.toList(resourceRootNhc.listChildren());
			List<String> selectors = null;
			for (Resource resourceNhc : resourceNhcs) {
				selectors = new ArrayList<>();
				selectors.add(resourceNhc.getName());
				generateTargetResultsAndCache(resourceResolver, selectors, locale, brand, country,
						jsonNodePath);
				List<Resource> resourceNhcNhss = IteratorUtils.toList(resourceNhc.listChildren());
				for (Resource resourceNhcNhs : resourceNhcNhss) {
					selectors = new ArrayList<>();
					selectors.add(resourceNhc.getName());
					selectors.add(resourceNhcNhs.getName());
					generateTargetResultsAndCache(resourceResolver, selectors, locale, brand, country,
							jsonNodePath);
				}
			}
		}
		log.debug("End: TargetResults");
	}

	@Override
	public void buildNaturalJsonResults(final ResourceResolver resourceResolver, final String locale,
			final String brand, final String country) {
		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(HostDesign24AppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(HostDesign24AppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR).toString();
		NaturalColors naturalColors =
				searchNaturalHairColorTagsOnPages(resourceResolver, locale, brand, country);

		if (naturalColors != null) {
			saveJsonResults(resourceResolver.adaptTo(Session.class), naturalColors, locale, jsonNodePath);
		}
	}

	@Override
	public void buildTargetNaturalJsonResults(final ResourceResolver resourceResolver,
			final String locale, final String brand, final String country) {
		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(HostDesign24AppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(HostDesign24AppConstants.TARGET_NATURAL_COLORS).toString();
		TargetNaturalColors targetNaturalColors =
				searchTargetNaturalHairColorTagsOnPages(resourceResolver, locale, brand, country);

		if (targetNaturalColors != null) {
			saveJsonResults(resourceResolver.adaptTo(Session.class), targetNaturalColors, locale,
					jsonNodePath);
		}
	}

	@Override
	public TargetNaturalColors searchTargetNaturalHairColorTagsOnPages(
			final ResourceResolver resourceResolver, final String locale, final String brand,
			final String country) {
		TargetNaturalColors targetNaturalColors = new TargetNaturalColors();

		// natural colors
		List<ColorVO> colorVOsNhcWithNhs = getNaturalTargetTagsResults(resourceResolver, locale,
				this.naturalHairColorsPath, HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION);
		if (!colorVOsNhcWithNhs.isEmpty()) {
			targetNaturalColors.setNhcWithNhs(colorVOsNhcWithNhs);
		}

		// target colors
		List<ColorVO> colorVOsThcWithThs = getNaturalTargetTagsResults(resourceResolver, locale,
				this.targetHairColorsPath, HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION);
		if (!colorVOsThcWithThs.isEmpty()) {
			targetNaturalColors.setThcWithThs(colorVOsThcWithThs);
		}

		return targetNaturalColors;
	}

	private List<ColorVO> getNaturalTargetTagsResults(final ResourceResolver resourceResolver,
			final String locale, final String hairColorsPath, final String hairShadeAbbreviation) {
		List<ColorVO> colorVOs = new ArrayList<>();
		Resource resource = resourceResolver.getResource(hairColorsPath);
		Tag baseTag = resource.adaptTo(Tag.class);
		Iterator<Tag> colorTags = baseTag.listChildren();

		Map<String, String> targetHairShadeGUIds = new HashMap<>();
		if (StringUtils.equals(hairShadeAbbreviation,
				HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
			targetHairShadeGUIds = getTargetHairShadeGUIDs();
		}

		while (colorTags.hasNext()) {
			ColorVO colorVO = new ColorVO();
			List<ShadeVO> shadeVOs = new ArrayList<>();
			Tag colorTag = colorTags.next();

			List<String> selectors = new ArrayList<>();
			selectors.add(colorTag.getName());

			colorVO.setNameColor(colorTag.getName());
			colorVO.setLabelColor(colorTag.getTitle(new Locale(locale)));
			colorVO.setShades(shadeVOs);

			Iterator<Tag> shadeTags = colorTag.listChildren();
			while (shadeTags.hasNext()) {
				ShadeVO shadeVO = new ShadeVO();
				Tag shadeTag = shadeTags.next();

				selectors = new ArrayList<>();
				selectors.add(colorTag.getName());
				selectors.add(shadeTag.getName());

				shadeVO.setNameShade(shadeTag.getName());
				shadeVO.setLabelShade(shadeTag.getTitle(new Locale(locale)));
				shadeVO.setUrlShade(createColorOrShadeImageUrls(shadeTag, hairShadeAbbreviation));
				shadeVO.setUrlShadeHead(createWomanHeadImageUrl(shadeTag, hairShadeAbbreviation));

				// only for ths
				if (StringUtils.equals(hairShadeAbbreviation,
						HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
					if (targetHairShadeGUIds != null && !targetHairShadeGUIds.isEmpty()
							&& targetHairShadeGUIds.containsKey(shadeTag.getName())) {
						shadeVO.setPerfectCamColorCode(targetHairShadeGUIds.get(shadeTag.getName()));
						shadeVO.setPerfectCamColorTile(
								new StringBuilder(HostDesign24AppConstants.PERFECT_CAMCODE_TILE_PATH)
										.append(shadeTag.getName()).append(".jpg").toString());
					}
				}

				shadeVOs.add(shadeVO);
			}

			if (StringUtils.isNotEmpty(colorVO.getNameColor())) {
				colorVOs.add(colorVO);
			}
		}
		return colorVOs;
	}

	@Override
	public NaturalColors searchNaturalHairColorTagsOnPages(final ResourceResolver resourceResolver,
			final String locale, final String brand, final String country) {
		NaturalColors naturalColors = new NaturalColors();
		List<ColorVO> colorVOsnhcWithNhs = new ArrayList<>();

		Resource resource = resourceResolver.getResource(this.naturalHairColorsPath);
		if (resource == null) {
			return naturalColors;
		}
		Tag baseTag = resource.adaptTo(Tag.class);
		if (baseTag == null) {
			return naturalColors;
		}
		Iterator<Tag> colorTags = baseTag.listChildren();
		while (colorTags.hasNext()) {
			ColorVO colorVO = new ColorVO();
			List<ShadeVO> shadeVOs = new ArrayList<>();
			Tag colorTag = colorTags.next();

			List<String> selectors = new ArrayList<>();
			selectors.add(colorTag.getName());

			int tagColorHitCounter = 0;
			Map<Integer, List<Hit>> result = searchResults(resourceResolver.adaptTo(Session.class),
					resourceResolver, selectors, brand, country);
			int size = 0;
			if (result != null && !result.isEmpty()) {
				for (Integer key : result.keySet()) {
					size = size + result.get(key).size();
				}
			}
			tagColorHitCounter = size;

			if (tagColorHitCounter > 0) {
				colorVO.setNameColor(colorTag.getName());
				colorVO.setLabelColor(colorTag.getTitle(new Locale(locale)));
				colorVO.setUrlColor(createImageUrlForColorOrShade(colorTag,
						HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
				colorVO.setUrlColorHead(createImageUrlForWomanHead(colorTag,
						HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
				colorVO.setProductsCount(tagColorHitCounter);
				colorVO.setShades(shadeVOs);
			}

			Iterator<Tag> shadeTags = colorTag.listChildren();
			while (shadeTags.hasNext()) {
				ShadeVO shadeVO = new ShadeVO();
				Tag shadeTag = shadeTags.next();

				selectors = new ArrayList<>();
				selectors.add(colorTag.getName());
				selectors.add(shadeTag.getName());

				int tagShadeHitCounter = 0;
				Map<Integer, List<Hit>> naturalHairColorResults = searchResults(
						resourceResolver.adaptTo(Session.class), resourceResolver, selectors, brand, country);
				int naturalHairColorResultSize = 0;
				if (naturalHairColorResults != null && !naturalHairColorResults.isEmpty()) {
					for (Integer key : naturalHairColorResults.keySet()) {
						naturalHairColorResultSize =
								naturalHairColorResultSize + naturalHairColorResults.get(key).size();
					}
				}
				tagShadeHitCounter = naturalHairColorResultSize;

				if (tagShadeHitCounter > 0) {
					shadeVO.setNameShade(shadeTag.getName());
					shadeVO.setLabelShade(shadeTag.getTitle(new Locale(locale)));
					shadeVO.setUrlShade(createImageUrlForColorOrShade(shadeTag,
							HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
					shadeVO.setUrlShadeHead(createImageUrlForWomanHead(shadeTag,
							HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
					shadeVO.setProductsCount(tagShadeHitCounter);
					shadeVOs.add(shadeVO);
				}
			}

			if (StringUtils.isNotEmpty(colorVO.getNameColor())) {
				colorVOsnhcWithNhs.add(colorVO);
			}
		}
		if (!colorVOsnhcWithNhs.isEmpty()) {
			naturalColors.setNhcWithNhs(colorVOsnhcWithNhs);
		}

		return naturalColors;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildProductJsonResults(SlingHttpServletRequest request,
			ResourceResolver resourceResolver, ChoicifyAppService choicifyAppService, final String locale,
			final String brand, final String country) throws RepositoryException {
		List<String> selectors = null;

		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(HostDesign24AppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(HostDesign24AppConstants.JSON_NAME_PRODUCTS).toString();

		Resource resourceRootNhc = resourceResolver
				.getResource(HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
		Resource resourceRootThc = resourceResolver
				.getResource(HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR);
		if (resourceRootNhc != null && resourceRootThc != null) {
			List<Resource> resourceNhcs = IteratorUtils.toList(resourceRootNhc.listChildren());
			List<Resource> resourceThcs = IteratorUtils.toList(resourceRootThc.listChildren());
			// nhc
			for (Resource resourceNhc : resourceNhcs) {
				List<Resource> resourceNhcNhss = IteratorUtils.toList(resourceNhc.listChildren());
				// nhc nhs
				for (Resource resourceNhcNhs : resourceNhcNhss) {
					// nhc nhs thc
					for (Resource resourceThc : resourceThcs) {
						List<Resource> resourceThcThss = IteratorUtils.toList(resourceThc.listChildren());
						// nhc nhs thc ths
						for (Resource resourceThcThs : resourceThcThss) {
							selectors = new ArrayList<>();
							selectors.add(resourceNhc.getName());
							selectors.add(resourceNhcNhs.getName());
							selectors.add(resourceThc.getName());
							selectors.add(resourceThcThs.getName());
							generateProductJsonResultsAndStore(request, resourceResolver, selectors, locale,
									brand, country, jsonNodePath);
						}
					}
				}
			}
		}
	}

	@Override
	public ProductResult searchProudctPagesByTags(SlingHttpServletRequest request,
			final ResourceResolver resourceResolver, final List<String> selectors, final String locale,
			final String brand, final String country) {
		ProductResult productResult = new ProductResult();

		TagManager tagManger = resourceResolver.adaptTo(TagManager.class);
		final Map<String, List<FilterOption>> keyfilters = new HashMap<>();

		List<ProductSearchModel> products = new ArrayList<>();
		createProductJsonOutPut(request, products, tagManger, keyfilters, resourceResolver, selectors,
				locale, brand, country);

		// remove duplicates
		products = products.stream().distinct().collect(Collectors.toList());
		productResult.setProducts(products);

		List<Filter> filters = new ArrayList<>();
		if (!keyfilters.isEmpty()) {
			Filter filter;
			for (Map.Entry<String, List<FilterOption>> entry : keyfilters.entrySet()) {
				filter = new Filter();
				List<FilterOption> filterOptions =
						entry.getValue().stream().distinct().collect(Collectors.toList());

				String name = null;
				String key = null;
				String tagPath = HostDesign24AppConstants.CHOICIFY_TAG_FILTER_ROOT_PATH
						+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + entry.getKey();
				Resource resource = resourceResolver.getResource(tagPath);
				if (resource != null) {
					Tag tag = resource.adaptTo(Tag.class);
					name = tag.getTitle(new Locale(locale));
					key = tag.getName();
				} else {
					name = entry.getKey();
					key = entry.getKey();
				}
				filter.setName(name);
				filter.setKey(key);
				filter.setOptions(filterOptions);

				filters.add(filter);
			}
		}
		productResult.setFilters(filters);

		return productResult;
	}

	private void generateProductJsonResultsAndStore(SlingHttpServletRequest request,
			final ResourceResolver resourceResolver, final List<String> selectors, final String locale,
			final String brand, final String country, final String jsonNodePath) {

		String[] combinations = selectors.stream().toArray(String[]::new);
		Map<Integer, List<String>> allCombinations =
				new ColorUtils().iterateColorCombinations(combinations);
		for (Map.Entry<Integer, List<String>> entry : allCombinations.entrySet()) {
			List<String> possibleCombination = entry.getValue();

			String nodeName = StringUtils.join(possibleCombination, "_");
			String savePath = jsonNodePath + "/" + nodeName;
			Session session = resourceResolver.adaptTo(Session.class);
			Node jsonNode = null;
			try {
				jsonNode = JcrUtils.getNodeIfExists(savePath, session);

				if (jsonNode == null || !jsonNode.hasProperty(locale)) {
					searchAndSave(request, resourceResolver, locale, brand, country, possibleCombination,
							savePath, session);
				}
			} catch (RepositoryException e) {
				log.error("RepositoryException {}", e);
			}

		}
	}

	private void searchAndSave(SlingHttpServletRequest request,
			final ResourceResolver resourceResolver, final String locale, final String brand,
			final String country, List<String> possibleCombination, String savePath, Session session) {
		ProductResult productResult = searchProudctPagesByTags(request, resourceResolver,
				possibleCombination, locale, brand, country);
		if (productResult != null) {
			saveJsonResults(session, productResult, locale, savePath);
		}
	}

	@Override
	public void saveJsonResults(Session session, final Object results, final String propertyKey,
			final String savePath) {

		String jsonResults = new Gson().toJson(results);

		Node resultNode;
		try {
			resultNode = JcrUtils.getOrCreateByPath(savePath, false, JcrConstants.NT_UNSTRUCTURED,
					JcrConstants.NT_UNSTRUCTURED, session, true);
			if (resultNode.hasProperty(propertyKey)) {
				resultNode.getProperty(propertyKey).remove();
				resultNode.save();
			}
			resultNode.setProperty(propertyKey, jsonResults);
			JcrUtils.setLastModified(resultNode, Calendar.getInstance());
			resultNode.save();
			log.info("Json node: {}", resultNode.getPath());
		} catch (RepositoryException ex) {
			log.error("RepositoryException {}", ex);
		}
	}

	private void createProductJsonOutPut(SlingHttpServletRequest request,
			List<ProductSearchModel> products, TagManager tagManger,
			final Map<String, List<FilterOption>> keyfilters, ResourceResolver resourceResolver,
			final List<String> selectors, final String locale, final String brand, final String country) {

		Map<Integer, List<Hit>> searchResult = searchResults(resourceResolver.adaptTo(Session.class),
				resourceResolver, selectors, brand, country);
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

		ProductSearchModel productSearchModel;
		if (searchResult != null && !searchResult.isEmpty()) {
			I18n i18n = I18nUtils.getI18n(request, locale);
			for (Integer key : searchResult.keySet()) {
				List<Hit> hits = searchResult.get(key);
				if (hits != null && !hits.isEmpty()) {
					for (Hit hit : hits) {
						if (pageManager != null) {
							Resource resource = null;
							Page page = null;
							productSearchModel = new ProductSearchModel();
							try {
								resource = hit.getResource();
								page = pageManager.getContainingPage(resource);
								if (page == null || !page.isValid()) {
									continue;
								}
							} catch (RepositoryException ex) {
								log.error("RepositoryException: {}", ex);
							}
							if (resource != null) {
								ColorationModel colorationModel = resource.adaptTo(ColorationModel.class);
								if (colorationModel != null) {
									productSearchModel.setColorTile(colorationModel.getColorTile());
								}
							}

							if (page.getContentResource() != null) {
								Resource resourcePrd = page.getContentResource()
										.getChild(HostDesign24AppConstants.JCR_NODE_NAME_PRODUCT);
								if (resourcePrd != null) {
									ProductModel productResource = resourcePrd.adaptTo(ProductModel.class);
									if (productResource == null) {
										continue;
									}
									productSearchModel.setGtin(productResource.getGtin());
									productSearchModel.setBrandPagetitle(productResource.getBrandTitle());
									productSearchModel
											.setProductPageTitle(i18n.get(productResource.getCategoryTitle()));
									productSearchModel.setProductTitle(i18n.get(productResource.getProductTitle()));
									productSearchModel.setProductPageUrl(productResource.getProductUrl());
									productSearchModel
											.setProductImageUrl(productResource.getPackshot().getFileReference());
									productSearchModel
											.setSuggestedRetailsPrice(productResource.getSuggestedRetailsPrice());
									productSearchModel.setSelectedStoreOnly(productResource.getSelectedStoreOnly());
									productSearchModel.setProductPage(productResource.getProductPagePath());
									productSearchModel.setPriority(key);

									createFilters(tagManger, keyfilters, productResource, locale);
									createProductTags(tagManger, productSearchModel, productResource,
											resourceResolver, key, locale);

									products.add(productSearchModel);
								}
							}
						}
					}
				}
			}
		}
	}

	private void createFilters(TagManager tagManger, final Map<String, List<FilterOption>> keyfilters,
			ProductModel productResource, final String locale) {
		Resource filterResource = productResource.getFilterResource();
		if (filterResource != null) {
			Iterator<Resource> resources = filterResource.listChildren();
			FilterOption option;
			while (resources.hasNext()) {
				Resource resource = resources.next();
				ValueMap valueMap = resource.adaptTo(ValueMap.class);
				for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
					if (JcrConstants.JCR_PRIMARYTYPE.equals(entry.getKey())) {
						continue;
					}
					if (!keyfilters.containsKey(entry.getKey())) {
						keyfilters.put(entry.getKey(), new ArrayList<FilterOption>());
					}
					String[] tagValues = valueMap.get(entry.getKey(), String[].class);
					Tag tag = tagManger.resolve(tagValues[0]);
					option = new FilterOption();
					option.setKey(tag.getName());
					option.setName(tag.getTitle(new Locale(locale)));

					keyfilters.getOrDefault(entry.getKey(), new ArrayList<FilterOption>()).add(option);
				}
			}
		}
	}

	private ProductTag searchObjectInList(String key, List<ProductTag> pageTags) {

		return pageTags.stream().filter(x -> StringUtils.equals(key, x.getKey())).findAny()
				.orElse(null);
	}

	private void createProductTags(TagManager tagManger, ProductSearchModel productSearchModel,
			ProductModel productResource, ResourceResolver resourceResolver, final int priority,
			final String locale) {
		Map<String, List<FilterOption>> keyTags = new HashMap<>();
		List<ProductTag> pageTags = new ArrayList<>();
		List<Resource> tagResources = productResource.getColorationPaths();

		addTagsToProduct(tagManger, keyTags, tagResources, locale);
		addTagFiltersToProduct(tagManger, productResource, keyTags, locale);
		createTagFiltersforChoicifyApps(keyTags, pageTags, resourceResolver, locale);

		// add priority tag
		ProductTag productPriorityTag = new ProductTag();
		List<FilterOption> priorityOptions = new ArrayList<>();
		productPriorityTag.setName("Category");
		productPriorityTag.setKey("category");
		FilterOption filterOption = new FilterOption();
		filterOption.setKey(Integer.toString(priority));
		filterOption.setName(Integer.toString(priority));
		priorityOptions.add(filterOption);
		productPriorityTag.setOptions(priorityOptions);
		pageTags.add(productPriorityTag);

		// remove duplicates
		pageTags = pageTags.stream().distinct().collect(Collectors.toList());
		productSearchModel.setTags(pageTags);
	}

	private void addTagsToProduct(TagManager tagManger, Map<String, List<FilterOption>> keyTags,
			List<Resource> tagResources, final String locale) {
		if (tagResources != null) {
			for (Resource eachResource : tagResources) {
				if (eachResource != null) {
					ValueMap valueMap = eachResource.adaptTo(ValueMap.class);
					for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
						if (JcrConstants.JCR_PRIMARYTYPE.equals(entry.getKey())
								|| HostDesign24AppConstants.PRODUCT_PRIORITY.equals(entry.getKey())
								|| HostDesign24AppConstants.COLOR_TILE.equals(entry.getKey())) {
							continue;
						}
						if (!keyTags.containsKey(entry.getKey())) {
							keyTags.put(entry.getKey(), new ArrayList<FilterOption>());
						}
						String[] tagValues = valueMap.get(entry.getKey(), String[].class);
						for (String tagString : tagValues) {
							Tag tag = tagManger.resolve(tagString);
							if (tag == null) {
								continue;
							}
							List<FilterOption> optionsList = keyTags.get(entry.getKey());

							FilterOption filterOption = new FilterOption();
							filterOption.setKey(tag.getName());
							filterOption.setName(tag.getTitle(new Locale(locale)));
							optionsList.add(filterOption);
						}
					}
				}
			}
		}
	}

	/**
	 * create tag used for filtering product on the choicify application.
	 * 
	 * @param keyTags {@link Map}
	 * @param pageTags {@link List<FilterOption>}
	 * @param resourceResolver {@link ResourceResolver}
	 */
	private void createTagFiltersforChoicifyApps(Map<String, List<FilterOption>> keyTags,
			List<ProductTag> pageTags, ResourceResolver resourceResolver, final String locale) {
		if (!keyTags.isEmpty()) {
			for (Map.Entry<String, List<FilterOption>> entry : keyTags.entrySet()) {
				ProductTag productTag = searchObjectInList(entry.getKey(), pageTags);
				if (productTag == null) {
					productTag = new ProductTag();
				}
				String key = null;
				String name = null;
				String tagPath = HostDesign24AppConstants.CHOICIFY_TAG_FILTER_ROOT_PATH
						+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + entry.getKey();
				Resource resource = resourceResolver.getResource(tagPath);
				if (resource != null) {
					Tag tag = resource.adaptTo(Tag.class);
					if (tag == null) {
						log.error("Tag doesn't exist {}", resource.getPath());
						continue;
					}
					key = tag.getName();
					name = tag.getTitle(new Locale(locale));
				} else {
					key = entry.getKey();
					name = entry.getKey();
				}
				productTag.setName(name);
				productTag.setKey(key);
				List<FilterOption> optionsList = entry.getValue();
				productTag.setOptions(optionsList.stream().distinct().collect(Collectors.toList()));

				pageTags.add(productTag);
			}
		}
	}

	private Map<Integer, List<Hit>> searchResults(Session session,
			final ResourceResolver resourceResolver, final List<String> selectors, final String brand,
			final String country) {

		Map<Integer, List<Hit>> searchResults = new HashMap<>();

		if (selectors == null) {
			log.debug("Empty Selector!");
			return null;
		}
		String naturalHairColor = null;
		String naturalHairShade = null;
		String targetHairColor = null;
		String targetHairShade = null;

		for (String selector : selectors) {
			if (!isSearchTag(selector)) {
				continue;
			}
			selector = selector.trim();
			Resource tagResource = getValidTagResource(selector, resourceResolver,
					HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
			if (tagResource == null) {
				tagResource = getValidTagResource(selector, resourceResolver,
						HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR);
			}
			if (tagResource == null) {
				continue;
			}
			if (StringUtils.startsWith(selector,
					HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)) {
				naturalHairColor = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
			}
			if (StringUtils.startsWith(selector,
					HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)) {
				naturalHairShade = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
			}
			if (StringUtils.startsWith(selector,
					HostDesign24AppConstants.TARGET_HAIR_COLOR_ABBREVIATION)) {
				targetHairColor = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
			}
			if (StringUtils.startsWith(selector,
					HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
				targetHairShade = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
			}
		}

		QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
		String searchPath = String.format(this.choicifySearchPath, brand, country);
		List<Hit> priority1Group = new ArrayList<>();
		List<Hit> priority2Group = new ArrayList<>();

		// priority 1
		SearchResult searchResult1 = searchProducts(searchPath, naturalHairColor, naturalHairShade,
				targetHairColor, targetHairShade, session, queryBuilder, "1");
		if (searchResult1 != null) {
			List<Hit> searchResultsPriority1 = searchResult1.getHits();
			if ((searchResultsPriority1 != null && !searchResultsPriority1.isEmpty())) {
				priority1Group.addAll(searchResultsPriority1);
				searchResults.put(1, priority1Group);
			}
		}

		// priority 2
		SearchResult searchResult2 = searchProducts(searchPath, naturalHairColor, naturalHairShade,
				targetHairColor, targetHairShade, session, queryBuilder, "2");
		if (searchResult2 != null) {
			List<Hit> searchResultsPriority2 = searchResult2.getHits();
			if ((searchResultsPriority2 != null && !searchResultsPriority2.isEmpty())) {
				priority2Group.addAll(searchResultsPriority2);
				searchResults.put(2, priority2Group);
			}
		}

		return searchResults;
	}

	private List<Hit> searchNodesWithNaturalHairColorOrShades(Session session,
			QueryBuilder queryBuilder, TagManager tagManager, String naturalHairColor,
			String naturalHairShade, final String locale, final String brand, final String country) {
		List<Hit> results = new ArrayList<>();

		String searchPath = String.format(this.choicifySearchPath, brand, country);
		List<Hit> searchResults = searchNaturalColorsOrShades(session, queryBuilder, tagManager,
				naturalHairColor, naturalHairShade, searchPath).getHits();
		if (searchResults != null && !searchResults.isEmpty()) {
			results.addAll(searchResults);
		}
		return results;
	}

	private SearchResult searchNaturalColorsOrShades(Session session, QueryBuilder queryBuilder,
			TagManager tagManager, String naturalHairColor, String naturalHairShade, String searchPath) {
		Map<String, String> predicateMap = new HashMap<>();
		predicateMap.put(HostDesign24AppConstants.QUERY_STRING_PATH_NAME, searchPath);
		predicateMap.put(HostDesign24AppConstants.QUERY_STRING_TYPE_NAME, "cq:PageContent");
		predicateMap.put(HostDesign24AppConstants.QUERY_STRING_LIMIT_NAME, "-1");
		if (StringUtils.isNotEmpty(naturalHairColor)) {
			naturalHairColor = createTagValueStoredInJcrContent(tagManager.resolve(naturalHairColor));
			predicateMap.put("1_property", HostDesign24AppConstants.NATURAL_HAIR_COLORS_PROP_NAME);
			predicateMap.put("1_property.value", naturalHairColor);
			predicateMap.put("1_property.depth", "4");
		}
		if (StringUtils.isNotEmpty(naturalHairShade)) {
			naturalHairShade = createTagValueStoredInJcrContent(tagManager.resolve(naturalHairShade));
			predicateMap.put("2_property", HostDesign24AppConstants.NATURAL_HAIR_SHADES_PROP_NAME);
			predicateMap.put("2_property.value", naturalHairShade);
			predicateMap.put("2_property.depth", "4");
		}
		PredicateGroup predicateGroup = PredicateGroup.create(predicateMap);
		com.day.cq.search.Query query = queryBuilder.createQuery(predicateGroup, session);
		SearchResult searchResult = query.getResult();
		return searchResult;
	}

	private boolean isSearchTag(String selector) {
		if (StringUtils.equals(HostDesign24AppConstants.CHOICIFY_SERVLET_SELECTOR, selector)
				|| StringUtils.equals(HostDesign24AppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR,
						selector)
				|| StringUtils.equals(HostDesign24AppConstants.JSON_NAME_PRODUCTS, selector) || StringUtils
						.equals(HostDesign24AppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR, selector)) {
			return false;
		}
		return true;
	}

	private SearchResult searchProducts(final String path, String naturalHairColor,
			String naturalHairShade, String targetHairColor, String targetHairShade, Session session,
			QueryBuilder queryBuilder, String priority) {
		Map<String, String> predicateMap = new HashMap<>();
		predicateMap.put("path", path);
		predicateMap.put("type", "nt:unstructured");
		predicateMap.put("p.limit", "-1");

		if (StringUtils.isNotBlank(naturalHairColor)) {
			predicateMap.put("1_property", "naturalHairColor");
			predicateMap.put("1_property.value", naturalHairColor);
		}

		if (StringUtils.isNotBlank(naturalHairShade)) {
			predicateMap.put("2_property", "naturalHairShade");
			predicateMap.put("2_property.value", naturalHairShade);
		}

		if (StringUtils.isNotBlank(targetHairColor)) {
			predicateMap.put("3_property", "targetHairColor");
			predicateMap.put("3_property.value", targetHairColor);
		}

		if (StringUtils.isNotBlank(targetHairShade)) {
			predicateMap.put("4_property", "targetHairShade");
			predicateMap.put("4_property.value", targetHairShade);
		}
		if (StringUtils.isNotBlank(priority)) {
			predicateMap.put("5_property", "productPriority");
			predicateMap.put("5_property.value", priority);
		}
		PredicateGroup predicateGroup = PredicateGroup.create(predicateMap);
		com.day.cq.search.Query query = queryBuilder.createQuery(predicateGroup, session);
		return query.getResult();
	}

	/**
	 * Create and add tag filters to product.
	 * 
	 * @param tagManger {@link TagManager}
	 * @param productResource {@link ProductModel}
	 * @param keyTags {@link Map}
	 */
	@SuppressWarnings("unchecked")
	private void addTagFiltersToProduct(TagManager tagManger, ProductModel productResource,
			Map<String, List<FilterOption>> keyTags, final String locale) {
		Resource resourceFilter = productResource.getFilterResource();
		if (resourceFilter != null) {
			List<Resource> resourceFilters = IteratorUtils.toList(resourceFilter.listChildren());
			for (Resource tagResource : resourceFilters) {
				ValueMap valueMap = tagResource.adaptTo(ValueMap.class);
				if (!keyTags.containsKey(tagResource.getName())) {
					keyTags.put(tagResource.getName(), new ArrayList<FilterOption>());
				}
				String[] tagValues = valueMap.get(tagResource.getName(), String[].class);
				if (tagValues != null && tagValues.length > 0) {
					Tag tag = tagManger.resolve(tagValues[0]);
					if (tag == null) {
						continue;
					}
					List<FilterOption> optionsList = keyTags.get(tagResource.getName());
					FilterOption filterOption = new FilterOption();

					filterOption.setKey(tag.getName());
					filterOption.setName(tag.getTitle(new Locale(locale)));
					optionsList.add(filterOption);
				}
			}
		}
	}

	private String createImageUrlForColorOrShade(Tag tag, String abbreviation,
			ResourceResolver resourceResolver) {
		String tagPath = tag.getPath();
		String damRootPath = HostDesign24AppConstants.CHOICIFY_DAM_ROOT_PATH;
		String tagRootPath = HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR;
		String extension = HostDesign24AppConstants.CHOCIFY_ASSET_EXTENSION_JPG;
		String assetPath;
		if (StringUtils.equals(abbreviation, HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
				|| StringUtils.equals(abbreviation,
						HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + tag.getName() + extension;
		} else {
			String colorFamilyFolderName =
					tag.getName() + HostDesign24AppConstants.CHOCIFY_FOLDER_NAME_FAMILY;
			String assetName =
					tag.getName() + HostDesign24AppConstants.CHOCIFY_FOLDER_NAME_FAMILY + extension;
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + colorFamilyFolderName
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + assetName;
		}

		if (resourceResolver.getResource(assetPath) == null) {
			return null;
		}
		return getChoicifyDomainName() + assetPath;
	}

	private String createImageUrlForWomanHead(Tag tag, String abbreviation,
			ResourceResolver resourceResolver) {
		String tagPath = tag.getPath();
		String damRootPath = HostDesign24AppConstants.CHOICIFY_DAM_ROOT_PATH;
		String tagRootPath = HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR;
		String extension = HostDesign24AppConstants.CHOCIFY_ASSET_EXTENSION_PNG;
		String tagNameWithNhs = StringUtils.replace(tag.getName(), abbreviation, "");
		String assetNameWithoutExtension =
				abbreviation + HostDesign24AppConstants.CHOCIFY_IMAGE_WOMAN_HEAD_NAME + tagNameWithNhs;
		String assetPath = "";
		if (StringUtils.equals(abbreviation, HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
				|| StringUtils.equals(abbreviation,
						HostDesign24AppConstants.TARGET_HAIR_COLOR_ABBREVIATION)) {
			String colorFamilyFolderName =
					tag.getName() + HostDesign24AppConstants.CHOCIFY_FOLDER_NAME_FAMILY;
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + colorFamilyFolderName
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + assetNameWithoutExtension + "Family"
					+ extension;
		} else {
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + assetNameWithoutExtension + extension;
		}

		if (resourceResolver.getResource(assetPath) == null) {
			return null;
		}

		return getChoicifyDomainName() + assetPath;
	}

	private String createColorOrShadeImageUrls(Tag tag, String abbreviation) {
		String tagPath = tag.getPath();
		String damRootPath = HostDesign24AppConstants.CHOICIFY_COLORS_DAM_ROOT_PATH;
		String tagRootPath = HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR;
		String extension = HostDesign24AppConstants.CHOCIFY_ASSET_EXTENSION_JPG;
		String assetPath;
		if (StringUtils.equals(abbreviation, HostDesign24AppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
				|| StringUtils.equals(abbreviation,
						HostDesign24AppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + tag.getName() + extension;
		} else {
			String colorFamilyFolderName =
					tag.getName() + HostDesign24AppConstants.CHOCIFY_FOLDER_NAME_FAMILY;
			String assetName =
					tag.getName() + HostDesign24AppConstants.CHOCIFY_FOLDER_NAME_FAMILY + extension;
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + colorFamilyFolderName
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + assetName;
		}

		return getChoicifyDomainName() + assetPath;
	}

	private String createWomanHeadImageUrl(Tag tag, String abbreviation) {
		String tagPath = tag.getPath();
		String damRootPath = HostDesign24AppConstants.CHOICIFY_COLORS_DAM_ROOT_PATH;
		String tagRootPath = HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR;
		String extension = HostDesign24AppConstants.CHOCIFY_ASSET_EXTENSION_PNG;
		String tagNameWithNhs = StringUtils.replace(tag.getName(), abbreviation, "");
		String assetNameWithoutExtension =
				abbreviation + HostDesign24AppConstants.CHOCIFY_IMAGE_WOMAN_HEAD_NAME + tagNameWithNhs;
		String assetPath = "";
		if (StringUtils.equals(abbreviation, HostDesign24AppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
				|| StringUtils.equals(abbreviation,
						HostDesign24AppConstants.TARGET_HAIR_COLOR_ABBREVIATION)) {
			String colorFamilyFolderName =
					tag.getName() + HostDesign24AppConstants.CHOCIFY_FOLDER_NAME_FAMILY;
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + colorFamilyFolderName
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + assetNameWithoutExtension + "Family"
					+ extension;
		} else {
			assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
					+ HostDesign24AppConstants.STRING_SEPERATOR_SLASH + assetNameWithoutExtension + extension;
		}

		return getChoicifyDomainName() + assetPath;
	}

	private ResourceResolver getResourceResolver(final String service) throws LoginException {
		return ResourceResolverUtils.getServiceResourceResolver(this.resourceResolverFactory, service);
	}
}
