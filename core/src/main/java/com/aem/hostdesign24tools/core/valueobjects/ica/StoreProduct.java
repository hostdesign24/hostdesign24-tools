package com.aem.hostdesign24tools.core.valueobjects.ica;

import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class StoreProduct {

	private String store;
	private String id;
	private Product product;
}
