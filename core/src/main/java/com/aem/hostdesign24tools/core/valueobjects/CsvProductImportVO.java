package com.aem.hostdesign24tools.core.valueobjects;

import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class CsvProductImportVO {

	private String gtin;
	private String brand;
	// haircolor, hairstyle ... etc
	private String brandType;
	private String country;
	private String language;
	private String categoryLocal;
	private String category;
	private String brandTitle;
	private String productLineTitle;
	private String productTitle;
	private String productTitleLocal;
	private String status;
	private String imagePath;
	private String productUrl;
	private String productPagePath;

}
