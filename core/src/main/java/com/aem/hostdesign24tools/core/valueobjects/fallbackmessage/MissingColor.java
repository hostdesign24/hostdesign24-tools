package com.aem.hostdesign24tools.core.valueobjects.fallbackmessage;

import java.util.ArrayList;
import java.util.List;
import com.aem.hostdesign24tools.core.valueobjects.productsearch.ProductTag;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class MissingColor {
	@Expose
	private String errorMessage;

	@Expose
	private String language;

	@Expose
	private String requestUrl;

	@Expose
	private List<ProductTag> tags = new ArrayList<>();

}
