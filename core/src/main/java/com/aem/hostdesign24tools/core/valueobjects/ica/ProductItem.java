package com.aem.hostdesign24tools.core.valueobjects.ica;

import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class ProductItem {

	private String id;
	private String type;
}
