$(document)
		.ready(
				function() {

					// Function that will allow us to know if Ajax uploads are
					// supported
					function supportAjaxUploadWithProgress() {
						return supportFileAPI()
								&& supportAjaxUploadProgressEvents()
								&& supportFormData();

						// Is the File API supported?
						function supportFileAPI() {
							var fi = document.createElement('INPUT');
							fi.type = 'file';
							return 'files' in fi;
						}
						;

						// Are progress events supported?
						function supportAjaxUploadProgressEvents() {
							var xhr = new XMLHttpRequest();
							return !!(xhr && ('upload' in xhr) && ('onprogress' in xhr.upload));
						}
						;

						// Is FormData supported?
						function supportFormData() {
							return !!window.FormData;
						}
					}

					// Actually confirm support
					if (supportAjaxUploadWithProgress()) {
						// Ajax uploads are supported!
						// Change the support message and enable the upload
						// button
						var notice = document.getElementById('support-notice');
						var uploadBtn = document
								.getElementById('upload-button-id');
						notice.innerHTML = "Your browser supports HTML uploads to AEM.";
						uploadBtn.removeAttribute('disabled');

						// Init the Ajax form submission
						initFullFormAjaxUpload();

						// Init the single-field file upload
						// initFileOnlyAjaxUpload();
					}

					function initFullFormAjaxUpload() {
						var uploadBtn = document
								.getElementById('upload-button-id');
						uploadBtn.onclick = function(evt) {
							var form = document.getElementById('form-id');
							// FormData receives the whole form
							var formData = new FormData(form);

							// We send the data where the form wanted
							var action = form.getAttribute('action');

							// Code common to both variants
							sendXHRequest(formData, action);
						}
					}

					function initFileOnlyAjaxUpload() {
						var uploadBtn = document
								.getElementById('upload-button-id');
						uploadBtn.onclick = function(evt) {
							var formData = new FormData();

							// Since this is the file only, we send it to a
							// specific location
							// var action = '/upload';

							// FormData only has the file
							var fileInput = document.getElementById('file-id');
							var file = fileInput.files[0];
							formData.append('our-file', file);

							// Code common to both variants
							sendXHRequest(formData);
						}
					}

					// Once the FormData instance is ready and we know
					// where to send the data, the code is the same
					// for both variants of this technique
					function sendXHRequest(formData) {
						$(".healthCheckDownload").hide();
						onloadstartHandler();
						var url = '/etc/choicifytools/productimporter.healthcheck.json';
						$.ajax({
							type : 'POST',
							url : url,
							processData : false,
							contentType : false,
							data : formData,
							success : function(data) {
								onloadFinishedHandler();
								$(".healthCheckDownload").show();
							},
							error : function(error) {

							}
						});
					}

					// Handle the start of the transmission
					function onloadstartHandler() {
						var div = document.getElementById('upload-status');
						div.innerHTML = 'Health check is running!';
					}

					function onloadFinishedHandler() {
						var div = document.getElementById('upload-status');
						div.innerHTML = 'Products health check has been finished, please download report!';
					}
				});