package com.aem.hostdesign24tools.core.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import com.drew.lang.annotations.Nullable;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
public abstract class HttpRequestUtils {

  public static String executeGetRequest(final String url, @Nullable final List<Header> headers,
      @Nullable RequestConfig config) throws IOException {
    String result = null;
    if (config == null) {
      config = createConfig(3000);
    }

    try (CloseableHttpClient httpclient =
        HttpClients.custom().setDefaultRequestConfig(config).build()) {
      final HttpGet httpget = new HttpGet(url);
      long startTimestamp = 0;
      if (log.isTraceEnabled()) {
        log.trace("Method {} - URI {}", httpget.getMethod(), httpget.getURI());
        startTimestamp = System.currentTimeMillis();
      }

      setGetHeaders(httpget, headers);

      final ResponseHandler<String> responseHandler = HttpRequestUtils::getResponseString;

      result = httpclient.execute(httpget, responseHandler);
      if (log.isTraceEnabled()) {
        if (startTimestamp > 0) {
          log.trace("time: {} ms", (System.currentTimeMillis() - startTimestamp));
        }
        log.trace(result);
      }
    }
    return result;
  }

  private static void setGetHeaders(final HttpGet httpget, @Nullable final List<Header> headers) {
    if (headers != null) {
      for (final Header header : headers) {
        httpget.addHeader(header);
      }
    }
  }

  private static String getResponseString(final HttpResponse response) throws IOException {
    final int status = response.getStatusLine().getStatusCode();
    if (status >= 200 && status < 300) {
      final HttpEntity entity = response.getEntity();
      return entity != null ? EntityUtils.toString(entity, Charset.forName("UTF-8")) : null;
    } else {
      throw new ClientProtocolException("Unexpected response status: " + status);
    }
  }

  public static RequestConfig createConfig(final int timeout) {
    return RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).setConnectTimeout(timeout)
        .setConnectionRequestTimeout(timeout).setSocketTimeout(timeout).build();
  }
}
