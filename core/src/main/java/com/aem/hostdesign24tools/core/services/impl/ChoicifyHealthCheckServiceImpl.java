package com.aem.hostdesign24tools.core.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.excel.ExcelReaderException;
import com.aem.hostdesign24tools.core.excel.PoiExcelReader;
import com.aem.hostdesign24tools.core.models.ProductModel;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.ChoicifyHealthCheckService;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.valueobjects.ProductHealthCheckVO;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;

/**
 * @author HostDesign24.com
 *
 */
@Service(ChoicifyHealthCheckService.class)
@Component(metatype = false, immediate = true, label = "Choicify Product Health Check Service")
public class ChoicifyHealthCheckServiceImpl implements ChoicifyHealthCheckService {

	protected final Logger LOGGER = LoggerFactory.getLogger(ChoicifyHealthCheckServiceImpl.class);

	@Reference
	private ChoicifyAppService choicifyAppService;

	/**
	 * Execute thread importer Service.
	 */
	@Override
	public void process(InputStream inputStream, ResourceResolver resourceResolver,
			SlingHttpServletRequest request) {
		Session session = null;
		try {
			session = resourceResolver.adaptTo(Session.class);
			processSpreadSheet(inputStream, resourceResolver, session, request);
		} catch (ExcelReaderException e) {
			LOGGER.error("ExcelReaderException", e);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
		} finally {
			if (session != null && session.isLive()) {
				session.logout();
			}
			ResourceResolverUtils.close(resourceResolver);
			IOUtils.closeQuietly(inputStream);
		}
	}

	private void processSpreadSheet(InputStream inputStream, ResourceResolver resourceResolver,
			Session session, SlingHttpServletRequest request) throws IOException, ExcelReaderException {

		boolean unpublishedProducts = request.getParameter("unpublishedProducts") != null
				? Boolean.parseBoolean(request.getParameter("unpublishedProducts"))
				: false;
		boolean notExistingProducts = request.getParameter("notExistingProducts") != null
				? Boolean.parseBoolean(request.getParameter("notExistingProducts"))
				: false;
		boolean noChoicifyColors = request.getParameter("noChoicifyColors") != null
				? Boolean.parseBoolean(request.getParameter("noChoicifyColors"))
				: false;

		LOGGER.debug("START: processing products gtins from Excel file");
		Workbook workbook = PoiExcelReader.read(inputStream);
		Sheet sheet = workbook.getSheetAt(0);

		List<ProductHealthCheckVO> results = new ArrayList<>();
		int rowcount = 0;
		ProductHealthCheckVO eachRow = new ProductHealthCheckVO();

		DataFormatter objDefaultFormat = new DataFormatter();
		FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
		for (Row row : sheet) {
			eachRow = new ProductHealthCheckVO();
			// skip for header row
			if (rowcount == 0) {
				rowcount++;
				continue;
			}

			Cell initialColorFamily = row.getCell(0);
			Cell initialShadeName = row.getCell(1);
			Cell desiredColorFamily = row.getCell(2);
			Cell desiredShadeName = row.getCell(3);
			Cell gtin = row.getCell(4);

			objFormulaEvaluator.evaluate(gtin);
			String gtinNumber = objDefaultFormat.formatCellValue(gtin, objFormulaEvaluator);

			List<String> colorationPath = new ArrayList<>();
			colorationPath.add(initialColorFamily.toString());
			colorationPath.add(initialShadeName.toString());
			colorationPath.add(desiredColorFamily.toString());
			colorationPath.add(desiredShadeName.toString());

			if (StringUtils.isBlank(gtin.toString())) {
				LOGGER.error("CSV file content has empty line.");
				continue;
			} else {
				// setting the values
				eachRow.setGtin(gtinNumber);
				eachRow.setInitialColorFamily(initialColorFamily.toString());
				eachRow.setInitialShadeName(initialShadeName.toString());
				eachRow.setDesiredColorFamily(desiredColorFamily.toString());
				eachRow.setDesiredShadeName(desiredShadeName.toString());
				searchByGtin(resourceResolver, eachRow, StringUtils.join(colorationPath, ","));

				// possible filters
				if (unpublishedProducts && notExistingProducts && noChoicifyColors) {
					if (!eachRow.isPublished() && !eachRow.isProductPageExist() && !eachRow.isHasChoicify()) {
						results.add(eachRow);
					}
				} else if (unpublishedProducts && notExistingProducts) {
					if (!eachRow.isPublished() && !eachRow.isProductPageExist()) {
						results.add(eachRow);
					}
				} else if (unpublishedProducts && noChoicifyColors) {
					if (!eachRow.isPublished() && !eachRow.isHasChoicify()) {
						results.add(eachRow);
					}
				} else if (notExistingProducts && noChoicifyColors) {
					if (!eachRow.isProductPageExist() && !eachRow.isHasChoicify()) {
						results.add(eachRow);
					}
				} else {
					if (unpublishedProducts) {
						if (!eachRow.isPublished() && eachRow.isProductPageExist()) {
							results.add(eachRow);
						}
					} else if (notExistingProducts) {
						if (!eachRow.isProductPageExist()) {
							results.add(eachRow);
						}
					} else if (noChoicifyColors) {
						if (!eachRow.isHasChoicify() && eachRow.isProductPageExist()) {
							results.add(eachRow);
						}
					} else {
						results.add(eachRow);
					}
				}
			}
		}
		try {
			saveHealthCheckResults(session, results);
		} catch (RepositoryException e) {
			LOGGER.error("RepositoryException", e);
		}
		LOGGER.debug("END: processing products gtins from Excel file");
	}

	private void saveHealthCheckResults(Session session, List<ProductHealthCheckVO> productsList)
			throws RepositoryException {
		String results = new Gson().toJson(productsList);

		Node resultNode =
				JcrUtils.getOrCreateByPath(HostDesign24AppConstants.CHOICIFY_HEALTHCHECK_RESULT_PATH, false,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED, session, true);

		if (resultNode.hasProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS)) {
			resultNode.getProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS).remove();
			resultNode.getSession().save();
		}
		resultNode.setProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, results);
		JcrUtils.setLastModified(resultNode, Calendar.getInstance());
		session.save();
	}

	private void searchByGtin(ResourceResolver resourceResolver,
			final ProductHealthCheckVO productRowVO, String colorCombination) {

		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<ProductModel> productnodes =
				choicifyAppService.findProductNodesByGtin(productRowVO.getGtin(), resourceResolver, "", "");

		if (productnodes != null && !productnodes.isEmpty()) {
			productRowVO.setProductPageExist(true);
			for (ProductModel eachProduct : productnodes) {
				Page page = pageManager.getPage(eachProduct.getProductPagePath());

				if (eachProduct.getColorCombinations() == null
						|| eachProduct.getColorCombinations().isEmpty()) {
					productRowVO.setHasChoicify(false);
					productRowVO.setColorCombinationExist(false);
				} else {
					productRowVO.setHasChoicify(true);
					if (eachProduct.getColorCombinations().contains(colorCombination)) {
						productRowVO.setColorCombinationExist(true);
					} else {
						productRowVO.setColorCombinationExist(false);
					}
				}
				ReplicationStatus replicationStatus = null;
				try {
					replicationStatus = page.adaptTo(ReplicationStatus.class);
				} catch (Exception ex) {
					productRowVO.setPublished(false);
					continue;
				}
				if (replicationStatus.isDeactivated() || !replicationStatus.isDelivered()) {
					productRowVO.setPublished(false);
				} else if (replicationStatus.isActivated()) {
					productRowVO.setPublished(true);
				}
				productRowVO.setProductPath(page.getPath());
			}
		} else {
			productRowVO.setHasChoicify(false);
			productRowVO.setColorCombinationExist(false);
			productRowVO.setPublished(false);
			productRowVO.setProductPageExist(false);
		}
	}

}
