package com.aem.hostdesign24tools.core.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.ServerException;
import java.util.Arrays;
import java.util.List;
import javax.jcr.Session;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.threads.ModifiableThreadPoolConfig;
import org.apache.sling.commons.threads.ThreadPool;
import org.apache.sling.commons.threads.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.ChoicifyReplicationService;
import com.aem.hostdesign24tools.core.services.runnable.ChoicifyMetadataImporterService;
import com.aem.hostdesign24tools.core.valueobjects.ThreadVO;

/**
 * @author HostDesign24.com
 *
 */
@SlingServlet(resourceTypes = "sling/servlet/default",
		selectors = {"metadataImporter", "deleteMetadata"}, extensions = "json",
		methods = {"GET", "POST"})
public class ChoicifyMetadataImporterServlet extends AbstractAllMethodsServlet {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -2407470181172575579L;

	@Reference
	private ChoicifyAppService choicifyAppService;

	@Reference
	ChoicifyReplicationService choicifyReplicationService;

	@Reference
	private ThreadPoolManager threadPoolManager;

	/** Default log. */
	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private ResourceResolver resourceResolver;
	private Session session;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		response.setContentType("text/plain");

		try {

			final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
			if (!ServletFileUpload.isMultipartContent(request)) {
				response.setStatus(500);
				LOGGER.debug("Internal server error: is not MultipartContent");
				response.getWriter()
						.println(String.format("Internal server error: is not MultipartContent"));
				response.getWriter().close();
				return;
			}
			resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
			session = resourceResolver.adaptTo(Session.class);
			LOGGER.info("START: processing Colors and Shades from Excel file");

			RequestParameter[] productsFiles = request.getRequestParameters("productsFile");
			RequestParameter productsFile = productsFiles[0];
			InputStream inputStream = productsFile.getInputStream();
			if (inputStream == null) {
				response.setStatus(405);
				response.getWriter().println(String.format("File not properly uploaded"));
				response.getWriter().close();
				return;
			}
			boolean deleteAction = false;
			if (selectors.contains("deleteMetadata")) {
				deleteAction = true;
			}

			ThreadVO threadVO = new ThreadVO();
			threadVO.setDeleteAction(deleteAction);
			threadVO.setResourceResolver(resourceResolver);
			threadVO.setSession(session);
			threadVO.setStream(inputStream);
			threadVO.setChoicifyAppService(choicifyAppService);
			threadVO.setChoicifyReplicationService(choicifyReplicationService);
			String brand = request.getParameter("importbrand");
			threadVO.setBrand(brand);

			String country = request.getParameter("importcountry");
			threadVO.setCountry(country);

			ChoicifyMetadataImporterService choicifyMetadataImporter =
					new ChoicifyMetadataImporterService();
			choicifyMetadataImporter.setThreadVO(threadVO);
			ThreadPool pool = threadPoolManager.get(DIFF_POOL);
			if (pool == null) {
				ModifiableThreadPoolConfig threadPoolConfig = new ModifiableThreadPoolConfig();
				pool = threadPoolManager.create(threadPoolConfig, DIFF_POOL);
			}
			pool.execute(choicifyMetadataImporter);

			LOGGER.info("FINISHED: processing Colors and Shades from Excel file");
			response.setStatus(200);
			response.getWriter().println(
					"CSV Importer is started. Please reload this page after some seconds to see the results");
			response.getWriter().close();

		} catch (LoginException ex) {
			LOGGER.error("LoginException while reading ResourceResolver {}", ex);
		}
	}
}
