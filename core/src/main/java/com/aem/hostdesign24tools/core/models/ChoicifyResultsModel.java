package com.aem.hostdesign24tools.core.models;

import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.enums.ChoicifyBrandType;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.valueobjects.ColorsImportResults;
import com.aem.hostdesign24tools.core.valueobjects.CsvImportVO;
import com.aem.hostdesign24tools.core.valueobjects.CsvProductImportVO;
import com.aem.hostdesign24tools.core.valueobjects.ProductImportResults;
import com.google.gson.Gson;
import lombok.Getter;

/**
 * Model to get Hana and AEM store results in json format. This results are stored by a cron job.
 * 
 * @author HostDesign24.com
 */
@Model(adaptables = Resource.class)
public class ChoicifyResultsModel {

	@Getter
	private Date modificationTime;
	@Getter
	private List<CsvProductImportVO> failureProductResults;
	@Getter
	private List<CsvProductImportVO> successProductResults;

	@Getter
	private List<CsvImportVO> failureResults;
	@Getter
	private List<CsvImportVO> successResults;
	@Getter
	private List<CsvImportVO> noProductResults;

	@Self
	private Resource resource;

	@OSGiService
	private ChoicifyAppService choicifyAppService;

	@PostConstruct
	public void init() {
		String name = StringUtils.substringAfterLast(resource.getResourceType(), "/");
		if (!StringUtils.equals("importproducts", name)) {
			name = "metadataimporter";
		}
		String path = HostDesign24AppConstants.CHOICIFY_RESULTS_ROOT_PATH + name;
		Resource varResource = resource.getResourceResolver().getResource(path);
		if (varResource == null) {
			return;
		}
		ValueMap valueMap = varResource.adaptTo(ValueMap.class);
		if (valueMap != null) {
			String results = valueMap.get(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, String.class);
			modificationTime = valueMap.get("jcr:lastModified", Date.class);

			if (StringUtils.isNotEmpty(results)) {
				if (StringUtils.equals("importproducts", name)) {
					ProductImportResults productImportResults =
							new Gson().fromJson(results, ProductImportResults.class);
					if (productImportResults != null) {
						failureProductResults = productImportResults.getFailure();
						successProductResults = productImportResults.getSuccess();
					}
				} else {
					ColorsImportResults colorsImportResults =
							new Gson().fromJson(results, ColorsImportResults.class);
					if (colorsImportResults != null) {
						failureResults = colorsImportResults.getFailure();
						successResults = colorsImportResults.getSuccess();
						noProductResults = colorsImportResults.getNoProduct();
					}
				}
			}
		}
	}

	public List<String> getBrands() {
		return choicifyAppService.getAllowedsites();
	}

	public List<String> getCountries() {
		String defaultPath = String.format(HostDesign24AppConstants.CHOICIFY_COLORS_DEFAULT_PATH,
				ChoicifyBrandType.DM.getCountryType());
		return choicifyAppService.getChildNodes(defaultPath);
	}
}
