package com.aem.hostdesign24tools.core.services.impl;

import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.enums.ChoicifyBrandType;
import com.aem.hostdesign24tools.core.enums.ChoicifyCountryType;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.GenerateChoicifyStyleJsonResultsService;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.day.cq.commons.jcr.JcrConstants;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(GenerateChoicifyStyleJsonResultsService.class)
@Component(metatype = false, immediate = true, label = "Choicify Style Generate Json Results")
public class GenerateChoicifyStyleJsonResultsServiceImpl
		implements GenerateChoicifyStyleJsonResultsService {

	@Reference
	private ChoicifyAppService choicifyAppService;

	/**
	 * Execute thread importer Service.
	 */
	@Override
	public void processJsonResult(ResourceResolver resourceResolver,
			SlingHttpServletRequest request) {
		try {
			log.debug("GenerateJsonResultsService :: START: processing json results");
			generateJsonResults(resourceResolver, request);

			log.debug("GenerateJsonResultsService :: END: processing json results has been finished");
		} catch (RepositoryException ex) {
			log.error("RepositoryException {}", ex);
		} finally {
			ResourceResolverUtils.close(resourceResolver);
		}
	}


	private void generateJsonResults(ResourceResolver resourceResolver,
			SlingHttpServletRequest request) throws RepositoryException {

		String brand = request.getParameter("importbrand");
		String country = request.getParameter("importcountry");

		if (StringUtils.isBlank(brand)) {
			brand = ChoicifyBrandType.DM.getCountryType();
		}
		if (StringUtils.isBlank(country)) {
			country = ChoicifyCountryType.DE.getCountryType();
		}

		String searchPath = String.format(HostDesign24AppConstants.I18nTextPath, brand);
		Resource resource = resourceResolver.getResource(searchPath);
		List<Resource> resources = IteratorUtils.toList(resource.listChildren());
		for (Resource childresource : resources) {

			// store existing locales to content
			String basePath = String.format(choicifyAppService.getChoicifySearchPath(), brand, country);
			Session session = resourceResolver.adaptTo(Session.class);
			String cacheResultsPath = new StringBuilder(basePath).append("/")
					.append(HostDesign24AppConstants.CHOICIFY_JSON_RESULTS_NODE).toString();
			Node cacheResultsNode = JcrUtils.getOrCreateByPath(cacheResultsPath, false,
					JcrConstants.NT_UNSTRUCTURED, JcrConstants.NT_UNSTRUCTURED, session, true);
			if (cacheResultsNode.hasProperty("locales")) {
				String existingValue = cacheResultsNode.getProperty("locales").getString();
				if (StringUtils.isNotBlank(existingValue)) {
					existingValue = existingValue + "|" + childresource.getName();
					cacheResultsNode.setProperty("locales", existingValue);
				}
			} else {
				cacheResultsNode.setProperty("locales", childresource.getName());
			}
			cacheResultsNode.save();
			// end

			choicifyAppService.buildProductJsonResults(request, resourceResolver, choicifyAppService,
					childresource.getName(), brand, country);
			choicifyAppService.buildTargetResultsJson(resourceResolver, childresource.getName(), brand,
					country);
			choicifyAppService.buildNaturalJsonResults(resourceResolver, childresource.getName(), brand,
					country);
		}
	}
}
