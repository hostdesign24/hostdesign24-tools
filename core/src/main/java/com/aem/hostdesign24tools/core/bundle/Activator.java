package com.aem.hostdesign24tools.core.bundle;

import javax.jcr.Session;
import javax.jcr.security.AccessControlManager;
import javax.jcr.security.AccessControlPolicy;
import javax.jcr.security.AccessControlPolicyIterator;
import javax.jcr.security.Privilege;
import org.apache.jackrabbit.api.security.JackrabbitAccessControlList;
import org.apache.jackrabbit.api.security.JackrabbitAccessControlPolicy;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
public class Activator implements BundleActivator {

	private ResourceResolverFactory resourceResolverFactory = null;

	public void start(final BundleContext context) throws Exception {
		try {
			this.getResourceResolverFactory(context);
			if (this.resourceResolverFactory != null) {
				this.setPermissionForChoicifyAdmin();
			}
		} catch (Exception ex) {
			log.error("error on activating bundle", ex);
			throw ex;
		}
		log.info("bundle started");
	}

	public void stop(final BundleContext context) throws Exception {
		log.info("bundle stopped");
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	private ResourceResolverFactory getResourceResolverFactory(final BundleContext context)
			throws Exception {
		if (this.resourceResolverFactory == null) {
			final ServiceReference serviceReference =
					context.getServiceReference(ResourceResolverFactory.class.getName());
			if (serviceReference != null) {
				Object service = context.getService(serviceReference);
				if (service != null) {
					this.resourceResolverFactory = (ResourceResolverFactory) service;
				}
			}
		}
		return this.resourceResolverFactory;
	}

	@SuppressWarnings("deprecation")
	private void setPermissionForChoicifyAdmin() throws Exception {
		ResourceResolver adminResourceResolver = null;
		try {
			Privilege[] privileges = null;
			Authorizable authorizable = null;
			// should not use admin rights!
			adminResourceResolver = this.resourceResolverFactory.getAdministrativeResourceResolver(null);
			final Session session = adminResourceResolver.adaptTo(Session.class);
			final Resource rootResource = adminResourceResolver.getResource("/");
			final UserManager userManager = rootResource.adaptTo(UserManager.class);
			final AccessControlManager accessControlManager = session.getAccessControlManager();

			final AccessControlPolicy[] policies = accessControlManager.getPolicies("/");
			JackrabbitAccessControlList controlList = null;
			if (policies == null || policies.length == 0) {
				AccessControlPolicyIterator policyIterator =
						accessControlManager.getApplicablePolicies("/");
				while (controlList == null && policyIterator.hasNext()) {
					AccessControlPolicy policy = policyIterator.nextAccessControlPolicy();
					if (policy instanceof JackrabbitAccessControlPolicy) {
						controlList = (JackrabbitAccessControlList) policy;
					}
				}
			} else {
				controlList = (JackrabbitAccessControlList) policies[0];
			}

			authorizable = userManager.getAuthorizable("hostdesign24admin");
			privileges = new Privilege[] {accessControlManager.privilegeFromName(Privilege.JCR_ALL)};
			controlList.addEntry(authorizable.getPrincipal(), privileges, true);

			accessControlManager.setPolicy(controlList.getPath(), controlList);
			adminResourceResolver.adaptTo(Session.class).save();
			log.info("added permission jcr:all to hostdesign24admin at /");
		} finally {
			if (adminResourceResolver != null && adminResourceResolver.isLive()) {
				adminResourceResolver.close();
			}
		}
	}
}
