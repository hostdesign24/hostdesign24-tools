package com.aem.hostdesign24tools.core.services;

import java.util.List;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.models.ProductModel;
import com.aem.hostdesign24tools.core.valueobjects.CsvImportVO;
import com.aem.hostdesign24tools.core.valueobjects.naturalcolors.NaturalColors;
import com.aem.hostdesign24tools.core.valueobjects.productsearch.ProductResult;
import com.aem.hostdesign24tools.core.valueobjects.targetcolors.TargetColors;
import com.aem.hostdesign24tools.core.valueobjects.targetnaturalcolors.TargetNaturalColors;
import com.day.cq.search.result.Hit;
import com.day.cq.tagging.Tag;

/**
 * @author HostDesign24.com
 *
 */
public interface ChoicifyAppService {

	List<String> getAllowedsites();

	Map<String, String> getTargetHairShadeGUIDs();

	String createTagValueStoredInJcrContent(Tag tag);

	List<Hit> searchProductPageByGtins(ResourceResolver resourceResolver, String propertyName,
			CsvImportVO csvImportVO);

	String getChoicifyDomainName();

	List<Resource> getResourcesFromSearchResult(List<Hit> searchResult);

	void buildProductJsonResults(SlingHttpServletRequest request,
			final ResourceResolver resourceResolver, final ChoicifyAppService choicifyAppService,
			final String locale, final String brand, final String country) throws RepositoryException;

	List<ProductModel> findProductNodesByGtin(final String gtin, ResourceResolver resourceResolver,
			String brand, String country);

	ProductResult searchProudctPagesByTags(SlingHttpServletRequest request,
			ResourceResolver resourceResolver, List<String> selectors, final String locale,
			final String brand, final String country);

	TargetColors searchTargetHairColorTagsOnPages(ResourceResolver resourceResolver,
			List<String> selectors, final String locale, final String brand, final String country)
			throws RepositoryException;

	void buildTargetResultsJson(ResourceResolver resourceResolver, String locale, String brand,
			String country);

	NaturalColors searchNaturalHairColorTagsOnPages(ResourceResolver resourceResolver, String locale,
			String brand, String country);

	void buildNaturalJsonResults(ResourceResolver resourceResolver, String locale, String brand,
			String country);

	List<String> getAvailableCountrys();

	List<String> getChildNodes(String path);

	String getChoicifySearchPath();

	void buildTargetNaturalJsonResults(ResourceResolver resourceResolver, String locale, String brand,
			String country);

	TargetNaturalColors searchTargetNaturalHairColorTagsOnPages(ResourceResolver resourceResolver,
			String locale, String brand, String country);

	void saveJsonResults(Session session, Object results, String locale, String savePath);

	Resource getValidTagResource(String tagName, ResourceResolver resourceResolver, String rootPath);
}
