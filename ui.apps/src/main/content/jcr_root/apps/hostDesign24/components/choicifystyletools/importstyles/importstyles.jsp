<%@page session="false"
	import="org.apache.sling.commons.json.io.JSONStringer,
          	com.adobe.granite.ui.components.AttrBuilder,
            com.adobe.granite.ui.components.Config,
			com.adobe.granite.ui.components.ComponentHelper,
            com.adobe.granite.ui.components.Tag"%>
<%@include file="/apps/hostDesign24/global.jsp"%>
<%
  final ComponentHelper cmp = new ComponentHelper(pageContext);
  if (!cmp.getRenderCondition(resource, false).check()) {
    return;
  }
  /*DON'T REMOVE */
  Config cfg = cmp.getConfig();

  Tag tag = cmp.consumeTag();
  AttrBuilder attrs = tag.getAttrs();
  cmp.populateCommonAttrs(attrs);

  String targetCollection = cfg.get("targetCollection", String.class);
  String consoleId = cfg.get("consoleId", String.class);
  String title = cfg.get("jcr:title", String.class);
  String pageURITemplate = cfg.get("pageURITemplate", String.class);

  attrs.addClass("granite-collection-pagetitle");
  attrs.add("data-granite-collection-pagetitle-target", targetCollection);
  attrs.add("data-granite-collection-pagetitle-src", pageURITemplate);
  attrs.add("data-granite-collection-pagetitle-base", title);
%>

<choicifysling:adaptTo adaptable="${resource}"
	adaptTo="com.aem.hostdesign24tools.core.models.ChoicifyResultsModel" var="choicify" />
<coral-importproducts <%=attrs%>>
<div class="coral--light choicify-tools-page">
	<div id="choicify-tools-app">
		<div class="page" role="main">
			<div class="content">
				<div class="content-container">
					<div class="content-container-inner">
						<div>
							<h2>Upload Products CSV File</h2>

							<!-- The form starts -->
							<form action="/" method="post" enctype="multipart/form-data" id="importProducts" >
								<input name="_charset_" type="hidden" value="utf-8">
								<coral-fileupload id="fileUpload" name="productsFile" class="coral-Form-field coral3-FileUpload" required action="/">
									<div coral-fileupload-dropzone class="coral-FileUpload-dropZone">
										<span class="dropzone drop-zone-text">&nbsp;Drop files here or </span>
										<button coral-fileupload-select is="coral-button" variant="secondary">Select files...</button>
										<input class="selectedfilename" is="coral-textfield" placeholder="File name" name="selectedfilename" value="" disabled readonly>
									</div>
								</coral-fileupload>
                                <br/>
                                
                                <h2>Select the brand you want to upload products</h2>
                                <coral-select name="importcountry" placeholder="Choose brand" required>
                                	<c:forEach var="type" items="${choicify.brands}" varStatus="status">
								  		<coral-select-item value="${type}">${type}</coral-select-item>
								  	</c:forEach>
								</coral-select>
								<br/><br/>
								
								<p>
									<coral-checkbox type="checkbox" id="deleteImportedContent"
										name="cleannContent" value="true">Delete imported styles </coral-checkbox>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</coral-importproducts>

<style>
tbody, tr, td, th {
	border: 1px solid;
}
</style>