package com.aem.hostdesign24tools.core.services;

import org.apache.sling.api.resource.ResourceResolver;

/**
 * @author HostDesign24.com
 *
 */
public interface ScrappingToolForICAService {

	public void process(ResourceResolver resourceResolver);

}
