package com.aem.hostdesign24tools.core.enums;

import lombok.Getter;

/**
 * @author HostDesign24.com
 *
 */
public enum ChoicifyBrandType {
  DM("dm");

  @Getter
  private String countryType;

  ChoicifyBrandType(final String type) {
    this.countryType = type;
  }
}
