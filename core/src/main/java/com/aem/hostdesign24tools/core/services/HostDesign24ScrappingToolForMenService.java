package com.aem.hostdesign24tools.core.services;

import javax.jcr.Session;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

/**
 * @author HostDesign24.com
 *
 */
public interface HostDesign24ScrappingToolForMenService {

	public void process(ResourceResolver resourceResolver, SlingHttpServletRequest request);

	public void processSchedularInputs(String[] urlmapper, Session session);

}
