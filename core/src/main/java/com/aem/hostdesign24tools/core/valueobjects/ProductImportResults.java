package com.aem.hostdesign24tools.core.valueobjects;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class ProductImportResults {

	private List<CsvProductImportVO> failure = new ArrayList<>();
	private List<CsvProductImportVO> success = new ArrayList<>();
}
