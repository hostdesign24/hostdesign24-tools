package com.aem.hostdesign24tools.core.valueobjects.productsearch;

import java.util.ArrayList;
import java.util.List;
import com.aem.hostdesign24tools.core.valueobjects.Filter;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class ProductResult {
	@Expose
	private List<ProductSearchModel> products = new ArrayList<>();

	@Expose
	private List<Filter> filters = new ArrayList<>();

}
