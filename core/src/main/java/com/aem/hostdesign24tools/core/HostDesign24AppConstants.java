package com.aem.hostdesign24tools.core;

/**
 * @author HostDesign24.com
 *
 */
public class HostDesign24AppConstants {


	public static final String JCR_PRIMARY_TYPE_UNSTRUCTURED = "nt:unstructured";
	public static final String STRING_SEPERATOR_SLASH = "/";
	public static final String STRING_SEPERATOR_COLON = ":";
	public static final String QUERY_STRING_PATH_NAME = "path";
	public static final String EMPTY_STRING = "";
	public static final String QUERY_STRING_TYPE_NAME = "type";
	public static final String PROP_NAME_COLOR_RESULTS = "results";
	public static final String JCR_CONTENT_NODE = "jcr:content";
	public static final String CQ_PAGE_CONTENT_NODE = "cq:PageContent";
	public static final String JCR_TITLE_NODE = "jcr:title";
	public static final String CQ_PAGE_NODE = "cq:Page";
	public static final String CQ_TEMPLATE_PROPERTY = "cq:template";
	public static final String SLING_RESOURCETYPE = "sling:resourceType";
	public static final String SLING_ORDERED_FOLDER = "sling:OrderedFolder";
	public static final String HOSTDESIGN24_SERVICE = "hostDesign24Service";
	public static final String QUERY_STRING_LIMIT_NAME = "p.limit";
	public static final String CHOICIFY_SERVLET_SELECTOR = "choicify";
	public static final String HTML_EXTENSION = ".html";
	public static final String HIDE_IN_NAVIGATION = "hideInNav";
	public static final String JCR_NODE_NAME_PRODUCT = "product";
	public static final String JCR_NODE_NAME_PACKSHOT = "packshot";

	public static final String CHOICIFY_TAG_ROOT_PATH_NAMESPACE = "/etc/tags/choicify";
	public static final String CHOICIFY_TAG_FILTER_ROOT_PATH = "/etc/tags/choicify/filters";
	public static final String CHOICIFY_TAG_DURABILITY_NAME = "durability";
	public static final String CHOICIFY_TAG_PERMANENT_NAME = "permanent";
	public static final String CHOICIFY_TAG_TEMPORARY_NAME = "temporary";
	public static final String CHOCIFY_ASSET_EXTENSION_PNG = ".png";
	public static final String CHOCIFY_IMAGE_WOMAN_HEAD_NAME = "Woman";
	public static final String CHOCIFY_FOLDER_NAME_FAMILY = "Family";

	public static final String JSON_NAME_PRODUCT_FILTER = "filters";
	public static final String JSON_NAME_PRODUCTS = "products";
	public static final String JSON_NAME = "name";
	public static final String JSON_KEY = "key";
	public static final String JSON_OPTIONS = "options";
	public static final String JSON_TAGS = "tags";
	public static final String JSON_NAME_BRAND_PAGE_TITLE = "brandPagetitle";
	public static final String JSON_NAME_PRODUCT_PAGE_TITLE = "productPageTitle";
	public static final String JSON_NAME_PRODUCT_TITLE = "productTitle";
	public static final String JSON_NAME_PRODUCT_PAGE_URL = "productPageUrl";
	public static final String JSON_NAME_PRODCUT_IMAGE_URL = "productImageUrl";
	public static final String JSON_NAME_SUGGESTED_RETAIL_PRICE = "suggestedRetailsPrice";
	public static final String JSON_NAME_SELECTED_STORE_ONLY = "selectedStoreOnly";

	public static final String NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR = "naturalcolors";
	public static final String TARGET_HAIR_COLORS_AND_SHADES_SELECTOR = "targetcolors";
	public static final String TARGET_NATURAL_COLORS = "targetnaturalcolors";

	public static final String CHOICIFY_RESULTS_ROOT_PATH = "/var/choicify/";
	public static final String CHOICIFY_HEALTHCHECK_RESULT_PATH =
			CHOICIFY_RESULTS_ROOT_PATH + "healthCheckProducts";
	public static final String CHOICIFY_DAM_ROOT_PATH = "/content/dam/choicify";
	public static final String CHOICIFY_COLORS_DAM_ROOT_PATH = "/content/dam/choicify/colors";
	public static final String CHOICIFY_PRODUCT_ROOT_PATH = "/content/choicify/";
	public static final String CHOCIFY_ASSET_EXTENSION_JPG = ".jpg";
	public static final String CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR = "/etc/tags/choicify/haircolor";
	public static final String TAG_DURABILITY_ROOT_PATH = "/etc/tags/choicify/filters/durability/";
	public static final String TAG_BRANDS_ROOT_PATH = "/etc/tags/choicify/filters/brands/";
	public static final String ROOT_PATH_NATURAL_HAIR_COLOR =
			CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR + STRING_SEPERATOR_SLASH + "naturalhair";
	public static final String ROOT_PATH_TARGET_HAIR_COLOR =
			CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR + STRING_SEPERATOR_SLASH + "targethair";
	public static final String CHOICIFY_ROOT_PATH = "/choicify";
	/** Gtin name for products. **/
	public static final String CHOICIFY_GTIN_NAME = "gtin";

	public static final String SCRAPPING_PRODUCTS_PATH_FOR_MEN =
			"/var/hostDesign24/scrappingPathForMen";
	public static final String SCRAPPING_PRODUCTS_PATH_FOR_ICA =
			"/var/hostDesign24/scrappingPathForica";
	public static final String SCRAPPING_PRODUCTS_PATH_FOR_WOMEN = "/var/hostDesign24/scrappingPath";

	public static final String NATURAL_HAIR_COLOR_ABBREVIATION = "nhc";
	public static final String NATURAL_HAIR_SHADE_ABBREVIATION = "nhs";
	public static final String TARGET_HAIR_COLOR_ABBREVIATION = "thc";
	public static final String TARGET_HAIR_SHADE_ABBREVIATION = "ths";

	public static final String TARGET_HAIR_COLORS_PROP_NAME = "targetHairColor";
	public static final String TARGET_HAIR_SHADES_PROP_NAME = "targetHairShade";
	public static final String NATURAL_HAIR_COLORS_PROP_NAME = "naturalHairColor";
	public static final String NATURAL_HAIR_SHADES_PROP_NAME = "naturalHairShade";

	public static final String CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR =
			"/etc/tags/choicify/haircolor/naturalhair";
	public static final String CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR =
			"/etc/tags/choicify/haircolor/targethair";

	/** Resourcetype Constants. */
	public static final String RESOURCETYPE_FOLDER = "choicify/components/page/folderpage";
	public static final String RESOURCETYPE_PRODUCT = "choicify/components/page/productpage";

	/** Template Constants. */
	public static final String TEMPLATE_FOLDER = "/apps/choicify/templates/folderpage";
	public static final String TEMPLATE_PRODUCT = "/apps/choicify/templates/productpage";

	/** Product Constants. */
	public static final String CHOICIFY_PRODUCT_URL = "productUrl";
	public static final String CHOICIFY_PRODUCT_PAGE_PATH = "productPagePath";
	public static final String CHOICIFY_FILE_REFERENCE = "fileReference";
	public static final String BRANDS = "brands";
	public static final String BRAND_TITLE = "brandTitle";
	public static final String PRODUCT_PRIORITY = "productPriority";
	public static final String COLOR_TILE = "colorTile";
	public static final String COLORATIONPATHS = "/colorationPaths";
	public static final String PRODUCT_COLORATIONS_PATH = "/choicify/colorationPaths/";

	public static final String CHOICIFY_PRODUCT_RESULT_PATH =
			CHOICIFY_RESULTS_ROOT_PATH + "importproducts";
	public static final String CHOICIFY_METADATA_RESULT_PATH =
			CHOICIFY_RESULTS_ROOT_PATH + "metadataimporter";

	public static final String CHOICIFY_HAIRCOLOR_BRAND = "haircolor";
	public static final String CHOICIFY_HAIRSTYLE_BRAND = "hairstyle";

	public static final String CHOICIFY_JSON_RESULTS_NODE = "cachedResults";
	public static final String PERFECT_CAMCODE_TILE_PATH =
			"https://schwarzkopf.de/content/dam/choicify/perfectcam/";
	public static final String CHOICIFY_COLORS_DEFAULT_PATH = "/content/choicify/%s/haircolor/";
	public static final String I18nTextPath = "/apps/hostDesign24/translations/%s/i18n";
	public static final String MISSING_PROP = "missingcolor_";

}
