package com.aem.hostdesign24tools.core.services.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.enums.ChoicifyBrandType;
import com.aem.hostdesign24tools.core.enums.ChoicifyCountryType;
import com.aem.hostdesign24tools.core.excel.ExcelReaderException;
import com.aem.hostdesign24tools.core.excel.PoiExcelReader;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.aem.hostdesign24tools.core.services.GenerateFallbackMessagesService;
import com.aem.hostdesign24tools.core.utils.ResourceResolverUtils;
import com.aem.hostdesign24tools.core.valueobjects.FilterOption;
import com.aem.hostdesign24tools.core.valueobjects.fallbackmessage.MissingColor;
import com.aem.hostdesign24tools.core.valueobjects.productsearch.ProductTag;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(GenerateFallbackMessagesService.class)
@Component(metatype = false, immediate = true,
		label = "Generate Fallback Messages for Color Combinations")
public class GenerateFallbackMessagesServiceImpl implements GenerateFallbackMessagesService {

	@Reference
	private ChoicifyAppService choicifyAppService;

	private final static String requestUrl =
			"https://www.schwarzkopf.de/de/home.choicify.%s.haircolor.%s.%s.products.%s.%s.json";

	/**
	 * Execute thread importer Service.
	 */
	@Override
	public void process(InputStream inputStream, ResourceResolver resourceResolver,
			SlingHttpServletRequest request) {
		try {
			log.debug("GenerateFallbackMessagesService :: START: processing");
			generateJsonResults(inputStream, resourceResolver, request);

			log.debug("GenerateFallbackMessagesService :: END: process has been finished");
		} catch (RepositoryException | ExcelReaderException ex) {
			log.error("Exception {}", ex);
		} finally {
			ResourceResolverUtils.close(resourceResolver);
		}
	}


	private void generateJsonResults(InputStream inputStream, ResourceResolver resourceResolver,
			SlingHttpServletRequest request) throws RepositoryException, ExcelReaderException {

		String brand = request.getParameter("importbrand");
		String country = request.getParameter("importcountry");


		if (StringUtils.isBlank(brand)) {
			brand = ChoicifyBrandType.DM.getCountryType();
		}
		if (StringUtils.isBlank(country)) {
			country = ChoicifyCountryType.DE.getCountryType();
		}

		String basePath = String.format(choicifyAppService.getChoicifySearchPath(), brand, country);
		Session session = resourceResolver.adaptTo(Session.class);

		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(HostDesign24AppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(HostDesign24AppConstants.JSON_NAME_PRODUCTS).toString();

		if (jsonNodePath != null) {
			// read workbook
			Workbook workbook = PoiExcelReader.read(inputStream);
			Sheet sheet = workbook.getSheetAt(0);
			int rowcount = 0;

			String otherLanguage = StringUtils.EMPTY;
			String localLanguage = StringUtils.EMPTY;
			List<String> possibleCombination;
			MissingColor missingColor;

			TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
			for (Row row : sheet) {
				// skip for header row
				if (rowcount == 0) {

					otherLanguage = row.getCell(4).toString();
					localLanguage = row.getCell(5).toString();
					if (StringUtils.isNotBlank(otherLanguage)) {
						otherLanguage = StringUtils.lowerCase(otherLanguage);
					}
					if (StringUtils.isNotBlank(localLanguage)) {
						localLanguage = StringUtils.lowerCase(localLanguage);
					}
					rowcount++;
					continue;
				}
				log.info("processing row {}", rowcount);
				// read all columns from row
				Cell initialNhs = row.getCell(0);
				Cell initialThs = row.getCell(1);
				Cell recommendedThs = row.getCell(2);
				Cell recommendedNhs = row.getCell(3);
				Cell messageInOtherLanguage = row.getCell(4);
				Cell messageInLocalLanguage = row.getCell(5);
				rowcount++;
				if (StringUtils.isBlank(initialNhs.toString())) {
					log.debug("CSV file content has empty line.");
					break;
				}

				// fallback case
				if (recommendedThs == null) {
					recommendedThs = initialThs;
				}
				if (recommendedNhs == null) {
					recommendedNhs = initialNhs;
				}
				possibleCombination = new ArrayList<>();
				possibleCombination.add(initialNhs.toString());
				possibleCombination.add(initialThs.toString());

				String nodeName = StringUtils.join(possibleCombination, "_");
				String savePath = jsonNodePath + HostDesign24AppConstants.STRING_SEPERATOR_SLASH + nodeName;

				if (StringUtils.isNotBlank(localLanguage)
						&& StringUtils.isNotBlank(messageInLocalLanguage.toString())) {

					List<ProductTag> tags =
							getProductTags(tagManager, resourceResolver, localLanguage, initialNhs.toString(),
									initialThs.toString(), recommendedNhs.toString(), recommendedThs.toString());
					String requestedUrl = String.format(requestUrl, brand, country, localLanguage,
							recommendedNhs.toString(), recommendedThs.toString().trim());
					missingColor = new MissingColor();
					missingColor.setErrorMessage(messageInLocalLanguage.toString());
					missingColor.setLanguage(localLanguage);
					missingColor.setRequestUrl(requestedUrl);
					missingColor.setTags(tags);
					String jsonpropetyName = HostDesign24AppConstants.MISSING_PROP + localLanguage;
					choicifyAppService.saveJsonResults(session, missingColor, jsonpropetyName, savePath);
				}
				if (StringUtils.isNotBlank(otherLanguage)
						&& StringUtils.isNotBlank(messageInOtherLanguage.toString())) {
					List<ProductTag> tags =
							getProductTags(tagManager, resourceResolver, otherLanguage, initialNhs.toString(),
									initialThs.toString(), recommendedNhs.toString(), recommendedThs.toString());
					String requestedUrl = String.format(requestUrl, brand, country, otherLanguage,
							recommendedNhs.toString(), recommendedThs.toString().trim());
					missingColor = new MissingColor();
					missingColor.setErrorMessage(messageInOtherLanguage.toString());
					missingColor.setLanguage(otherLanguage);
					missingColor.setRequestUrl(requestedUrl);
					missingColor.setTags(tags);
					String jsonpropetyName = HostDesign24AppConstants.MISSING_PROP + otherLanguage;
					choicifyAppService.saveJsonResults(session, missingColor, jsonpropetyName, savePath);
				}
			}
		}
	}

	private List<ProductTag> getProductTags(TagManager tagManager, ResourceResolver resourceResolver,
			String locale, String nhs, String ths, String recommendednhs, String recommendedths) {

		List<ProductTag> tags = new ArrayList<>();
		// requested colors
		ProductTag productTag = new ProductTag();
		productTag.setKey("requestColors");
		productTag.setName("requestColors");
		List<FilterOption> options = new ArrayList<>();

		FilterOption filterOption = findValidTagResource(tagManager, resourceResolver, locale, nhs);
		options.add(filterOption);
		filterOption = findValidTagResource(tagManager, resourceResolver, locale, ths);
		options.add(filterOption);
		productTag.setOptions(options);
		tags.add(productTag);

		// recommended colors
		productTag = new ProductTag();
		productTag.setKey("recommendedColors");
		productTag.setName("recommendedColors");
		options = new ArrayList<>();
		filterOption = findValidTagResource(tagManager, resourceResolver, locale, recommendednhs);
		options.add(filterOption);
		filterOption = findValidTagResource(tagManager, resourceResolver, locale, recommendedths);
		options.add(filterOption);
		productTag.setOptions(options);
		tags.add(productTag);

		return tags;
	}


	private FilterOption findValidTagResource(TagManager tagManager,
			ResourceResolver resourceResolver, String locale, String shade) {
		FilterOption filterOption = new FilterOption();
		shade = shade.trim();
		Resource tagResource = getTagResource(shade, resourceResolver);
		if (tagResource != null) {
			String naturalHairShade =
					choicifyAppService.createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
			Tag tag = tagManager.resolve(naturalHairShade);
			if (tag != null) {
				filterOption.setKey(shade);
				filterOption.setName(tag.getTitle(new Locale(locale)));
			}
		} else {
			log.error("Tag Resources are empty for {}", shade);
		}
		return filterOption;
	}

	private Resource getTagResource(String selector, ResourceResolver resourceResolver) {
		Resource tagResource = choicifyAppService.getValidTagResource(selector, resourceResolver,
				HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
		if (tagResource == null) {
			tagResource = choicifyAppService.getValidTagResource(selector, resourceResolver,
					HostDesign24AppConstants.CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR);
		}

		return tagResource;
	}
}
