package com.aem.hostdesign24tools.core.valueobjects;

/**
 * @author HostDesign24.com
 *
 */
public class ProductHealthCheckVO {

  private String gtin;
  private String initialColorFamily;
  private String initialShadeName;
  private String desiredColorFamily;
  private String desiredShadeName;
  private boolean hasChoicify;
  private boolean isPublished;
  private boolean isColorCombinationExist;
  private boolean isProductPageExist;
  private String productPath;

  public boolean isProductPageExist() {
    return isProductPageExist;
  }

  public void setProductPageExist(boolean isProductPageExist) {
    this.isProductPageExist = isProductPageExist;
  }

  public boolean isColorCombinationExist() {
    return isColorCombinationExist;
  }

  public void setColorCombinationExist(boolean isColorCombinationExist) {
    this.isColorCombinationExist = isColorCombinationExist;
  }

  public String getGtin() {
    return gtin;
  }

  public void setGtin(String gtin) {
    this.gtin = gtin;
  }

  public boolean isPublished() {
    return isPublished;
  }

  public void setPublished(boolean isPublished) {
    this.isPublished = isPublished;
  }

  public boolean isHasChoicify() {
    return hasChoicify;
  }

  public void setHasChoicify(boolean hasChoicify) {
    this.hasChoicify = hasChoicify;
  }

  public String getInitialColorFamily() {
    return initialColorFamily;
  }

  public void setInitialColorFamily(String initialColorFamily) {
    this.initialColorFamily = initialColorFamily;
  }

  public String getInitialShadeName() {
    return initialShadeName;
  }

  public void setInitialShadeName(String initialShadeName) {
    this.initialShadeName = initialShadeName;
  }

  public String getDesiredColorFamily() {
    return desiredColorFamily;
  }

  public void setDesiredColorFamily(String desiredColorFamily) {
    this.desiredColorFamily = desiredColorFamily;
  }

  public String getDesiredShadeName() {
    return desiredShadeName;
  }

  public void setDesiredShadeName(String desiredShadeName) {
    this.desiredShadeName = desiredShadeName;
  }

public String getProductPath() {
	return productPath;
}

public void setProductPath(String productPath) {
	this.productPath = productPath;
}

}
