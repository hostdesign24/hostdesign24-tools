package com.aem.hostdesign24tools.core.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.ImportProductsService;
import com.aem.hostdesign24tools.core.services.ImportStyleProductsService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@SlingServlet(resourceTypes = "sling/servlet/default",
		selectors = {"createschwarzkopfproducts", "createstyleproducts"}, extensions = "json",
		methods = {"GET", "POST"})
public class ImportProductsServlet extends AbstractAllMethodsServlet {

	private static final long serialVersionUID = -6978585149802445252L;

	@Reference
	private ImportProductsService importProductsService;

	@Reference
	private ImportStyleProductsService importStyleProductsService;

	private ResourceResolver resourceResolver;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		response.setContentType("text/plain");
		try {

			if (!ServletFileUpload.isMultipartContent(request)) {
				response.setStatus(500);
				log.debug("Internal server error: is not MultipartContent");
				response.getWriter().println("Internal server error: is not MultipartContent");
				response.getWriter().close();
				return;
			}
			resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);

			RequestParameter[] productsFiles = request.getRequestParameters("productsFile");
			InputStream inputStream = null;
			if (productsFiles != null) {
				RequestParameter productsFile = productsFiles[0];
				inputStream = productsFile.getInputStream();
				if (inputStream == null) {
					response.setStatus(405);
					response.getWriter().println("File not properly uploaded");
					response.getWriter().close();
					return;
				}
			}
			final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
			if (selectors.contains("createschwarzkopfproducts")) {
				importProductsService.process(inputStream, resourceResolver, request);
			} else if (selectors.contains("createstyleproducts")) {
				importStyleProductsService.process(inputStream, resourceResolver, request);
			}

			response.setStatus(200);
			response.getWriter()
					.println("Products has been created. Please reload this page to see the results");
			response.getWriter().close();
		} catch (LoginException ex) {
			log.error("LoginException while reading ResourceResolver {}", ex);
		}
	}

}
