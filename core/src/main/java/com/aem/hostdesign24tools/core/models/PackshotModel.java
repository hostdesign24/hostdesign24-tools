package com.aem.hostdesign24tools.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

/**
 * Model for images.
 * 
 * @author HostDesign24.com
 */
@Model(adaptables = Resource.class)
public class PackshotModel {
  @Inject
  @Optional
  private String fileReference;

  @PostConstruct
  public void postConstruct() {
    //do nothing
  }

  public String getFileReference() {
    return this.fileReference;
  }
}
