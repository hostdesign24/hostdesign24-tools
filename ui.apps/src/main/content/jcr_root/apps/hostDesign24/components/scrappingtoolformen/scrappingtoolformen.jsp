<%@page session="false"%>
<%@ page import="java.util.Locale,
java.util.ResourceBundle,
javax.jcr.NodeIterator,
javax.jcr.Session,
javax.jcr.Node, javax.jcr.Value, javax.jcr.NodeIterator,
org.apache.sling.api.resource.ValueMap,
com.day.cq.commons.jcr.*" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/hostDesign24/global.jsp"%>
<cq:includeClientLib categories="cq.jquery" />
<cq:includeClientLib categories="hostDesign24.component.tools.scrappingtoolformen" />
<div class="coral--light choicify-tools-page">
	<div id="choicify-tools-app">
		<div class="page" role="main">
			<div class="content">
				<div class="content-container">
					<div class="content-container-inner">
						<div>
							<h2>HostDesign24 Scrapping Tool For Men</h2>
							<p id="support-notice">Your browser does not support Ajax
								uploads :-(The form will be submitted as normal.</p>
							<!-- The form starts -->
							<form action="/" method="POST" enctype="multipart/form-data"
								id="form-id">
								
							
								<p>
									<input type="button" value="Start Process" id="upload-button-id"
										disabled="disabled" />
								</p>
								<!-- Placeholders for messages set by event handlers -->
								<p id="upload-status"></p>
								<p id="csvImporterProgress"></p>
								<p id="csvImporterResultSuccess"></p>
								<p id="csvImporterResultFailue"></p>
							</form>
						</div>
                        <div id="timeid">
						<%
Session ses = resourceResolver.adaptTo(Session.class);
Resource resources = slingRequest.getResourceResolver().getResource("/apps/hostDesign24/components/scrappingtoolformen/timestamp");
if(resources!=null){
ValueMap vm = resources.adaptTo(ValueMap.class); 
%>
                        <%=vm.get("time", String.class) %>
<%
}%>
                        </div>
                        <br/>
						<a x-cq-linkchecker="skip"
							href="/etc/hostDesign24/productimporter.scrappingtoolformen.json"
							class="scrappingToolDownload">Download</a>
                        
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
tbody, tr, td, th {
	border: 1px solid;
}
</style>