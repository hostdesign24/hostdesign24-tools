package com.aem.hostdesign24tools.core.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpStatus;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.GenerateChoicifyStyleJsonResultsService;
import com.aem.hostdesign24tools.core.services.GenerateJsonResultsService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@SlingServlet(resourceTypes = "sling/servlet/default",
		selectors = {"generatejsonresults", "generateStyeJsonResults"}, extensions = "json",
		methods = {"POST"})
public class GenerateJsonResultsServlet extends AbstractAllMethodsServlet {

	private static final long serialVersionUID = -6978585149802445252L;

	@Reference
	private GenerateJsonResultsService generateJsonResultsService;

	@Reference
	private GenerateChoicifyStyleJsonResultsService generateChoicifyStyleJsonResultsService;

	private ResourceResolver resourceResolver;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		response.setStatus(HttpStatus.SC_METHOD_NOT_ALLOWED);
		return;
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		response.setContentType("text/plain");
		try {

			this.resourceResolver = getResourceResolver(HostDesign24AppConstants.HOSTDESIGN24_SERVICE);
			final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
			if (selectors.contains("generatejsonresults")) {
				this.generateJsonResultsService.processJsonResult(resourceResolver, request);
			} else if (selectors.contains("generateStyeJsonResults")) {
				generateChoicifyStyleJsonResultsService.processJsonResult(resourceResolver, request);
			}

			response.setStatus(200);
			return;
		} catch (LoginException ex) {
			log.error("LoginException while reading ResourceResolver {}", ex);
		}
	}

}
