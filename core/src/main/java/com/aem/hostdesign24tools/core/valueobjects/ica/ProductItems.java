package com.aem.hostdesign24tools.core.valueobjects.ica;

import java.util.List;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class ProductItems {

	private List<ProductItem> items;
}
