package com.aem.hostdesign24tools.core.valueobjects.styleformen;

import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class Link {

	private String href;
	private String rel;
}
