package com.aem.hostdesign24tools.core.valueobjects.styleformen;

import java.util.List;
import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class SearchResults {

	private List<ServiceProduct> serviceProducts;
}
