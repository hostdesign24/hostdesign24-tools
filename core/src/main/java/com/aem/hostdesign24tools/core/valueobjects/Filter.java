package com.aem.hostdesign24tools.core.valueobjects;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

/**
 * @author HostDesign24.com
 *
 */
public class Filter {

  @Expose
  private String name;
  @Expose
  private String key;
  @Expose
  private String type = "select";
  @Expose
  private List<FilterOption> options = new ArrayList<>();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<FilterOption> getOptions() {
    return options;
  }

  public void setOptions(List<FilterOption> options) {
    this.options = options;
  }
}
