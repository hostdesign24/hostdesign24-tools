/**
 *
 */
package com.aem.hostdesign24tools.core.utils;

import java.util.Locale;
import java.util.ResourceBundle;
import org.apache.sling.api.SlingHttpServletRequest;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 * @author HostDesign24.com
 *
 */
public class I18nUtils {

	public static I18n getI18n(final SlingHttpServletRequest request) {
		final String currentPagePath = request.getRequestPathInfo().getResourcePath();
		I18n i18n = null;
		final Page currentPage =
				request.getResourceResolver().adaptTo(PageManager.class).getContainingPage(currentPagePath);

		if (currentPage != null) {
			final Locale pageLocale = currentPage.getLanguage(false);
			final ResourceBundle resourceBundle = request.getResourceBundle(pageLocale);
			i18n = new I18n(resourceBundle);
		} else {
			ResourceBundle resourceBundle = request.getResourceBundle(new Locale("de"));
			i18n = new I18n(resourceBundle);
		}
		return i18n;
	}

	public static I18n getI18n(final SlingHttpServletRequest request, final String locale) {
		I18n i18n = null;

		final Locale pageLocale = new Locale(locale);
		final ResourceBundle resourceBundle = request.getResourceBundle(pageLocale);
		i18n = new I18n(resourceBundle);

		return i18n;
	}
}
