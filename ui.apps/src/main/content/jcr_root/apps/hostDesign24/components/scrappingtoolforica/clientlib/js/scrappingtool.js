$(document)
		.ready(
				function() {

					// Function that will allow us to know if Ajax uploads are
					// supported
					function supportAjaxUploadWithProgress() {
						return supportFileAPI()
								&& supportAjaxUploadProgressEvents()
								&& supportFormData();

						// Is the File API supported?
						function supportFileAPI() {
							var fi = document.createElement('INPUT');
							fi.type = 'file';
							return 'files' in fi;
						}
						;

						// Are progress events supported?
						function supportAjaxUploadProgressEvents() {
							var xhr = new XMLHttpRequest();
							return !!(xhr && ('upload' in xhr) && ('onprogress' in xhr.upload));
						}
						;

						// Is FormData supported?
						function supportFormData() {
							return !!window.FormData;
						}
					}

					// Actually confirm support
					if (supportAjaxUploadWithProgress()) {
						// Ajax uploads are supported!
						// Change the support message and enable the upload
						// button
						var notice = document.getElementById('support-notice');
						var uploadBtn = document
								.getElementById('upload-button-id');
						notice.innerHTML = "Your browser supports HTML uploads to AEM.";
						uploadBtn.removeAttribute('disabled');

						// Init the Ajax form submission
						initFullFormAjaxUpload();

						// Init the single-field file upload
						// initFileOnlyAjaxUpload();
					}

					function initFullFormAjaxUpload() {
						var uploadBtn = document
								.getElementById('upload-button-id');
						uploadBtn.onclick = function(evt) {
							var form = document.getElementById('form-id');

							var action = form.getAttribute('action');
							sendXHRequest(action);
						}
					}

					function initFileOnlyAjaxUpload() {
						var uploadBtn = document
								.getElementById('upload-button-id');
						uploadBtn.onclick = function(evt) {
							sendXHRequest();
						}
					}

					// Once the FormData instance is ready and we know
					// where to send the data, the code is the same
					// for both variants of this technique
					function sendXHRequest() {
						$(".scrappingToolDownload").hide();
                        $("#timeid").hide();
						onloadstartHandler();
						var url = '/etc/hostDesign24/productimporter.scrappingtoolforica.json';
						$.ajax({
							type : 'POST',
							url : url,
							processData : false,
							contentType : false,
							success : function(data) {
								onloadFinishedHandler();
								$(".scrappingToolDownload").show();
                                
							},
							error : function(error) {

							}
						});
					}

					// Handle the start of the transmission
					function onloadstartHandler() {
						var div = document.getElementById('upload-status');
						div.innerHTML = 'Scrapping Tool For ICA is running!';
					}

					function onloadFinishedHandler() {
						var div = document.getElementById('upload-status');
						div.innerHTML = 'Creation of product list completed, please download report!';
					}
				});