package com.aem.hostdesign24tools.core.valueobjects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HostDesign24.com
 *
 */
public class ColorsImportResults {

	private List<CsvImportVO> failure = new ArrayList<CsvImportVO>();
	private List<CsvImportVO> success = new ArrayList<CsvImportVO>();
	private List<CsvImportVO> noProduct = new ArrayList<CsvImportVO>();

	public List<CsvImportVO> getFailure() {
		return failure;
	}

	public void setFailure(List<CsvImportVO> failure) {
		this.failure = failure;
	}

	public List<CsvImportVO> getSuccess() {
		return success;
	}

	public void setSuccess(List<CsvImportVO> success) {
		this.success = success;
	}

	public List<CsvImportVO> getNoProduct() {
		return noProduct;
	}

	public void setNoProduct(List<CsvImportVO> noProduct) {
		this.noProduct = noProduct;
	}
}
