package com.aem.hostdesign24tools.core.valueobjects;

import org.apache.commons.codec.binary.StringUtils;
import com.google.gson.annotations.Expose;

/**
 * @author HostDesign24.com
 *
 */
public class FilterOption {

	@Expose
	private String key;
	@Expose
	private String name;

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FilterOption) {
			return StringUtils.equals(((FilterOption) obj).getKey(), this.key);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.key.hashCode();
	}

}
