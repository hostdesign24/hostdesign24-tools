package com.aem.hostdesign24tools.core.valueobjects.productsearch;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author HostDesign24.com
 *
 */
public class ProductSearchModel {

	@Expose
	@Getter
	@Setter
	private String gtin;
	@Expose
	@Getter
	@Setter
	private String colorTile;
	@Expose
	@Getter
	@Setter
	private String brandPagetitle;
	@Expose
	@Getter
	@Setter
	private String productPageTitle;
	@Expose
	@Getter
	@Setter
	private String productTitle;
	@Expose
	@Getter
	@Setter
	private String productPageUrl;
	@Expose
	@Getter
	@Setter
	private String productImageUrl;
	@Getter
	@Setter
	private String productPage;
	@Expose
	private boolean suggestedRetailsPrice;
	@Expose
	private boolean selectedStoreOnly;
	@Expose
	@Getter
	@Setter
	private int priority;
	@Expose
	@Getter
	@Setter
	private List<ProductTag> tags = new ArrayList<>();

	public boolean isSuggestedRetailsPrice() {
		return suggestedRetailsPrice;
	}

	public void setSuggestedRetailsPrice(boolean suggestedRetailsPrice) {
		this.suggestedRetailsPrice = suggestedRetailsPrice;
	}

	public boolean isSelectedStoreOnly() {
		return selectedStoreOnly;
	}

	public void setSelectedStoreOnly(boolean selectedStoreOnly) {
		this.selectedStoreOnly = selectedStoreOnly;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProductSearchModel) {
			return StringUtils.equals(((ProductSearchModel) obj).getProductPage(), getProductPage());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getProductPage().hashCode();
	}
}
