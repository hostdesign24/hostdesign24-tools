package com.aem.hostdesign24tools.core.services;

import java.io.InputStream;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

/**
 * @author HostDesign24.com
 *
 */
public interface GenerateFallbackMessagesService {

	void process(InputStream inputStream, ResourceResolver resourceResolver,
			SlingHttpServletRequest request);
}
