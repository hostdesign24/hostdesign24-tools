package com.aem.hostdesign24tools.core.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.ScrappingToolForICAService;
import com.aem.hostdesign24tools.core.utils.HttpRequestUtils;
import com.aem.hostdesign24tools.core.valueobjects.CsvProductImportVO;
import com.aem.hostdesign24tools.core.valueobjects.ica.Product;
import com.aem.hostdesign24tools.core.valueobjects.ica.ProductItem;
import com.aem.hostdesign24tools.core.valueobjects.ica.ProductItems;
import com.aem.hostdesign24tools.core.valueobjects.ica.Store;
import com.aem.hostdesign24tools.core.valueobjects.ica.StoreProduct;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.internal.LinkedTreeMap;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(ScrappingToolForICAService.class)
@Component(metatype = true, immediate = true, label = "Choicify Scrapping Tool For ICA Service")
public class ScrappingToolForICAServiceImpl implements ScrappingToolForICAService {

	@Property(label = "ICA stores url",
			value = "https://handla.ica.se/api/store/v1?groupby=citygroup&customertype=B2C",
			description = "scsrappingtool.ica.stores.url")
	private static final String STORES_URL = "scsrappingtool.ica.stores.url";


	@Property(label = "ICA stores related products",
			value = "https://handla.ica.se/api/content/v1/collections/facets/customer-type/B2C/store/%s/products?categories=harfargning-id_869",
			description = "scsrappingtool.ica.stores.products")
	private static final String STORES_PRODUCTS = "scsrappingtool.ica.stores.products";

	@Property(label = "ICA stores related product details",
			value = "https://handla.ica.se/api/content/v1/collections/customer-type/B2C/store/%s/products?productIds=%s",
			description = "scsrappingtool.ica.stores.products.details")
	private static final String STORES_PRODUCTS_DETAILS =
			"scsrappingtool.ica.stores.products.details";

	@Property(label = "ICA product urls", value = "https://www.ica.se/handla/produkt/%s",
			description = "scsrappingtool.ica.product.url")
	private static final String PRODUCTS_URL = "scsrappingtool.ica.product.url";

	@Property(label = "ICA product image url",
			value = "https://assets.icanet.se/t_product_large_v1,f_auto/%s.jpg",
			description = "scrappingtool.ica.product.image.url")
	private static final String PRODUCTS_IMAGE_URL = "scrappingtool.ica.product.image.url";


	@Getter
	private String storesUrl;
	@Getter
	private String storesProducts;
	@Getter
	private String storesProductsDetails;
	@Getter
	private String productsUrl;
	@Getter
	private String productImageUrl;

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		log.info("[*** ScrappingToolForICAService: activating configuration service");
		readProperties(properties);
	}

	protected void readProperties(Map<String, Object> properties) {
		this.storesUrl = PropertiesUtil.toString(properties.get(STORES_URL), "");
		this.storesProducts = PropertiesUtil.toString(properties.get(STORES_PRODUCTS), "");
		this.storesProductsDetails =
				PropertiesUtil.toString(properties.get(STORES_PRODUCTS_DETAILS), "");
		this.productsUrl = PropertiesUtil.toString(properties.get(PRODUCTS_URL), "");
		this.productImageUrl = PropertiesUtil.toString(properties.get(PRODUCTS_IMAGE_URL), "");
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public void process(ResourceResolver resourceResolver) {
		try {
			log.debug("ScrappingToolForICAService Started");
			Map<Long, Product> products = new HashMap<>();
			String storesresponse = HttpRequestUtils.executeGetRequest(getStoresUrl(), null, null);
			Gson gson = new Gson();
			Map<String, LinkedTreeMap<String, Map>> storelevel1 =
					gson.fromJson(storesresponse, Map.class);
			for (Map.Entry<String, LinkedTreeMap<String, Map>> entry : storelevel1.entrySet()) {
				LinkedTreeMap<String, Map> innerlevel1 = entry.getValue();
				for (Map.Entry<String, Map> childentry : innerlevel1.entrySet()) {
					Map<String, Map> innerlevel2 = childentry.getValue();
					for (Map.Entry<String, Map> childentry2 : innerlevel2.entrySet()) {
						JsonArray storesjson =
								(JsonArray) gson.toJsonTree(childentry2.getValue(), ArrayList.class);
						Store[] stores = gson.fromJson(storesjson, Store[].class);
						for (Store store : stores) {
							findProductIds(gson, store, products);
						}
					}
				}
			}

			// stores product resutls
			Session session = resourceResolver.adaptTo(Session.class);
			List<CsvProductImportVO> results = new ArrayList<>();
			CsvProductImportVO csvProductImportVO = null;
			System.setProperty("file.encoding", "UTF-8");
			String category = "Hårfärgning";
			for (Map.Entry<Long, Product> product : products.entrySet()) {
				Product serviceProduct = product.getValue();
				csvProductImportVO = new CsvProductImportVO();

				csvProductImportVO.setGtin(Long.toString(serviceProduct.getGtin()));
				csvProductImportVO.setCountry("SV");
				csvProductImportVO.setLanguage("sv");
				csvProductImportVO.setBrandTitle(serviceProduct.getBrand());
				csvProductImportVO.setCategory(category);
				csvProductImportVO.setProductLineTitle(category);

				csvProductImportVO.setProductTitle(serviceProduct.getName());
				csvProductImportVO.setProductUrl(String.format(getProductsUrl(), serviceProduct.getSlug()));
				csvProductImportVO
						.setImagePath(String.format(getProductImageUrl(), serviceProduct.getImageId()));

				results.add(csvProductImportVO);
			}

			saveScrappingToolResults(session, results);

		} catch (Exception ex) {
			log.error("Exception {}", ex);
		}
		log.debug("ScrappingToolForICAService finished");
	}

	private void saveScrappingToolResults(Session session, List<CsvProductImportVO> results)
			throws RepositoryException {

		String jsonResults = new Gson().toJson(results);

		Node resultNode =
				JcrUtils.getOrCreateByPath(HostDesign24AppConstants.SCRAPPING_PRODUCTS_PATH_FOR_ICA, false,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED,
						HostDesign24AppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED, session, true);

		if (resultNode.hasProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS)) {
			resultNode.getProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS).remove();
			resultNode.getSession().save();
		}
		resultNode.setProperty(HostDesign24AppConstants.PROP_NAME_COLOR_RESULTS, jsonResults);
		JcrUtils.setLastModified(resultNode, Calendar.getInstance());
		session.save();
	}

	private void findProductIds(Gson gson, Store store, Map<Long, Product> products)
			throws IOException {
		String storesProductsURL = String.format(getStoresProducts(), store.getSlug());
		List<String> productIds = new ArrayList<>();
		log.debug("{}", storesProductsURL);
		String productIdsResponse = HttpRequestUtils.executeGetRequest(storesProductsURL, null, null);
		ProductItems productItems = gson.fromJson(productIdsResponse, ProductItems.class);
		if (productItems != null && productItems.getItems() != null
				&& !productItems.getItems().isEmpty()) {
			List<ProductItem> items = productItems.getItems();
			for (ProductItem productItem : items) {
				if (StringUtils.equals("product", productItem.getType())
						&& !products.containsKey(Long.parseLong(productItem.getId()))) {
					productIds.add(productItem.getId());
				}
			}

			if (!productIds.isEmpty()) {

				for (int i = 0; i < productIds.size(); i += 10) {
					List<String> minProductIds = productIds.subList(i, Math.min(productIds.size(), i + 10));
					String productsids = StringUtils.join(minProductIds, ",");
					String productsDetailsURL =
							String.format(getStoresProductsDetails(), store.getSlug(), productsids);
					log.debug("{}", productsDetailsURL);
					String productDetailsResponse =
							HttpRequestUtils.executeGetRequest(productsDetailsURL, null, null);

					StoreProduct[] storeProducts =
							gson.fromJson(productDetailsResponse, StoreProduct[].class);
					for (StoreProduct storeProduct : storeProducts) {
						products.put(storeProduct.getProduct().getGtin(), storeProduct.getProduct());
						log.debug("{}", storeProduct.getProduct().getGtin());
					}
				}
			}
		}
	}
}
