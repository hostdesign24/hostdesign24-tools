(function(document, $) {
	"use strict";
	var ui = $(window).adaptTo("foundation-ui");
	var URL_BASE =  "/etc/choicifytools/productimporter";
	var CREATE_PRODUCTS_PATH = URL_BASE + ".generatejsonresults.json";
	var GET_COUNTRYS_PATH = URL_BASE + ".getcolorscountries.json";
	 
	 function toggleInProgressDialog() {
	        var dialog = document.querySelector("#inProgressDialog");
	        if(!dialog) {
	            dialog = new Coral.Dialog().set({
	                variant: "error",
	                id: "inProgressDialog",
	                header: {
	                    innerHTML: Granite.I18n.get("INFO")
	                },
	                content: {
	                    innerHTML: Granite.I18n.get("Operation is currently in progress. You cannot start a new job until it is finished. Please wait")
	                },
	                footer: {
	                    innerHTML: "<coral-wait size=\"L\" variant=\"dots\"></coral-wait>"
	                }
	            });
	            document.body.appendChild(dialog);
	        }
	        dialog.show();
	    }
	 
	 $(document).on("change", "#importbrand", function(e) {
		 var brandlist = document.querySelector('#importbrand');
		 var importcountry = document.querySelector('#importcountry');
		 importcountry.items.clear();
		 
		 $.ajax({
	            type: "GET",
	            url: Granite.HTTP.externalize(GET_COUNTRYS_PATH+"?brand="+brandlist.selectedItem.value),
	            processData : false,
	            contentType : false,
	            success: function (data) {
	            	
	            	data.forEach(function(item){
	            		importcountry.items.add({
	            	        value: item,
	            	        content: {
	            	          textContent: item
	            	        }
	            	      });
	            	});
	            }
	        });
	 });
	
	$(document).on("click", "button.granite-collection-create", function(e) {
        var form = $("form#generateJsonResults");
        toggleInProgressDialog();
        // stop the event propagation
        e.preventDefault();
        var formdata = new FormData(form[0]);
        
        $.ajax({
            type: "POST",
            url: Granite.HTTP.externalize(CREATE_PRODUCTS_PATH),
            data: formdata,
            processData : false,
            contentType : false,
            success: function (data) {
                var data = $.trim(data);
                document.querySelector("#inProgressDialog").hide();
                ui.notify(Granite.I18n.get("Success"), Granite.I18n.get("Successfully imported"), "success");
            },
            error: function (xmlhttprequest, textStatus, message) {
                ui.notify(Granite.I18n.get("Error"), Granite.I18n.get("An error occurred while processing products: {0}.", message, "0 is the error message"), "error")
            }
        });
    });
	
})(document, Granite.$);