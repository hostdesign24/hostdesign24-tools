package com.aem.hostdesign24tools.core.valueobjects.targetnaturalcolors;

import java.util.ArrayList;
import java.util.List;
import com.aem.hostdesign24tools.core.valueobjects.ColorVO;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author Christopher Nde: HostDesign24
 *
 */
@Data
public class TargetNaturalColors {
	@Expose
	private List<ColorVO> thcWithThs = new ArrayList<>();
	@Expose
	private List<ColorVO> nhcWithNhs = new ArrayList<>();
}
