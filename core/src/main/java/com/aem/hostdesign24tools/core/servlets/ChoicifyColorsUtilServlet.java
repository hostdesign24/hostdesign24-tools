package com.aem.hostdesign24tools.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import com.aem.hostdesign24tools.core.HostDesign24AppConstants;
import com.aem.hostdesign24tools.core.services.ChoicifyAppService;
import com.google.gson.Gson;

/**
 * @author HostDesign24.com
 *
 */
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = {"getcolorscountries"},
		extensions = "json", methods = {"GET"})
public class ChoicifyColorsUtilServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	@Reference
	private ChoicifyAppService choicifyAppService;

	@Override
	protected void doGet(final SlingHttpServletRequest request,
			final SlingHttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		List<String> countrys = new ArrayList<>();
		String selectedBrand = request.getParameter("brand");
		if (StringUtils.isNotBlank(selectedBrand)) {
			String defaultPath =
					String.format(HostDesign24AppConstants.CHOICIFY_COLORS_DEFAULT_PATH, selectedBrand);
			countrys = choicifyAppService.getChildNodes(defaultPath);
		}

		response.getWriter().print(new Gson().toJson(countrys));
	}
}
