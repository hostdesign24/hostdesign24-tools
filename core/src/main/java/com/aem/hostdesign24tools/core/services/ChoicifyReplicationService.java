package com.aem.hostdesign24tools.core.services;

import javax.jcr.Session;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;

/**
 * @author HostDesign24.com
 *
 */
public interface ChoicifyReplicationService {

  public void replicate(Session session, String path, ReplicationActionType actiontype) throws ReplicationException;
}
