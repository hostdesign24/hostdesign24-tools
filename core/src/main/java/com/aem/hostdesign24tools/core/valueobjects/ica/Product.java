package com.aem.hostdesign24tools.core.valueobjects.ica;

import lombok.Data;

/**
 * @author HostDesign24.com
 *
 */
@Data
public class Product {

	private String sku;
	private String name;
	private long gtin;
	private String imageId;
	private String brand;
	private String slug;
}
